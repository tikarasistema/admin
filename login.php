<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
$email = '';
$senha = '';
$erro = 0;

if ($_POST) {
    $email = getPost('email');
    $senha = senha(getPost('senha'));

    $sql = "Select * from tbusuario where (stemail = :stemail and stsenha = :stsenha) and (situacao ='" . USUARIO_ATIVO . "')";
    $con = MyPdo::connect();
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':stemail', $email);
    $stmt->bindValue(':stsenha', $senha);
    $stmt->execute();
    $usuario = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$usuario) {
        $erro = 1;
        erro();
    }
    if (!getSession('erro')) {
        $_SESSION['usuario'] = array();
        $_SESSION['usuario']['tipo'] = $usuario['intipo'];
        $_SESSION['usuario']['idusuario'] = $usuario['idusuario'];
        $_SESSION['usuario']['usuarioNome'] = $usuario['stnome'];
        $_SESSION['usuario']['tema'] = $usuario['tema'];
        logado();
        header('location:index.php');
    }
}
topo(array(
    'login' => '1',
    'css' => array(
        'css/login/login.css',
    )
));
?>
<div class="modal fade" id="modalErroLogin" role="dialog" tabindex='-1'>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login inválido</h4>
            </div>
            <div class="modal-body">
                <div class="mensagem">

                </div>
                <h4 style="text-align: center;font-family: sans-serif;">Preencha email e senha novamente</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>

    </div>
</div>

<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4  col-lg-offset-4 col-lg-4 login">
    <!--<div class="titulo">HIVE<span class="modalidade">COMMERCE</span></div>-->
    <div class="text-center"><img src="img/logoHiveCommerce.png" alt=""style="
                                  width: 250px;
                                  margin-top: 13px;"
                                  /> </div>
    <form class="form-signin" role="form" method="post" action="login.php">
        <div class="form-group">
            <label for="email" class="sr-only">Email: </label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Endereço de e-mail">
        </div>

        <div class="form-group">
            <label for="senha" class="sr-only">Senha: </label>
            <input type="password" class="form-control" id="fsenha" name="senha" placeholder="Senha">
        </div>

        <button type="submit" class="btn btn-primary btn-block btn-login">Fazer login</button>
        <div class="form-group"></div>
        <div class="text-center"> Esqueceu sua senha? Clique <a href="lembrarsenha.php">aqui</a></div>
    </form>
</div>
<script>
    var erro = <?php echo $erro; ?>;
</script>
<?php
rodape(array(
    'login' => '1',
    'js' => array(
        'js/login/login.js',
    )
));
?>