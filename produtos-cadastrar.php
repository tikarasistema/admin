
<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
?>

<?php
$dsproduto = '';
$precocompra = '';
$precovenda = '';
$estoque = '';
$unidade = '';
$codigobarra = '';
$marca = '';
$classificacao = '';
$lucro = '';
$_SESSION['erro'] = false;
$con = myPdo::connect();


if ($_POST) {

    $dsproduto = $_POST['produto'];
    $precocompra = $_POST['precocompra'];
    $precovenda = $_POST['precovenda'];
    $situacao = PRODUTO_ATIVO;
    $estoque = $_POST['estoque'];
    $codigobarra = $_POST['codigobarra'];
    $observacao = $_POST['observacao'];
    $marca = $_POST['marca'];
    $classificacao = $_POST['classificacao'];
    $lucro = $_POST['lucro'];





    $sql = "SELECT idmarca FROM tbmarca where situacao != " . MARCA_INATIVA;
    $stmt = $con->query($sql);
    $marcas = $stmt->fetchall(PDO::FETCH_COLUMN);
    
    $sql = "SELECT idclassificacao FROM tbtipo";
    $stmt = $con->query($sql);
    $classificacoes = $stmt->fetchall(PDO::FETCH_COLUMN);

    if ($classificacao != 0) {
        if (!in_array($classificacao, $classificacoes)) {
            addMessage('Selecione tipo válido');
            erro();
        }
    }

    if (!empty($marca)) {
        if (!in_array($marca, $marcas)) {
            addMessage('Selecione marca válida, marca pode estar inativa');
            erro();
        }
    }

    if (strlen($dsproduto) <= 3) {
        addMessage("Preencha o campo produto");
        $_SESSION['erro'] = true;
    }

    if (empty($codigobarra)) {
        addMessage("Preencha o código de barra");
        $_SESSION['erro'] = true;
    }

    try {

        $select = $con->query("select codigobarra, count(codigobarra) from tbproduto group by codigobarra having count(codigobarra) > 1");

        $codbarra = $select->fetch(PDO::FETCH_ASSOC);
        if ($codbarra['codigobarra']) {
            addMessage("Produto com código de barra ja existe digite outro produto");
            $_SESSION['erro'] = true;
        }
    } catch (PDOException $e) {
        echo '' . $e->getMessage();
    }


    if (empty($precocompra)) {
        addMessage("Preencha o preço de compra");
        $_SESSION['erro'] = true;
    }
    if ($precocompra < 0) {
        addMessage("Preço de compra negativo, digite valores positivos");
        erro();
    }

    if (empty($precovenda)) {
        addMessage("Preencha o preço de venda");
        $_SESSION['erro'] = true;
    }
    if ($precovenda < 0) {
        addMessage("Preço de venda negativo, digite valores positivos");
    }

    if ($precovenda < $precocompra) {
        addMessage("Preço de venda deve ser maior que preço de compra");
        $_SESSION['erro'] = true;
    }
    if (empty($estoque)) {
        addMessage('Preencha campo estoque');
        erro();
    }
    if ($estoque < 0) {
        addMessage('Quantidade de estoque inválido');
        erro();
    }
    if (empty($lucro)) {
        addMessage('Digite Lucro');
        erro();
    }

    if ($lucro < 0) {
        addMessage('Lucro inválido');
        erro();
    }

    if (!getSession('erro')) {
        try {
            $stmt = $con->prepare("INSERT INTO tbproduto(produto, precocompra, precovenda, situacao, estoque, codigobarra, observacao, idmarca, idclassificacao, lucro) VALUES (:produto, :precocompra, :precovenda, :situacao, :estoque, :codigobarra, :observacao, :idmarca, :idclassificacao, :lucro)");
            $stmt->bindValue(':produto', $dsproduto);
            $stmt->bindValue(':precocompra', $precocompra);
            $stmt->bindValue(':precovenda', $precovenda);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindvalue(':estoque', $estoque);
            $stmt->bindValue(':codigobarra', $codigobarra);
            $stmt->bindValue(':observacao', $observacao);
            $stmt->bindValue(':idmarca', $marca);
            $stmt->bindValue(':idclassificacao', $classificacao);
            $stmt->bindValue(':lucro', $lucro);
            $r = $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'produtos.php',
                'origem' => 'produto',
                'tipo' => 'Cadastro'
            ));
        } catch (PDOException $e) {
            addMessage("Erro ao cadastrar" . $e);
            $_SESSION['erro'] = true;
        }
    }
}
?>


<?php
topo(array(
    'css' => array(
        "css/produto/produto.css",
    ),
    'pageName' => ' Cadastrar Produto',
    'icon' => 'fa fa-inbox fa-lg',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarProduto" name="produtos-cadastrar" role="form" method="post" action="produtos-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input class ="gerarcdbarra" type="checkbox" id="gerarcdbarra" name="gerarcdbarra"> Gerar Código de Barra?
                                        </label>
                                    </div>
                                    <div class="help-block" >Ao selecionar esta opção, sistema irá gerar um código de barra.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcodigobarra"> Código de barra</label>
                                    <input value="<?php echo $codigobarra;
?>" type="text" class="form-control" id="fcodigobarra" name="codigobarra" placeholder="Informe o código de barra ">
                                    <span id="help-block" class="help-block">&nbspExemplo: 0123456789</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="fproduto">Descrição do Produto *</label>
                                    <input value ="<?php echo $dsproduto; ?>" type="text" class="form-control" id="fproduto" name="produto"placeholder="Informe o produto ">
                                    <span id="help-block" class="help-block">&nbspMin: 3 caracteres</span>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="festoque">Estoque *</label>
                                <input value="<?php echo $estoque; ?>" type="text" class="form-control" id="estoque" name="estoque" value="0" placeholder="Informe o estoque ">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="precocompra">Preço compra *</label>
                                <div class="input-group">
                                    <span class="input-group-addon">R$</span>

                                    <input value="<?php echo $precocompra; ?>" type="text" class="form-control" id="precocompra"  name="precocompra"  length="15" onkeypress="return formatar_moeda(this, '', '.', event);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="lucro">Lucro*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">%</span>
                                    <input  type="text" class="form-control" id="lucro"  name="lucro" value="<?php echo $lucro; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="fprecovenda">Preço venda *</label>
                                <div class="input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input value="<?php echo $precovenda; ?>" type="text" class="form-control" id="precovenda"  name="precovenda"  length="15" onkeypress="return formatar_moedavenda(this, '', '.', event);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="classificacao">Classificação </label>
                                    <select class="form-control" id="classificacao" name="classificacao">
                                        <option value="0">Escolha o tipo de produto </option>
                                        <?php
                                        $sql = "SELECT idclassificacao, dsclassificacao FROM tbtipo  where situacao != '" . CLASSIFICACAO_INATIVO . "' order by dsclassificacao";
                                        $consulta = $con->query($sql);
                                        $consulta->execute();
                                        while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <option value="<?php echo $resultado['idclassificacao']; ?>"
                                                    <?php echo $classificacao == $resultado['idclassificacao'] ? 'selected' : ''; ?> >
                                                        <?php echo $resultado['dsclassificacao'];
                                                        ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="marca">Marca </label>
                                    <select class="form-control" id="marca" name="marca">
                                        <option value="0">--Selecione o Tipo --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <label for="observacao">Observação</label>
                                <input type="text" class="form-control" id="observacao" name="observacao" placeholder=" ">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarProduto">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarProduto">Cancelar </button>

            </div>

        </div>
    </div>
</div>
<script>
    var precocompra = '<?php echo $precocompra; ?>';
    var lucro = '<?php echo $lucro; ?>';
</script>
<?php
rodape(array(
    'js' => array(
        'js/produto/produto.js',
    )
));
?>
