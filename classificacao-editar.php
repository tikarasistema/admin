<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$con = MyPdo::connect();
$classificacao = '';
$idclassificacao = '';
$situacao = '';
$_SESSION['erro'] = false;

if (isset($_GET['cd'])) {
    $idclassificacao = $_GET['cd'];
    $sql = "Select * from tbtipo where idclassificacao = :idclassificacao";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idclassificacao', $idclassificacao);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$consulta) {
        editarErro(array(
            'page' => 'classificacoes.php',
            'origem' => 'classificação',
            'tipo' => 'Erro na atualização'
        ));
    }
}

if ((!isset($_GET['cd'])) and ( !isset($_POST['idclassificacao']))) {
    editarErro(array(
        'page' => 'classificacoes.php',
        'origem' => 'classificação',
        'tipo' => 'Erro na atualização'
    ));
}
if ($_POST) {

    $classificacao = getPost('classificacao');
    $idclassificacao = getPost('idclassificacao');
    $situacao = getPost('situacao');
    if ($situacao) {
        $situacao = CLASSIFICACAO_ATIVO;
    } else {
        $situacao = CLASSIFICACAO_INATIVO;
    }

    $sql = "Select UPPER(dsclassificacao) from tbtipo where dsclassificacao = UPPER(:dsclassificacao) and idclassificacao != :idclassificacao";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dsclassificacao', $classificacao);
    $stmt->bindValue(':idclassificacao', $idclassificacao);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Classificação cadastrada anteriormente");
        erro();
    }

    if (($situacao != CLASSIFICACAO_ATIVO) and ( $situacao != CLASSIFICACAO_INATIVO)) {
        addMessage("Selecione situação válida");
        erro();
    }

    if (empty($classificacao)) {
        addMessage("Preecnha campo descrição");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Update tbtipo set dsclassificacao = :dsclassificacao, situacao = :situacao where idclassificacao = :idclassificacao";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dsclassificacao', $classificacao);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':idclassificacao', $idclassificacao);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'classificacoes.php',
                'origem' => 'classificação',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}
$sql = "select * from tbtipo where idclassificacao = :idclassificacao";
$stmt = $con->prepare($sql);
$stmt->bindParam(':idclassificacao', $idclassificacao);
$stmt->execute();
$classificacaoAtual = $stmt->fetch(PDO::FETCH_ASSOC);
topo(array(
    "pageName" => 'Editar classificação #' . $idclassificacao,
    "icon" => 'fa fa-sitemap'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="editarClassificacao" name="editarClassificacao" role="form" method="post" action="classificacao-editar.php">
                    <input type="hidden" name="idclassificacao" value="<?php echo $idclassificacao; ?>">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fclassificacao">Descrição*</label>
                                    <input type="text" class="form-control" id="classificacao" name="classificacao" placeholder="Digite nome da classificação " value="<?php echo $classificacao ? $classificacao : $classificacaoAtual['dsclassificacao']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="fmarca"><input name="situacao" type="checkbox" <?php
                                            if ($_GET) {
                                                echo $classificacaoAtual['situacao'] == MARCA_ATIVA ? 'checked' : '';
                                            } elseif ($_POST) {
                                                echo $situacao == MARCA_ATIVA ? 'checked' : '';
                                            }
                                            ?>> Classificação Ativa?
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="editarClassificacao">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="editarClassificacao">Cancelar </button>
        </div>
    </div>
</div>
</div>
</div>
<?php rodape(array()); ?>
