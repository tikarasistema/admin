<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$entrada = (float) 0;
$total = (float) 0;
$qtdParcelas = 0;
$tementrada = 0;
$cartao = 0;
$con = MyPdo::connect();
$retorno['erro'] = '';
$retorno['erro']['msg'] = '';
$retorno['sucesso'] = '';
$situacaoParcela = PARCELA_ABERTA;
$desconto = 0;
$total2 = 0;

if ($_POST) {
    $entrada = getPost('entrada', null);
    $total = getPost('total');
    $qtdParcelas = getPost('qtdParcelas');
    $tementrada = getPost('tementrada');
    $idcompra = getPost('idcompra');
    $datacompra = getPost('datacompra');
    $idfornecedor = getPost('idfornecedor');
    $condicaopgto = getPost('condpgto');
    $cartao = getPost('cartao');
    $desconto = getPost('desconto');
    $totalValidar = getPost('totalValidar');

    if ($cartao == 1) {
        $situacaoParcela = PARCELA_CARTAO;
    } else {
        $situacaoParcela = PARCELA_ABERTA;
    }
    if ($tementrada) {
        if ($entrada < 0 or $entrada < 1) {
            $retorno['erro'] = 1;
            $retorno['erroMsg'] = 'Entrada com valor inválido';
        }
    }
    if ($desconto < 0) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Desconto com valor inválido';
    }
    if ($desconto > $totalValidar) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Desconto deve ser menor que total';
    }
    if ($total < 0) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Total com valor inválido';
    }
    if ($retorno['erro'] != 1) {
        try {
            $sql = "Update tbcompra set idcondpgto = :idcondpgto where idcompra = :idcompra";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idcompra', $idcompra);
            $stmt->bindValue(':idcondpgto', $condicaopgto);
            $stmt->execute();
            $retorno['sucesso'] = 2;
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erro']['msg'] = $e;
        }
        if ($qtdParcelas > 0) {
            $valorPorParcela = round($total / $qtdParcelas, 2);
        }
//Aqui a mágica acontece
        try {
            $sql = "Delete from tbcontaspagar where idcompra = :idcompra";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idcompra', $idcompra);
            $stmt->execute();
            $retorno['sucesso'] = 2;
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erro']['msg'] = $e;
        }
        try {
            $nrParcela = 0;
            if ($qtdParcelas > 0) {
                if ($tementrada) {
                    try {
                        $sql = "insert into tbcontaspagar (parcela_cp, idcompra, documento, dt_emissao, dt_vencimento, dt_pagamento, vlr_parcela, vlr_pago, situacao, idfornecedor) values (:parcela_cp ,:idcompra, :documento, :dt_emissao, :dt_vencimento, :dt_pagamento, :vlr_parcela, :vlr_pago, :situacao, :idfornecedor)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':parcela_cp', $nrParcela+=1);
                        $stmt->bindValue(':idcompra', $idcompra);
                        $stmt->bindValue(':documento', 'PG-' . PAGAR . $idcompra);
                        $stmt->bindValue(':dt_emissao', $datacompra);
                        $stmt->bindValue(':dt_vencimento', $datacompra);
                        $stmt->bindValue(':dt_pagamento', $datacompra);
                        $stmt->bindValue(':vlr_parcela', $entrada);
                        $stmt->bindValue(':vlr_pago', $entrada);
                        $stmt->bindValue(':situacao', PARCELA_BAIXADA);
                        $stmt->bindValue(':idfornecedor', $idfornecedor);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
                    }
                    try {
                        $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                                . "descLancamento, idhistorico, inautomatico) values "
                                . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                        $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                        $stmt->bindValue(':tipoLancamento', 2);
                        $stmt->bindValue(':vlLancamento', $entrada);
                        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                        $stmt->bindValue(':descLancamento', 'Baixa parcela saida pagar #' . $idcompra);
                        $stmt->bindValue(':idhistorico', 7);
                        $stmt->bindValue(':inautomatico', 1);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
                    }
//                    try {
//                        $sql = "Insert into tbamortizacao (parcela_cr, dt_pagto, vlr_pago, idusuario, idcompra, tipo) values (:parcela_cr, :dt_pagto, :vlr_pago, :idusuario, :idcompra, :tipo)";
//                        $stmt = $con->prepare($sql);
//                        $stmt->bindValue(':parcela_cr', $nrParcela);
//                        $stmt->bindValue(':dt_pagto', date('Y-m-d H:i:s'));
//                        $stmt->bindValue(':vlr_pago', $entrada);
//                        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
//                        $stmt->bindValue(':idcompra', $idcompra);
//                        $stmt->bindValue(':tipo', AMORTIZACAO_BAIXA);
//                        $stmt->execute();
//                    } catch (Exception $ex) {
//                        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
//                    }
                }
                for ($i = 0; $i < $qtdParcelas; $i++) {
                    $data = $i + 1;

                    $total2 += $valorPorParcela;

                    if ($i == $qtdParcelas - 1) {
                        if ($total2 < $total) {
                            $valorPorParcela = ($total - $total2) + $valorPorParcela;
                        }
                    }
                    try {
                        $sql = "insert into tbcontaspagar (parcela_cp, idcompra, documento, dt_emissao, dt_vencimento, dt_pagamento, vlr_parcela, situacao, idfornecedor) values (:parcela_cp, :idcompra, :documento, :dt_emissao, :dt_vencimento, :dt_pagamento, :vlr_parcela, :situacao, :idfornecedor)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':parcela_cp', ++$nrParcela);
                        $stmt->bindValue(':idcompra', $idcompra);
                        $stmt->bindValue(':documento', 'PG-' . PAGAR . $idcompra);
                        $stmt->bindValue(':dt_emissao', $datacompra);
                        $stmt->bindValue(':dt_vencimento', gerarParcela($datacompra, $data));
                        $stmt->bindValue(':dt_pagamento', null);
                        $stmt->bindValue(':vlr_parcela', $valorPorParcela);
                        $stmt->bindValue(':situacao', $situacaoParcela);
                        $stmt->bindValue(':idfornecedor', $idfornecedor);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                    }
                }
            } else {
                if ($cartao == 1) {
                    $situacaoParcela = PARCELA_CARTAO;
                } else {
                    $situacaoParcela = PARCELA_BAIXADA;
                }
                $sql = "insert into tbcontaspagar (parcela_cp, idcompra, documento, dt_emissao, dt_vencimento, dt_pagamento, vlr_parcela, vlr_pago, situacao, idfornecedor) values (:parcela_cp ,:idcompra, :documento, :dt_emissao, :dt_vencimento, :dt_pagamento, :vlr_parcela, :vlr_pago, :situacao, :idfornecedor)";
                try {
                    $stmt = $con->prepare($sql);
                    $stmt->bindValue(':parcela_cp', $nrParcela + 1);
                    $stmt->bindValue(':idcompra', $idcompra);
                    $stmt->bindValue(':documento', 'PG-' . PAGAR . $idcompra);
                    $stmt->bindValue(':dt_emissao', $datacompra);
                    $stmt->bindValue(':dt_vencimento', $datacompra);
                    $stmt->bindValue(':dt_pagamento', $datacompra);
                    $stmt->bindValue(':vlr_parcela', $total);
                    $stmt->bindValue(':vlr_pago', $total);
                    $stmt->bindValue(':situacao', $situacaoParcela);
                    $stmt->bindValue(':idfornecedor', $idfornecedor);
                    $stmt->execute();
                } catch (Exception $e) {
                    $retorno['erro'] = 1;
                    $retorno['erroMsg'] = 'erro a vista ' . $e;
                }
                try {
                    $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                            . "descLancamento, idhistorico, inautomatico) values "
                            . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                    $stmt = $con->prepare($sql);
                    $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                    $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                    $stmt->bindValue(':tipoLancamento', 2);
                    $stmt->bindValue(':vlLancamento', $total);
                    $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                    $stmt->bindValue(':descLancamento', 'Pagamento apagar #' . $idcompra . ' pagamento a vista');
                    $stmt->bindValue(':idhistorico', 7);
                    $stmt->bindValue(':inautomatico', 1);
                    $stmt->execute();
                } catch (Exception $e) {
                    $retorno['erro'] = 1;
                    $retorno['erroMsg'] = $e;
                }
            }
            $retorno['sucesso'] = 1;
        } catch (Exception $e) {
            $retorno['sucesso'] = 0;
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
        echo json_encode($retorno);
    } else {
        echo json_encode($retorno);
    }
}