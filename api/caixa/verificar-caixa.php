<?php

date_default_timezone_set('America/Sao_Paulo');

require '../../lib/conexao.php';
require '../../lib/config.php';
require '../../lib/funcoes.php';
require '../../lib/protege.php';

$caixa = array();
$pdo = new MyPdo;
$con = $pdo->connect();

$hoje = (date("Y-m-d"));
$select = $con->query("Select * from tbcaixa where dataAbertura = '$hoje'");
$resultado = $select->fetchAll(PDO::FETCH_ASSOC);

if (!$resultado) {
    $caixa['aberto'] = false;
    echo json_encode($caixa);
} else {
    $caixa['aberto'] = true;
    echo json_encode($caixa);
}

