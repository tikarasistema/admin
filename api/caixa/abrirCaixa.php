<?php

require '../../lib/conexao.php';
require '../../lib/config.php';
require '../../lib/funcoes.php';
require '../../lib/protege.php';

$con = MyPdo::connect();
$desclancamento = 'ABERTURA DE CAIXA';
$idhistorico = 1;
$vlLancamento = '';
$retorno = array();
$retorno['erro'] = '';
$retorno['success'] = '';
$_SESSION['erro'] = false;
$vlLancamento = getPost('saldoInicial');

if ($vlLancamento == '0.00') {
    $retorno['erro'] = "Valor de lançamento deve ser maior que zero";
}
if (!is_numeric($vlLancamento)) {
    $retorno['erro'] = "Valor de lançamento inválido";
}

if ($vlLancamento < 0) {
    $retorno['erro'] = "Valor de lançamento deve ser positivo";
}

if (!$retorno['erro']) {
    try {
        $sql = "select tipohistorico from tbhistorico where idhistorico = :idhistorico";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idhistorico', $idhistorico);
        $stmt->execute();
        $tipoLancamento = $stmt->fetch(PDO::FETCH_COLUMN);

        $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                . "descLancamento, idhistorico, inautomatico) values "
                . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
        $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
        $stmt->bindValue(':tipoLancamento', $tipoLancamento);
        $stmt->bindValue(':vlLancamento', $vlLancamento);
        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
        $stmt->bindValue(':descLancamento', $desclancamento);
        $stmt->bindValue(':idhistorico', $idhistorico);
        $stmt->bindValue(':inautomatico', LANCAMENTO_AUTOMATICO);
        $stmt->execute();
        $retorno['success'] = "Abertura de caixa realizada com sucesso";
    } catch (Exception $e) {
        $retorno['erro'] = $e;
    }
}

print_r(json_encode($retorno));
