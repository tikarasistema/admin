<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$produto = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $produto = $_POST['produto'];
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbproduto where idproduto = :produto");
    $stmt->bindValue(':produto', $produto);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == PRODUTO_ATIVO) {
        $situacao = PRODUTO_INATIVO;
    } else {
        $situacao = PRODUTO_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbproduto set situacao = :situacao where idproduto = $produto");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



