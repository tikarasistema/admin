<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$con = MyPdo::connect();

$idlancamento = getPost("id", null);
$retorno = array();
$retorno['erro'] = null;
$retorno['errousuario'] = null;
$retorno['success'] = null;

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('lancamento-conta-editar.php', $telasPermissao)) {
    $retorno['errousuario'] = 'Usuário não tem permissão para exclusão de lançamento';
}

if (($idlancamento != null) and ( $retorno['erro']['usuario'] == null)) {
    try {
        $stmt = $con->prepare("Select * from tbcaixa where idlancamento = :idlancamento");
        $stmt->bindValue(':idlancamento', $idlancamento);
        $stmt->execute();
        $lancamento = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        $retorno['erro'] = 'Lançamento inválido';
    }
    if ($lancamento['inautomatico'] == LANCAMENTO_AUTOMATICO) {
        $retorno['erro'] = 'Este é um lançamento automático de estorno ou baixa de parcela, não é possivel excluir o mesmo';
    }
    if ($lancamento['idhistorico'] == LANCAMENTO_ABERTURA_CAIXA) {
        $retorno['erro'] = 'Este é um lançamento automático de abertura de caixa, não é possivel excluir o mesmo';
    }

    if (null == $retorno['erro']) {
        $stmt = $con->prepare("Delete from tbcaixa where idlancamento = :idlancamento");
        $stmt->bindValue(':idlancamento', $idlancamento);
        $stmt->execute();
        $retorno['success'] = 'Lançamento excluído com sucesso';
    }
}

print_r(json_encode($retorno));
