<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idcompra = '';
$retorno = array();
$con = MyPdo::connect();
if ($_POST) {
    $idcompra = $_POST['idcompra'];

    try {
        $sql = "update tbcompra set situacao = " . COMPRA_CANCELADA . " where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
        $retorno['sucesso'] = 1;
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    echo json_encode($retorno);
}
