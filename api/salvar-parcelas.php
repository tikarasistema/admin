<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$entrada = (float) 0;
$total = (float) 0;
$qtdParcelas = 0;
$tementrada = 0;
$cartao = 0;
$con = MyPdo::connect();
$retorno['erro'] = '';
$retorno['erro']['msg'] = '';
$retorno['sucesso'] = '';
$situacaoParcela = PARCELA_ABERTA;
$desconto = 0;
$total2 = 0;
if ($_POST) {
    $entrada = getPost('entrada', null);
    $total = getPost('total');
    $qtdParcelas = getPost('qtdParcelas');
    $tementrada = getPost('tementrada');
    $idvenda = getPost('idvenda');
    $datavenda = getPost('datavenda');
    $idcliente = getPost('idcliente');
    $condicaopgto = getPost('condpgto');
    $cartao = getPost('cartao');
    $desconto = getPost('desconto');
    $totalValidar = getPost('totalValidar');

    if ($cartao == 1) {
        $situacaoParcela = PARCELA_CARTAO;
    } else {
        $situacaoParcela = PARCELA_ABERTA;
    }
    if ($tementrada) {
        if ($entrada < 0 or $entrada < 1) {
            $retorno['erro'] = 1;
            $retorno['erroMsg'] = 'Entrada com valor inválido';
        }
    }
    if ($desconto < 0) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Desconto com valor inválido';
    }
    if ($desconto > $totalValidar) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Desconto deve ser menor que total';
    }
    if ($total < 0) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = 'Total com valor inválido';
    }
    if ($retorno['erro'] != 1) {
        try {
            $sql = "Update tbvenda set idcondpgto = :idcondpgto where idvenda = :idvenda";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idvenda', $idvenda);
            $stmt->bindValue(':idcondpgto', $condicaopgto);
            $stmt->execute();
            $retorno['sucesso'] = 2;
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erro']['msg'] = $e;
        }
        if ($qtdParcelas > 0) {
            $valorPorParcela = round($total / $qtdParcelas, 2);
        }
//Aqui a mágica acontece
        try {
            $sql = "Delete from tbcontasreceber where idvenda = :idvenda";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idvenda', $idvenda);
            $stmt->execute();
            $retorno['sucesso'] = 2;
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erro']['msg'] = $e;
        }
        try {
            $nrParcela = 0;
            if ($qtdParcelas > 0) {
                if ($tementrada) {
                    try {
                        $sql = "insert into tbcontasreceber (parcela_cr,idvenda, documento, dt_emissao, dt_vencimento, dt_pagto, vlr_parcela,vrl_recebido, situacao, idcliente) values (:parcela_cr,:idvenda, :documento, :dt_emissao, :dt_vencimento, :dt_pagto, :vlr_parcela, :vlr_recebido, :situacao, :idcliente)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':parcela_cr', $nrParcela+=1);
                        $stmt->bindValue(':idvenda', $idvenda);
                        $stmt->bindValue(':documento', 'PA-' . RECEBER . $idvenda);
                        $stmt->bindValue(':dt_emissao', $datavenda);
                        $stmt->bindValue(':dt_vencimento', $datavenda);
                        $stmt->bindValue(':dt_pagto', $datavenda);
                        $stmt->bindValue(':vlr_parcela', $entrada);
                        $stmt->bindValue(':vlr_recebido', $entrada);
                        $stmt->bindValue(':situacao', PARCELA_BAIXADA);
                        $stmt->bindValue(':idcliente', $idcliente);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                    }
                    try {
                        $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                                . "descLancamento, idhistorico, inautomatico) values "
                                . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                        $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                        $stmt->bindValue(':tipoLancamento', 1);
                        $stmt->bindValue(':vlLancamento', $entrada);
                        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                        $stmt->bindValue(':descLancamento', 'Baixa parcela entrada venda #' . $idvenda);
                        $stmt->bindValue(':idhistorico', 5);
                        $stmt->bindValue(':inautomatico', 1);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                    }
                    try {
                        $sql = "Insert into tbamortizacao (parcela_cr, dt_pagto, vlr_pago, idusuario, idvenda, tipo) values (:parcela_cr, :dt_pagto, :vlr_pago, :idusuario, :idvenda, :tipo)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':parcela_cr', $nrParcela);
                        $stmt->bindValue(':dt_pagto', date('Y-m-d H:i:s'));
                        $stmt->bindValue(':vlr_pago', $entrada);
                        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                        $stmt->bindValue(':idvenda', $idvenda);
                        $stmt->bindValue(':tipo', AMORTIZACAO_BAIXA);
                        $stmt->execute();
                    } catch (Exception $ex) {
                        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
                    }
                }
                for ($i = 0; $i < $qtdParcelas; $i++) {
                    $data = $i + 1;
                    $total2 += $valorPorParcela;

                    if ($i == $qtdParcelas - 1) {
                        if ($total2 < $total) {
                            $valorPorParcela = ($total - $total2) + $valorPorParcela;
                        }
                    }
                    try {
                        $sql = "insert into tbcontasreceber (parcela_cr,idvenda, documento, dt_emissao, dt_vencimento, dt_pagto, vlr_parcela, situacao, idcliente) values (:parcela_cr,:idvenda, :documento, :dt_emissao, :dt_vencimento, :dt_pagto, :vlr_parcela, :situacao, :idcliente)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':parcela_cr', ++$nrParcela);
                        $stmt->bindValue(':idvenda', $idvenda);
                        $stmt->bindValue(':documento', 'PA-' . RECEBER . $idvenda);
                        $stmt->bindValue(':dt_emissao', $datavenda);
                        $stmt->bindValue(':dt_vencimento', gerarParcela($datavenda, $data));
                        $stmt->bindValue(':dt_pagto', null);
                        $stmt->bindValue(':vlr_parcela', $valorPorParcela);
                        $stmt->bindValue(':situacao', $situacaoParcela);
                        $stmt->bindValue(':idcliente', $idcliente);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $retorno['erro'] = 1;
                        $retorno['erroMsg'] = $e;
                    }
                }
            } else {
                if ($cartao == 1) {
                    $situacaoParcela = PARCELA_CARTAO;
                } else {
                    $situacaoParcela = PARCELA_BAIXADA;
                }
                $sql = "insert into tbcontasreceber (parcela_cr,idvenda, documento, dt_emissao, dt_vencimento, dt_pagto, vlr_parcela, vrl_recebido, situacao, idcliente) values (:parcela_cr,:idvenda, :documento, :dt_emissao, :dt_vencimento, :dt_pagto, :vlr_parcela, :vlr_recebido, :situacao, :idcliente)";
                try {
                    $stmt = $con->prepare($sql);
                    $stmt->bindValue(':parcela_cr', $nrParcela + 1);
                    $stmt->bindValue(':idvenda', $idvenda);
                    $stmt->bindValue(':documento', 'PA-' . RECEBER . $idvenda);
                    $stmt->bindValue(':dt_emissao', $datavenda);
                    $stmt->bindValue(':dt_vencimento', $datavenda);
                    $stmt->bindValue(':dt_pagto', $datavenda);
                    $stmt->bindValue(':vlr_parcela', $total);
                    $stmt->bindValue(':vlr_recebido', $total);
                    $stmt->bindValue(':situacao', $situacaoParcela);
                    $stmt->bindValue(':idcliente', $idcliente);
                    $stmt->execute();
                } catch (Exception $e) {
                    $retorno['erro'] = 1;
                    $retorno['erroMsg'] = 'erro a vista ' . $e;
                }
                try {
                    $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                            . "descLancamento, idhistorico, inautomatico) values "
                            . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                    $stmt = $con->prepare($sql);
                    $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                    $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                    $stmt->bindValue(':tipoLancamento', 1);
                    $stmt->bindValue(':vlLancamento', $total);
                    $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                    $stmt->bindValue(':descLancamento', 'Baixa venda #' . $idvenda . ' pagamento a vista');
                    $stmt->bindValue(':idhistorico', 5);
                    $stmt->bindValue(':inautomatico', 1);
                    $stmt->execute();
                } catch (Exception $e) {
                    $retorno['erro'] = 1;
                    $retorno['erroMsg'] = $e;
                }
            }
            $retorno['sucesso'] = 1;
        } catch (Exception $e) {
            $retorno['sucesso'] = 0;
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
        echo json_encode($retorno);
    } else {
        echo json_encode($retorno);
    }
}