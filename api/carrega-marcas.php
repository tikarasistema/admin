<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$marcas = array();
$classificacao = $_POST['classificacao'];

$produto = null;
$select = '';
$pdo = new MyPdo;
$con = $pdo->connect();
$marcasEnviar = array();

    $sql = "Select idmarca, dsmarca from tbmarca as m where m.idmarca in (select idmarca from tbtipomarca where idclassificacao = :idclassificacao) and situacao != 0";
    $stmt = $con->prepare($sql);
    $stmt->bindParam(':idclassificacao', $classificacao);
    $stmt->execute();
    $marcas = $stmt->fetchAll(PDO::FETCH_ASSOC);



foreach ($marcas as $marca) {
    $marcasEnviar[] = array(
        'idmarca' => $marca['idmarca'],
        'dsmarca' => $marca['dsmarca'],
    );
}

echo json_encode($marcasEnviar);

