<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$con = MyPdo::connect();
$retorno['erro'] = 0;
$retorno['erro']['msg'];
$retorno['sucesso'] = 0;

if ($_POST) {
    $idprod = $_POST['idproduto'];
    $idcompra = $_POST['idcompra'];

    $sql = "Select * from tbitem_compra where idproduto = :idproduto and idcompra = :idcompra";

    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcompra', $idcompra);
    $stmt->bindValue(':idproduto', $idprod);
    $stmt->execute();
    $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (!$consulta) {
        $retorno['erro'] = 1;
        $retorno['erro']['msg'] = "Item inexistente";
    }

    if ($retorno['erro'] != 1) {
        try {
            $sql = "Delete from tbitem_compra where idcompra = :idcompra and idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idcompra', $idcompra);
            $stmt->bindValue(':idproduto', $idprod);
            $stmt->execute();
            $retorno['sucesso'] = 1;
        } catch (Exception $exc) {
            $retorno['erro']['msg'] = "Erro ao excluir item " . $e;
            $retorno['erro'] = 1;
        }
    }
    echo json_encode($retorno);
}
