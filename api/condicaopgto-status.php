<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$condpgto = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $condpgto = getPost('condpgto');
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbcondpgto where idcondpgto = :condpgto");
    $stmt->bindValue(':condpgto', $condpgto);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == CONDPGTO_ATIVO) {
        $situacao = CONDPGTO_INATIVO;
    } else {
        $situacao = CONDPGTO_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbcondpgto set situacao = :situacao where idcondpgto = $condpgto");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



