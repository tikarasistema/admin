<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';
$fornecedor = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $fornecedor = $_POST['fornecedor'];
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbfornecedor where idfornecedor = :fornecedor");
    $stmt->bindValue(':fornecedor', $fornecedor);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == FORNECEDOR_ATIVO) {
        $situacao = FORNECEDOR_INATIVO;
    } elseif ($resultado['situacao'] == FORNECEDOR_INATIVO) {
        $situacao = FORNECEDOR_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbfornecedor set situacao = :situacao where idfornecedor = $fornecedor");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



