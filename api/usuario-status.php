<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$usuario = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $usuario = $_POST['usuario'];
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbusuario where idusuario = :usuario");
    $stmt->bindValue(':usuario', $usuario);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == USUARIO_ATIVO) {
        $situacao = USUARIO_INATIVO;
    } else {
        $situacao = USUARIO_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbusuario set situacao = :situacao where idusuario = $usuario");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



