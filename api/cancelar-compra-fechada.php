<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idcompra = '';
$retorno = array();
$retorno['parcelas'] = '';
$con = MyPdo::connect();
if ($_POST) {
    $idcompra = $_POST['idcompra'];
    try {
        $sql = "select idproduto, qtde_item from tbitem_compra where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    foreach ($itens as $item) {
        try {
            $sql = "select estoque from tbproduto where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->execute();
            $estoque = $stmt->fetch(PDO::FETCH_COLUMN);
        } catch (Exception $e) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
        try {
            $sql = "Update tbproduto set estoque = ($estoque - :qtde_item) where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->bindValue(':qtde_item', $item['qtde_item']);
            $stmt->execute();
        } catch (Exception $e) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
    }

    try {
        $sql = "select parcela_cp, idcompra, vlr_pago from tbcontaspagar where idcompra = :idcompra and (situacao = 'B' or situacao = 'P')";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
        $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($parcelas as $parcela) {
            $retorno['parcelas'][] = $parcela;
            try {
                $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                        . "descLancamento, idhistorico, inautomatico) values "
                        . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                $stmt = $con->prepare($sql);
                $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                $stmt->bindValue(':tipoLancamento', LANCAMENTO_CREDITO);
                $stmt->bindValue(':vlLancamento', $parcela['vlr_pago']);
                $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                $stmt->bindValue(':descLancamento', 'Reembolso parcela da compra #' . $idcompra);
                $stmt->bindValue(':idhistorico', 8);
                $stmt->bindValue(':inautomatico', 1);
                $stmt->execute();
            } catch (Exception $e) {
                $retorno['erro'] = 1;
                $retorno['erroMsg'] = $e;
            }
        }
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    try {
        $sql = "Update tbcontaspagar set situacao = '" . PARCELA_ESTORNADA . "' where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    try {
        $sql = "Update tbcompra set situacao = '" . COMPRA_CANCELADA . "' where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    echo json_encode($retorno);
}


