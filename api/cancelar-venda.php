<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idvenda = '';
$retorno = array();
$con = MyPdo::connect();
if ($_POST) {
    $idvenda = $_POST['idvenda'];

    try {
        $sql = "update tbvenda set situacao = " . VENDA_CANCELADA . " where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
        $retorno['sucesso'] = 1;
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    echo json_encode($retorno);
}
