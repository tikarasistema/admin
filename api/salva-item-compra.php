<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';
if ($_POST) {
    $con = MyPdo::connect();
    $idproduto = $_POST['idproduto'];
    $idcompra = $_POST['idcompra'];
    $qtde_item = $_POST['qtde_item'];
    $preco_item = $_POST['preco_item'];
    $retorno = array();
    define('CADASTRO', 1);
    define('UPDATE', 2);
    $retorno['tipo'] = '';
    $retorno['sucesso'] = '';
    $retorno['produto'] = '';
    $retorno['sucessoMsg'] = '';
    $retorno['erro'] = '';
    $retorno['erroMsg'] = '';

    $sql = "Select idproduto, idcompra from tbitem_compra where idproduto = :idproduto and idcompra = :idcompra";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcompra', $idcompra);
    $stmt->bindValue(':idproduto', $idproduto);
    $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_COLUMN);

    $sql = "Select estoque from tbproduto where idproduto = :idproduto ";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idproduto', $idproduto);
    $stmt->execute();
    $estoque = $stmt->fetch(PDO::FETCH_COLUMN);

    if ($resultado) {
        try {
            $sql = "Update tbitem_compra set qtde_item = :qtde_item , preco_item = :preco_item where idcompra = :idcompra and idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idcompra', $idcompra);
            $stmt->bindValue(':idproduto', $idproduto);
            $stmt->bindValue(':qtde_item', $qtde_item);
            $stmt->bindValue(':preco_item', $preco_item);
            $stmt->execute();

            $sql = "Select p.produto from tbitem_compra as i inner join"
                    . " tbproduto as p on i.idproduto = p.idproduto where i.idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $idproduto);
            $stmt->execute();
            $resultadoProdNome = $stmt->fetch(PDO::FETCH_COLUMN);

            $produto = array(
                'idcompra' => $idcompra,
                'idproduto' => $idproduto,
                'qtde_item' => $qtde_item,
                'preco_item' => $preco_item,
                'descricao' => $resultadoProdNome,
            );
            $retorno['tipo'] = UPDATE;
            $retorno['sucesso'] = 1;
            $retorno['produto'] = $produto;
            $retorno['sucessoMsg'] = 'Inserido com sucesso';

//                $sql = "Update tbproduto set estoque = :estoque where idproduto = :idproduto";
//                $stmt = $con->prepare($sql);
//                $stmt->bindValue(':idproduto', $idproduto);
//                $stmt->bindValue(':estoque', $estoque - $qtde_item);
//                $stmt->execute();
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erroMsg'] = 'erro ao inserir produto ' . $e;
        }
    } else {
        try {
            $sql = "Insert into tbitem_compra (idcompra, idproduto, qtde_item, preco_item) values (:idcompra, :idproduto, :qtde_item, :preco_item)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idcompra', $idcompra);
            $stmt->bindValue(':idproduto', $idproduto);
            $stmt->bindValue(':qtde_item', $qtde_item);
            $stmt->bindValue(':preco_item', $preco_item);
            $stmt->execute();

            $sql = "Select p.produto from tbitem_compra as i inner join"
                    . " tbproduto as p on i.idproduto = p.idproduto where i.idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $idproduto);
            $stmt->execute();
            $resultadoProdNome = $stmt->fetch(PDO::FETCH_COLUMN);

            $produto = array(
                'idcompra' => $idcompra,
                'idproduto' => $idproduto,
                'qtde_item' => $qtde_item,
                'preco_item' => $preco_item,
                'descricao' => $resultadoProdNome,
            );
            $retorno['tipo'] = CADASTRO;
            $retorno['sucesso'] = 1;
            $retorno['produto'] = $produto;
            $retorno['sucessoMsg'] = 'Inserido com sucesso ';

            $stmt->execute();
        } catch (Exception $e) {
            $retorno['erro'] = 1;
            $retorno['erroMsg'] = 'erro ao inserir produto ' . $e;
        }
    }
    echo json_encode($retorno);
}