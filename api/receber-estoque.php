<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idcompra = '';
$retorno['erro'] = '';
$retorno['msgErro'] = '';
$con = MyPdo::connect();
if ($_POST) {
    $idcompra = $_POST['idcompra'];

    $sql = "select idproduto, qtde_item from tbitem_compra where idcompra = :idcompra";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcompra', $idcompra);
    $stmt->execute();
    $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
 foreach ($itens as $item) {
        $sql = "select estoque from tbproduto where idproduto = :idproduto";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idproduto', $item['idproduto']);
        $stmt->execute();
        $estoque = $stmt->fetch(PDO::FETCH_COLUMN);

    }

    if ($retorno['erro'] != 1) {
        foreach ($itens as $item) {
            $sql = "Update tbproduto set estoque = ($estoque + :qtde_item) where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->bindValue(':qtde_item', $item['qtde_item']);
            $stmt->execute();
        }
        $sql = "Update tbcompra set situacao = " . COMPRA_FECHADA . " where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
    }

    echo json_encode($retorno);
}

