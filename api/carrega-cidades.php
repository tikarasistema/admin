<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$cidades = array();
$estado = $_POST['estado'];

//abrindo conexão
$pdo = new MyPdo;
$con = $pdo->connect();
////exemplo insert
//$insert = $con->prepare('INSERT INTO tbcliente (nome_razao) VALUES(:nome)');
//$insert->bindValue(":nome", "Marcelo");
//$insert->execute();
//
////exemplo select
$select = $con->query("Select id, nome from cidade where estado = $estado order by nome");


while ($row = $select->fetch(PDO::FETCH_ASSOC)) {
    $cidades[] = array(
        'id' => $row['id'],
        'nome' => $row['nome'],
    );
}

echo json_encode($cidades);

