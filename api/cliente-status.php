<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';
$cliente = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $cliente = $_POST['cliente'];
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbcliente where idcliente = :cliente");
    $stmt->bindValue(':cliente', $cliente);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == CLIENTE_ATIVO) {
        $situacao = CLIENTE_INATIVO;
    } elseif ($resultado['situacao'] == CLIENTE_INATIVO) {
        $situacao = CLIENTE_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbcliente set situacao = :situacao where idcliente = $cliente");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



