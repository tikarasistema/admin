<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idvenda = '';
$retorno['erro'] = '';
$retorno['msgErro'] = '';
$con = MyPdo::connect();
if ($_POST) {
    $idvenda = $_POST['idvenda'];

    $sql = "select idproduto, qtde_item from tbitem_venda where idvenda = :idvenda";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idvenda', $idvenda);
    $stmt->execute();
    $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($itens as $item) {
        $sql = "select estoque from tbproduto where idproduto = :idproduto";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idproduto', $item['idproduto']);
        $stmt->execute();
        $estoque = $stmt->fetch(PDO::FETCH_COLUMN);

        if ($estoque < $item['qtde_item']) {
            $retorno['erro'] = 1;
            $retorno['msgErro'][] = " Quantidade de estoque do produto #{$item['idproduto']} indisponível,<br><br> <strong> Quantidade disponível: {$estoque} </strong>";
        }
    }

    if ($retorno['erro'] != 1) {
        foreach ($itens as $item) {
            $sql = "Update tbproduto set estoque = ($estoque - :qtde_item) where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->bindValue(':qtde_item', $item['qtde_item']);
            $stmt->execute();
        }
        $sql = "Update tbvenda set situacao = " . VENDA_FECHADA . " where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
    }

    echo json_encode($retorno);
}

