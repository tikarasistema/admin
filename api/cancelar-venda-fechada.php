<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$idvenda = '';
$retorno = array();
$retorno['parcelas'] = '';
$con = MyPdo::connect();
if ($_POST) {
    $idvenda = $_POST['idvenda'];
    try {
        $sql = "select idproduto, qtde_item from tbitem_venda where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    foreach ($itens as $item) {
        try {
            $sql = "select estoque from tbproduto where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->execute();
            $estoque = $stmt->fetch(PDO::FETCH_COLUMN);
        } catch (Exception $e) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
        try {
            $sql = "Update tbproduto set estoque = ($estoque + :qtde_item) where idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idproduto', $item['idproduto']);
            $stmt->bindValue(':qtde_item', $item['qtde_item']);
            $stmt->execute();
        } catch (Exception $e) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
        }
    }

    try {
        $sql = "select parcela_cr, idvenda, vrl_recebido from tbcontasreceber where idvenda = :idvenda and (situacao = 'B' or situacao = 'P')";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
        $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($parcelas as $parcela) {
            $retorno['parcelas'][] = $parcela;
            try {
                $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                        . "descLancamento, idhistorico, inautomatico) values "
                        . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
                $stmt = $con->prepare($sql);
                $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
                $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
                $stmt->bindValue(':tipoLancamento', LANCAMENTO_DEBITO);
                $stmt->bindValue(':vlLancamento', $parcela['vrl_recebido']);
                $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
                $stmt->bindValue(':descLancamento', 'Estorno parcela da venda #' . $idvenda);
                $stmt->bindValue(':idhistorico', 6);
                $stmt->bindValue(':inautomatico', 1);
                $stmt->execute();
            } catch (Exception $e) {
                $retorno['erro'] = 1;
                $retorno['erroMsg'] = $e;
            }
        }
    } catch (Exception $e) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    try {
        $sql = "Update tbcontasreceber set situacao = '" . PARCELA_ESTORNADA . "' where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    try {
        $sql = "Update tbvenda set situacao = '" . VENDA_CANCELADA . "' where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($e, true) . "</pre>");
    }

    echo json_encode($retorno);
}


