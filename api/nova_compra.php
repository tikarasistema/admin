<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$con = MyPdo::connect();

if ($_POST) {
    $data_compra = date("Y-m-d H:i:s");
    $idfornecedor = $_POST['idfornecedor'];
    $situacao = COMPRA_ABERTA;
    $retorno['erro'] = '';
    $retorno['erroMsg'] = '';
    $retorno['sucesso'] = '';

    $sql = "Select idfornecedor from tbfornecedor where situacao = " . FORNECEDOR_ATIVO . " and idfornecedor = :idfornecedor";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idfornecedor', $idfornecedor);
    $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_COLUMN);
    if (!$resultado) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = '<strong>Fornecedor inválido ou inativo</strong>';
    }
    if (!$retorno['erro']) {
        try {
            $sql = "Insert into tbcompra (data_compra, idfornecedor, situacao, idusuario) values (:data_compra, :idfornecedor, :situacao, :idusuario)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':data_compra', $data_compra);
            $stmt->bindValue(':idfornecedor', $idfornecedor);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->execute();
            $retorno['sucesso'] = 1;
            $retorno['idcompra'] = $con->lastInsertId();
        } catch (Exception $ex) {
            $retorno['erro'] = 1;
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
        }
    }
    echo json_encode($retorno);
}


