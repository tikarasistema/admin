<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$marca = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $marca = getPost('marca');
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbmarca where idmarca = :marca");
    $stmt->bindValue(':marca', $marca);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == MARCA_ATIVA) {
        $situacao = MARCA_INATIVA;
    } else {
        $situacao = MARCA_ATIVA;
    }
    try {
        $stmt = $con->prepare("Update tbmarca set situacao = :situacao where idmarca = $marca");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



