<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

define('CODBARRA', 1);
define('DESCRICAO', 2);
$con = MyPdo::connect();
$metodo = '';
$desccod = '';
$produtos = array();
$retorno['produtos'] = array();
if ($_POST) {
    $metodo = $_POST['metodo'];
    $desccod = $_POST['produto'];

    if ($metodo == CODBARRA) {
        try {
            $sql = "Select * from tbproduto where codigobarra like '$desccod%' and situacao != " . PRODUTO_INATIVO;
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':desccod', $desccod);
            $stmt->execute();
            $produtos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $retorno['produtos'] = $produtos;
            $retorno['sucesso'] = 1;
        } catch (Exception $ex) {
            $retorno['erro'] = 1;
        }
    } elseif ($metodo == DESCRICAO) {
        try {
            $sql = "Select * from tbproduto where produto like '$desccod%' and situacao != " . PRODUTO_INATIVO;
            $stmt = $con->prepare($sql);
            $stmt->bindParam(':desccod', $desccod);
            $stmt->execute();
            $produtos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $retorno['produtos'] = $produtos;
            $retorno['sucesso'] = 1;
        } catch (Exception $ex) {
            $retorno['erro'] = 1;
        }
    }
}
echo json_encode($retorno);

