<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$classificacao = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $classificacao = getPost('classificacao');
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbtipo where idclassificacao = :classificacao");
    $stmt->bindValue(':classificacao', $classificacao);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == CLASSIFICACAO_ATIVO) {
        $situacao = CLASSIFICACAO_INATIVO;
    } else {
        $situacao = CLASSIFICACAO_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbtipo set situacao = :situacao where idclassificacao = $classificacao");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



