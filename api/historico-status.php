<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$historico = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $historico = getPost('historico');
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbhistorico where idhistorico = :historico");
    $stmt->bindValue(':historico', $historico);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == CONDPGTO_ATIVO) {
        $situacao = HISTORICO_ATIVO;
    } else {
        $situacao = HISTORICO_INATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbhistorico set situacao = :situacao where idhistorico = $historico");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



