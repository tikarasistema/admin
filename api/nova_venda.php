<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$con = MyPdo::connect();

if ($_POST) {
    $data_venda = date("Y-m-d H:i:s");
    $idcliente = $_POST['idcliente'];
    $situacao = VENDA_ABERTA;
    $retorno['erro'] = '';
    $retorno['erroMsg'] = '';
    $retorno['sucesso'] = '';

    $sql = "Select idcliente from tbcliente where situacao = " . CLIENTE_ATIVO . " and idcliente = :idcliente";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcliente', $idcliente);
    $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_COLUMN);
    if (!$resultado) {
        $retorno['erro'] = 1;
        $retorno['erroMsg'] = '<strong>Cliente inválido ou inativo</strong>';
    }
    if (!$retorno['erro']) {
        try {
            $sql = "Insert into tbvenda (data_venda, idcliente, situacao, idusuario) values (:data_venda, :idcliente, :situacao,:idusuario)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':data_venda', $data_venda);
            $stmt->bindValue(':idcliente', $idcliente);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->execute();
            $retorno['sucesso'] = 1;
            $retorno['idvenda'] = $con->lastInsertId();
        } catch (Exception $ex) {
            $retorno['erro'] = 1;
        }
    }
    echo json_encode($retorno);
}


