<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$papel = '';
$msg = array();
$situacao = '';
if ($_POST) {
    $papel = getPost('papel');
    $con = MyPdo::connect();

    $stmt = $con->prepare("select * from tbpapel where idpapel = :papel");
    $stmt->bindValue(':papel', $papel);
    $consulta = $stmt->execute();
    $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$resultado) {
        $retorno['erro'] = 1;
    } else {
        $retorno['erro'] = 0;
    }
    if ($resultado['situacao'] == PAPEL_ATIVO) {
        $situacao = PAPEL_INATIVO;
    } else {
        $situacao = PAPEL_ATIVO;
    }
    try {
        $stmt = $con->prepare("Update tbpapel set situacao = :situacao where idpapel = $papel");
        $stmt->bindValue(':situacao', $situacao);
        $stmt->execute();
    } catch (Exception $e) {
        echo $e;
    }
    echo json_encode($retorno);
}



