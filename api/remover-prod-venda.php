<?php

require '../lib/conexao.php';
require '../lib/config.php';
require '../lib/funcoes.php';
require '../lib/protege.php';

$con = MyPdo::connect();
$retorno['erro'] = 0;
$retorno['erro']['msg'];
$retorno['sucesso'] = 0;

if ($_POST) {
    $idprod = $_POST['idproduto'];
    $idvenda = $_POST['idvenda'];

    $sql = "Select * from tbitem_venda where idproduto = :idproduto and idvenda = :idvenda";

    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idvenda', $idvenda);
    $stmt->bindValue(':idproduto', $idprod);
    $stmt->execute();
    $consulta = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (!$consulta) {
        $retorno['erro'] = 1;
        $retorno['erro']['msg'] = "Item inexistente";
    }

    if ($retorno['erro'] != 1) {
        try {
            $sql = "Delete from tbitem_venda where idvenda = :idvenda and idproduto = :idproduto";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idvenda', $idvenda);
            $stmt->bindValue(':idproduto', $idprod);
            $stmt->execute();
            $retorno['sucesso'] = 1;
        } catch (Exception $exc) {
            $retorno['erro']['msg'] = "Erro ao excluir item " . $e;
            $retorno['erro'] = 1;
        }
    }
    echo json_encode($retorno);
}
