<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$idusuario = '';
$_SESSION['erro'] = false;
if (isset($_GET['cd'])) {
    $idusuario = $_GET['cd'];
    if ($idusuario != $_SESSION['usuario']['idusuario']) {
        editarErro(array(
            'page' => 'index.php',
            'origem' => 'usuário',
            'tipo' => 'Perfil inválido'
        ));
    }
    $sql = "Select * from tbusuario where idusuario = :idusuario";
} elseif (isset($_POST['idusuario'])) {
    $idusuario = $_POST['idusuario'];
} else {
    editarErro(array(
        'page' => 'usuarios.php',
        'origem' => 'perfil',
        'tipo' => 'Erro na atualização'
    ));
}

$sql = "Select * from tbusuario where idusuario = :idusuario";
$stmt = $con->prepare($sql);
$stmt->bindValue(':idusuario', $idusuario);
$stmt->execute();
$usuario = $stmt->fetch(PDO::FETCH_ASSOC);


$email = '';
$nome = '';
$senha = '';
$senha2 = '';
$tipo = '';


$stmt = $con->query("Select idpapel, dspapel from tbpapel where situacao = " . PAPEL_ATIVO);
$stmt->execute();
$papeisDiponiveis = $stmt->fetchAll(PDO::FETCH_ASSOC);

if ($_POST) {
    $nome = getPost('nome');
    $email = getPost('email');
    $senha = getPost('senha');
    $senha2 = getPost('senha2');
    $tipo = getPost('tipo');
    $idusuario = getPost('idusuario');



    if (empty($nome)) {
        addMessage("Preencha o nome");
        erro();
    }
    if (empty($senha)) {
        addMessage("Preencha a senha");
        erro();
    } elseif (strlen($senha) < 6) {
        addMessage("Senha deve ter mínimo de 6 dígitos");
        erro();
    }

    if ($senha != $senha2) {
        addMessage("O campo senha e repita senha devem ser iguais");
        erro();
    }
    if (empty($email)) {
        addMessage("Preencha o email");
        erro();
    }
    $con = MyPdo::connect();
    $sql = "Select UPPER(stemail) from tbusuario where stemail = UPPER(:stemail) and idusuario != :idusuario";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':stemail', $email);
    $stmt->bindValue(':idusuario', $idusuario);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($consulta) {
        addMessage('Email ja cadastrado, insira um email diferente');
        erro();
    }

    if (!getSession('erro')) {
        $senha = senha($senha);
        $db = MyPdo::connect();
        $prepare = $db->prepare('Update tbusuario set stnome = :stnome, stemail = :stemail'
                . ', stsenha = :stsenha where idusuario = :idusuario');
        $prepare->execute(array(
            ':stnome' => $nome,
            ':stemail' => $email,
            ':stsenha' => $senha,
            ':idusuario' => $idusuario,
        ));
        sucessInsertUpdate(array(
            'page' => 'index.php',
            'origem' => 'perfil',
            'tipo' => 'Atualização'
        ));
    }
} else {
    $nome = $usuario['stnome'];
    $email = $usuario['stemail'];
    $tipo = $usuario['intipo'];
    $situacao = $usuario['situacao'];
}
topo(array(
    'css' => array(
        "css/usuario/usuario.css",
    ),
    'pageName' => ' Editar perfil ',
    'icon' => 'fa fa-lock',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="alterarUsuarios" name="alterarUsuarios" role="form" method="post" action="perfil.php">
                    <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcliente">Nome*</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite nome " value="<?php echo $nome; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="email">Email*</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Digite email válido " value="<?php echo $email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="senha">Nova Senha*</label>
                                        <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite senha ">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <label for="senha2">Digite Senha Novamente*</label>
                                    <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Digite senha novamente ">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="alterarUsuarios">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="alterarUsuarios">Cancelar </button>

            </div>
        </div>
    </div>
</div>
<?php
rodape(array(
    'js' => array(
        'js/cliente/cliente.js',
    )
));

