<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$situacao = array();
$filtro = '';
if ($_POST) {
    $situacao = $_POST['optradio'];

    $situacaoPesquisar = join(',', $situacao);
    $filtro = '<br><br>Filtrado por: <br><strong>';
    foreach ($situacao as $situacaoFiltro) {
        switch ($situacaoFiltro) {
            case PRODUTO_ATIVO:
                $filtro .= '<br>Produto ativo<br>';
                break;
            case PRODUTO_INATIVO:
                $filtro .= '<br>Produto inativo<br>';
                break;
        }
    }
}
topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
        "css/relatorios/produto/produto.css"
    ),
    "pageName" => 'Relatório de estoque dos produtos ' . '<br><br> Dia atual: ' . date('d/m/Y') . $filtro . '</strong>',
    "icon" => 'fa fa-list'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title remover-print">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="gerarRelatorio" name="gerarRelatorio" role="form" method="post" action="estoque-produtos.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="pesquisa">Selecione situação dos produtos* </label>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array(PRODUTO_ATIVO, $situacao) ? 'checked' : ''; ?> type="checkbox" class="metodo" value="1" name="optradio[]">Ativo</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array(PRODUTO_INATIVO, $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="0" name="optradio[]">Inativo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php if ($_POST) {
                                    ?>
                                    <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if ($_POST) {
                    try {
                        $optradio = $_POST['optradio'];

                        $con = MyPdo::connect();
                        $sql = "select idproduto, produto, estoque from tbproduto where situacao in ($situacaoPesquisar) order by produto asc";
                        $stmt = $con->prepare($sql);
                        $stmt->execute();
                        $produtos = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    } catch (Exception $e) {
                        var_dump($e);
                        exit;
                    }
                    ?>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Estoque</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($produtos as $produto) {
                                ?>
                                <tr>
                                    <td> <?php echo $produto['idproduto']; ?></td>
                                    <td> <?php echo $produto['produto']; ?></td>
                                    <td> <?php echo $produto['estoque']; ?></td>
                                </tr>
                                <?php
                            }
                            if (!$produtos) {
                                echo '<tr> <td colspan=3> Sem itens para listagem </td><tr>';
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                <button type="reset" class="btn btn-danger"  form="gerarRelatorio">Cancelar </button>
                <?php if ($_POST) {
                    ?>
                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                <?php }
                ?>

            </div>
        </div>
    </div>
</div>
<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js",
        "js/relatorios/produto/produto.js",
    )
));
?>
