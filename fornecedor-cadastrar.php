
<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$nome = '';
$nomeFatansia = '';
$rgie = '';
$cpfcnpj = '';
$tipo = '';
$telefone = '';
$celular = '';
$email = '';
$logradouro = '';
$bairro = '';
$numero = '';
$complemento = '';
$cidade = 0;
$generico = '';
$estado = 0;
$estadosTeste = '';
$cidadesTeste = '';
$situacao = FORNECEDOR_ATIVO;
$_SESSION['erro'] = false;

$con = MyPdo::connect();
$sqlEstado = "SELECT id FROM estado";
$consulta = $con->query($sqlEstado);
$consulta->execute();

$sqlCidade = "SELECT id FROM cidade";
$consultaCidade = $con->query($sqlCidade);
$consultaCidade->execute();

$sqlCnpj = "Select cpf_cnpj from tbfornecedor";
$consultaCnpjCpf = $con->query($sqlCnpj);
$consultaCnpjCpf->execute();

$estados = $consulta->fetchAll(PDO::FETCH_COLUMN);
$cidades = $consultaCidade->fetchAll(PDO::FETCH_COLUMN);
$cnpjcpfs = $consultaCnpjCpf->fetchAll(PDO::FETCH_COLUMN);


if ($_POST) {

    $hoje = strtotime(date("Y-m-d 00:00:00"));
    $nome = getPost('fornecedor');
    $cpfcnpj = getPost('cpfcnpj');
    $rgie = getPost('rgie');
    $tipo = getPost('tipo');
    $telefone = getPost('telefone');
    $celular = getPost('celular');
    $email = getPost('email');
    $logradouro = getPost('logradouro');
    $bairro = getPost('bairro');
    $numero = getPost('numero');
    $estado = getPost('estado');
    $nomeFatansia = getPost('nomefantasia');

//Falta validar
    $complemento = getPost('complemento');
    $cidade = getPost('cidade');
    $generico = getPost('generico');

    if (!$generico) {
        if ($estado == 0) {
            addMessage("Selecione um estado");
            erro();
        }
        if ($cidade == 0) {
            addMessage("Selecione uma cidade");
            erro();
        }
        if (in_array($cpfcnpj, $cnpjcpfs)) {
            addMessage('Cpf ou cnpj de fornecedor ja cadastrado insira outro fornecedor');
            erro();
        }
        if (!in_array($tipo, $tipos)) {
            addMessage('Selecione um tipo válido');
            erro();
        }
        if (!in_array($estado, $estados)) {
            addMessage('Selecione um estado válido');
            erro();
        }
        if (!in_array($cidade, $cidades)) {
            addMessage('Selecione uma cidade válida');
            erro();
        }
        if ($tipo == PESSOA_FISICA) {
            $retorno = cpf($cpfcnpj);
            if (true !== $retorno) {
                addMessage('CPF inválido!');
                erro();
            }
        } elseif ($tipo == PESSOA_JURIDICA) {
            $retorno = cnpj($cpfcnpj);
            if (!$retorno) {
                addMessage('CNPJ inválido!');
                erro();
            }
        }

        if (empty($nome)) {
            addMessage("Preencha razão social");
            erro();
        }
        if (empty($nomeFatansia)) {
            addMessage("Preencha nome fantasia");
            erro();
        }

        if (emailValidate($email) != true) {
            addMessage('Email Inválido');
            erro();
        }
        if (false === numberValidate($numero)) {
            addMessage("Preencha campo número com números ");
            erro();
        }
    } else {
        if ($estado != 0) {
            if (!in_array($estado, $estados)) {
                addMessage('Selecione um estado válido');
                erro();
            }
        }
        if ($cidade != 0) {
            if (!in_array($cidade, $cidades)) {
                addMessage('Selecione uma cidade válida');
                erro();
            }
        }
        if (!empty($cpfcnpj)) {
            if (in_array($cpfcnpj, $cnpjcpfs)) {
                addMessage('Cpf ou cnpj de fornecedor já cadastrado! Insira outro fornecedor');
                erro();
            }
        }
        if (!empty($tipo)) {
            if (!in_array($tipo, $tipos)) {
                addMessage('Selecione um tipo válido');
                erro();
            }
        }
        if (!empty($estado)) {
            if (!in_array($estado, $estados)) {
                addMessage('Selecione um estado válido');
                erro();
            }
        }
        if (!empty($cidade)) {
            if (!in_array($cidade, $cidades)) {
                addMessage('Selecione uma cidade válida');
                erro();
            }
        }
        if (!empty($cpfcnpj)) {
            if ($tipo == PESSOA_FISICA) {
                $retorno = cpf($cpfcnpj);
                if (true !== $retorno) {
                    addMessage('CPF inválido!');
                    erro();
                }
            } elseif ($tipo == PESSOA_JURIDICA) {
                $retorno = cnpj($cpfcnpj);
                if (!$retorno) {
                    addMessage('CNPJ inválido!');
                    erro();
                }
            } else {
                addMessage('Informe tipo');
                erro();
            }
        }
        if (!empty($email)) {
            if (emailValidate($email) != true) {
                addMessage('Email Inválido');
                erro();
            }
        }
        if (!empty($numero)) {
            if (false === numberValidate($numero)) {
                addMessage("Preencha campo número com números ");
                erro();
            }
        }
        if (empty($nomeFatansia)) {
            addMessage("Preencha o campo nome fantasia");
            erro();
        }
    }
    if (!getSession('erro')) {
        if ($cidade == 0) {
            $cidade = null;
        }
        if ($estado == 0) {
            $estado = null;
        }
        try {
            $sql = "Insert into tbfornecedor (estado, nome_razao, cpf_cnpj, rg_ie, tipo, telefone, celular,"
                    . "email, logradouro, bairro, numero, complemento, cidade, situacao, nome_fantasia) values(:estado, :nome, :cpf_cnpj,"
                    . " :rg_ie,:tipo, :telefone, :celular, :email,"
                    . ":logradouro, :bairro, :numero, :complemento, :cidade, :situacao, :nome_fantasia)";
            $stmt = $con->prepare($sql);
            $stmt->bindParam(":nome", $nome);
            $stmt->bindParam(":cpf_cnpj", $cpfcnpj);
            $stmt->bindParam(":rg_ie", $rgie);
            $stmt->bindParam(":tipo", $tipo);
            $stmt->bindParam(":telefone", $telefone);
            $stmt->bindParam(":celular", $celular);
            $stmt->bindParam(":email", $email);
            $stmt->bindParam(":logradouro", $logradouro);
            $stmt->bindParam(":bairro", $bairro);
            $stmt->bindParam(":numero", $numero);
            $stmt->bindParam(":complemento", $complemento);
            $stmt->bindParam(":cidade", $cidade);
            $stmt->bindParam(":situacao", $situacao);
            $stmt->bindParam(":nome_fantasia", $nomeFatansia);
            $stmt->bindParam(":estado", $estado);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'fornecedores.php',
                'origem' => 'fornecedor',
                'tipo' => 'Cadastro'
            ));
//            addMessage("Cadastro realizado com sucesso");
        } catch (Exception $e) {
            addMessage("Erro ao cadastrar");
            $_SESSION['erro'] = true;
            throw new Exception("Erro ao cadastrar" . $e);
        }
    }
}
?>


<?php
topo(array(
    'css' => array(
        "css/fornecedor/fornecedor.css",
    ),
    'pageName' => ' Cadastrar Fornecedor',
    'icon' => 'fa fa-truck',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarFornecedor" name="cadastrarFornecedor" role="form" method="post" action="fornecedor-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label> <input form="cadastrarFornecedor" type="checkbox" name="generico" <?php echo $generico ? 'checked' : ''; ?>  > Cadastro Genérico?</label>

                                    </div>
                                    <div class="help-block" >Ao selecionar esta opção, somente os campos preenchidos serão validados.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="ffornecedor">Nome Fantasia*</label>
                                    <input type="text" class="form-control" id="ffornecedor" name="nomefantasia" placeholder="Nome fantasia " value="<?php echo $nomeFatansia; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="ffornecedor">Razão Social*</label>
                                    <input type="text" class="form-control" id="ffornecedor" name="fornecedor" placeholder="Razão social " value="<?php echo $nome; ?>">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-6  col-md-6">
                                    <label for="ftipo">Tipo*</label>
                                    <select class="form-control" name="tipo" id="ftipo">
                                        <option value="0">Selecione</option>
                                        <option value="1" <?php echo $tipo == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                        <option value="2" <?php echo $tipo == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="fcpfcnpj">CPF / CNPJ*</label>
                                        <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Somente números " value="<?php echo $cpfcnpj; ?>">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <label for="frgie">RG / IE*</label>
                                <input type="text" class="form-control" id="frgie" name="rgie" placeholder="Somente números "  value="<?php echo $rgie; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="ftelefone">Telefone*</label>
                                    <input type="text" data-mask="(00) 0000-0000" class="form-control" id="ftelefone" name="telefone" value="<?php echo $telefone; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fcelular">Celular*</label>
                                    <input type="text" class="form-control" id="fcelular" name="celular" value="<?php echo $celular; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="fcelular">Email*</label>
                                    <input type="email" class="form-control" id="femail" name="email" placeholder="email@exemple.com" value="<?php echo $email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Endereço</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="flogradouro">Logradouro*</label>
                                            <input type="text" name="logradouro" id="flogradouro" class="form-control" value="<?php echo $logradouro; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fnumero">Bairro*</label>
                                            <input type="text" name="bairro" id="fbairro" class="form-control" value="<?php echo $bairro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-4">
                                        <div class="form-group">
                                            <label for="fnumero">Número*</label>
                                            <input type="number" name="numero" id="fnumero" class="form-control" value="<?php echo $numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="form-group">
                                            <label for="fcomplemento">Complemento</label>
                                            <input type="text" name="complemento" id="fcomplemento" class="form-control" value="<?php echo $complemento; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="festado">Estado* </label>
                                            <select class="form-control" id="festado" name="estado">
                                                <option value="0">Escolha o estado </option>
                                                <?php
                                                $sql = "SELECT id, nome FROM estado order by nome";
                                                $consulta = $con->query($sql);
                                                $consulta->execute();
                                                while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $resultado['id']; ?>"
                                                            <?php if ($resultado == $resultado['id']) { ?> selected <?php } ?>
                                                            ><?php echo $resultado['nome']; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fcidade">Cidade* </label>
                                            <select class="form-control" id="fcidade" name="cidade">
                                                <option value="3159">--Selecione o Estado --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarFornecedor">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarFornecedor">Cancelar </button>

            </div>
        </div>
    </div>
</div>
<script>
    var cidade = '';
</script>
<?php
rodape(array(
    'js' => array(
        'js/fornecedor/fornecedor.js',
    )
));
?>
