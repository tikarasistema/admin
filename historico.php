<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$erro = '';
$where = '';
$pagina = getGet('p', 1);
$query = getGet('q', '');
$editar = true;
try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('historico-editar.php', $telasPermissao)) {
    $editar = false;
}
$where = "where inautomatico != '1' or inautomatico is null ";

if ($query) {
    $where .= " and dshistorico like '$query%' ";
}

$totalRegPag = "10";

$inicio = ($pagina - 1) * $totalRegPag;

$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS * from tbhistorico {$where} LIMIT $inicio,$totalRegPag ");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$historicos = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$historicos) {
    $erro = 'Sem históricos para listagem';
}
topo(array(
    "css" => array(
        "css/historico/historico.css"
    ),
    "icon" => "fa fa-signal",
    "pageName" => " Pesquisar Históricos"
));
?>
<div class="modal fade" id="myModal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Alterar status</h4>
            </div>
            <div class="modal-body">
                <div class="mensagem">

                </div>
                <h3>Deseja alterar status?</h3>
            </div>
            <div class="modal-footer">
                <button id="btnAlterarStatus" type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>

    </div>
</div>
<div class="erroStatus">

</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="">
                    <div class="col-xs-11">
                        <div class="form-group">
                            <label class="sr-only" for="fquery">Pesquisa</label>
                            <input value="<?php echo $query; ?>" type="search" class="form-control" id="fquery" name="q" placeholder="Pesquisar">
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a  href="historico.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                    </div>
                    <div class="col-xs-1">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class=" fa fa-plus fa-fw"></i>
                            </button>
                            <ul class="dropdown-menu slidedown">
                                <li>
                                    <a href="historico-cadastrar.php">
                                        <i class="fa fa-signal"></i> Novo Histórico
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Situação</th>
                                <th>Descrição</th>
                                <th>Tipo</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($historicos as $historico) { ?>
                                <tr>
                                    <td> <?php echo $historico['idhistorico']; ?></td>
                                    <td> <?php
                                        if ($historico['situacao'] == HISTORICO_ATIVO) {
                                            if ($editar) {
                                                ?>
                                                <a href="#" data-id="<?php echo $historico['idhistorico']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs situacao">ativo</a>
                                            <?php } else {
                                                ?>
                                                <a href="#" class="btn btn-success btn-xs situacao">ativo</a>
                                                <?php
                                            }
                                        } else {
                                            if ($editar) {
                                                ?>
                                                <a href="#" data-id="<?php echo $historico['idhistorico']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-xs situacao">inativo</a>
                                            <?php } else {
                                                ?>
                                                <a href="#"class="btn btn-danger btn-xs situacao">inativo</a>
                                                <?php
                                            }
                                        }
                                        ?></td>
                                    <td> <?php echo $historico['dshistorico']; ?></td>
                                    <td> <?php echo $historico['tipohistorico'] == LANCAMENTO_CREDITO ? 'Crédito' : 'Débito'; ?> </td>
                                    <td>
                                        <a title = "Editar Histórico" href="historico-editar.php?cd=<?php echo $historico['idhistorico']; ?>"><i class="fa fa-edit"></i></a>
                                    </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="historico.php?p=<?php echo $anterior; ?>&q=<?php echo $query; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="historico.php?p=<?php echo $proximo; ?>&q=<?php echo $query; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>



<?php
rodape(array(
    "js" => array(
        "js/historico/historico.js",
    )
));
?>
<script>
    var query = '<?php echo $query; ?>';
</script>

