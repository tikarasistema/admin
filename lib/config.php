<?php

define('DIRETORIO', realpath('.')); //define caminho completo

define('URL', 'http://estagio.dev/admin');
/*
  Temas: default, bootstrap

  Temas bootswatch: cosmo, cyborg, darkly, journal, readable, sandstone, simplex, slate, superhero, yeti,paper
  Mais temas em http://bootswatch.com/
 */

define('RECEBER', 1);
define('PAGAR', 2);

define('PARCELA_ABERTA', 'A');
define('PARCELA_BAIXADA', 'B');
define('PARCELA_ESTORNADA', 'E');
define('PARCELA_CARTAO', 'C');
define('PARCELA_BAIXADA_PARCIAL', 'P');
define('PARCELA_BAIXADA_PARCIAL_ESTORNO', 'M');
define('PARCELA_BAIXADA_PARCIAL_CARTAO', 'L');
define('PARCELA_VENCIDA', 'V');

define('AMORTIZACAO_BAIXA', 1);
define('AMORTIZACAO_ESTORNO', 2);
define('AMORTIZACAO_BAIXA_CARTAO', 3);

define('CLASSIFICACAO_INATIVO', '0');
define('CLASSIFICACAO_ATIVO', '1');

define('MARCA_INATIVA', '0');
define('MARCA_ATIVA', '1');

define('CLIENTE_INATIVO', '0');
define('CLIENTE_ATIVO', '1');

define('PRODUTO_INATIVO', '0');
define('PRODUTO_ATIVO', '1');
define('PRODUTO_MANUTENCAO', '2');

define('USUARIO_INATIVO', '0');
define('USUARIO_ATIVO', '1');

define('VENDA_ABERTA', '1');
define('VENDA_FECHADA', '0');
define('VENDA_CANCELADA', '2');

define('COMPRA_ABERTA', '1');
define('COMPRA_FECHADA', '0');
define('COMPRA_CANCELADA', '2');

define('EMPRESA', 'Realize Presentes');

define('PESSOA_FISICA', '1');
define('PESSOA_JURIDICA', '2');

define('SEXO_MASCULINO', '1');
define('SEXO_FEMININO', '2');

define('DATE_BRASIL', '1');
define('DATE_AMERICA', '2');

define('PAPEL_INATIVO', '0');
define('PAPEL_ATIVO', '1');

define('FORNECEDOR_INATIVO', '0');
define('FORNECEDOR_ATIVO', '1');

define('CONDPGTO_INATIVO', '0');
define('CONDPGTO_ATIVO', '1');

define('LANCAMENTO_CREDITO', '1');
define('LANCAMENTO_DEBITO', '2');

define('HISTORICO_INATIVO', '0');
define('HISTORICO_ATIVO', '1');

define('LANCAMENTO_AUTOMATICO', 1);
define('LANCAMENTO_ABERTURA_CAIXA', 1);

$tiposLancamentos = array(
    LANCAMENTO_CREDITO,
    LANCAMENTO_DEBITO
);

$tipos = array(
    'Pessoa_fisica' => PESSOA_FISICA,
    'Pessoa_juridica' => PESSOA_JURIDICA
);

$sexos = array(
    'Masculino' => SEXO_MASCULINO,
    'Feminino' => SEXO_FEMININO
);

