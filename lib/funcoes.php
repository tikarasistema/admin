<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
date_default_timezone_set("Brazil/East");

function logado() {
    $_SESSION['logado'] = 1;
}

$temas = array(
    "cosmo", "cyborg", "darkly", "journal", "readable", "sandstone", "simplex", "slate", "superhero", "yeti", "paper"
);
$userTema = '';
if (isset($_SESSION['usuario']['tema'])) {
    $userTema = $_SESSION['usuario']['tema'];
}
$tema = ($userTema) ? $userTema : 'paper';
define('TWITTER_BOOTSTRAP_TEMA', $tema);

/**
 *
 * @param array $email= email para qual será enviado, $senha=novasenha
 */
function lembrarSenha($senha, $email) {
    // Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
    require_once realpath("lib/phpmailer/PHPMailerAutoload.php");
// Inicia a classe PHPMailer
    $mail = new PHPMailer();
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
    $mail->Host = "smtp.gmail.com"; // Endereço do servidor SMTP
    $mail->Port = '587';
    $mail->Username = 'hivecommerce@gmail.com'; // Usuário do servidor SMTP
    $mail->Password = 'hive123456'; // Senha do servidor SMTP
// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->From = "hivecommerce@gmail.com"; // Seu e-mail
    $mail->Sender = "hivecommerce@gmail.com";
    $mail->FromName = "Lembrete senha"; // Seu nome
// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    $mail->AddAddress("$email", 'Usuario');
    //$mail->AddAddress("guilhermerodriguestb@gmail.com");
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->Subject = "Alteracao de senha"; // Assunto da mensagem
    $mail->Body = "<h3>Nova senha para login no HiveCommerce :</h3><br><strong>$senha</strong>";
//$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";
// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo
// Envia o e-mail
    $enviado = $mail->Send();

// Limpa os destinatários e os anexos
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
// Exibe uma mensagem de resultado
    return $enviado;
}

function gerarParcela($datavenda, $dias) {
    $data = 30 * $dias;
    return date('Y-m-d', strtotime("+$data days", strtotime($datavenda)));
}

function headCss() {
    ?>
    <link rel="shortcut icon" type="image/x-icon" href="img/logo.ico" />
    <link href="./lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <?php headCssTema(TWITTER_BOOTSTRAP_TEMA); ?>
    <link href="./lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="./css/pace.css" rel="stylesheet" type="text/css"/>
    <link href="./lib/toggle/toggle.css" rel="stylesheet" type="text/css"/>
    <link href="./css/estilos.css" rel="stylesheet"><?php
}

function acessoNegado() {
    ?>
    <link href="./lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <div class="alert alert-danger alert-dismissable">
        <ul>
            <?php
            $voltar = '';
            if (isset($_SERVER['HTTP_REFERER'])) {
                $voltar = '<a href=' . $_SERVER['HTTP_REFERER'] . '> Clique aqui para voltar</a>';
            }
            ?>
            <li><?php
                echo "Usuário não tem permissão de acesso. $voltar";
                ?></li>
        </ul>
    </div>
    <?php
}

function headCssTema($tema = null) {
    $href = '';
    switch ($tema) {
        case 'bootstrap';
            $href = './lib/bootstrap/css/bootstrap-theme.min.css';
            break;

        case 'cosmo':
        case 'cyborg':
        case 'darkly':
        case 'journal':
        case 'readable':
        case 'sandstone':
        case 'simplex':
        case 'slate':
        case 'superhero':
        case 'yeti':
        case 'paper':
            $href = "./lib/bootswatch/$tema/bootstrap.min.css";
            break;
    }

    if ($href) {
        ?><link href="<?php echo $href; ?>" rel="stylesheet"><?php
    }
}

function cpf($input) {
// Code ported from jsfromhell.com
    $c = preg_replace('/\D/', '', $input);
    if (strlen($c) != 11 || preg_match("/^{$c[0]}{11}$/", $c)) {
        return false;
    }
    for ($s = 10, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--)
        ;
    if ($c[9] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
        return false;
    }
    for ($s = 11, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--)
        ;
    if ($c[10] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
        return false;
    }
    return true;
}

/**
 * Valida CNPJ
 *
 * @author Luiz Otávio Miranda <contato@todoespacoonline.com/w>
 * @param string $cnpj
 * @return bool true para CNPJ correto
 *
 */
function cnpj($cnpj) {
// Deixa o CNPJ com apenas números
    $cnpj = preg_replace('/[^0-9]/', '', $cnpj);

// Garante que o CNPJ é uma string
    $cnpj = (string) $cnpj;

// O valor original
    $cnpj_original = $cnpj;

// Captura os primeiros 12 números do CNPJ
    $primeiros_numeros_cnpj = substr($cnpj, 0, 12);

    /**
     * Multiplicação do CNPJ
     *
     * @param string $cnpj Os digitos do CNPJ
     * @param int $posicoes A posição que vai iniciar a regressão
     * @return int O
     *
     */
    if (!function_exists('multiplica_cnpj')) {

        function multiplica_cnpj($cnpj, $posicao = 5) {
// Variável para o cálculo
            $calculo = 0;

// Laço para percorrer os item do cnpj
            for ($i = 0; $i < strlen($cnpj); $i++) {
// Cálculo mais posição do CNPJ * a posição
                $calculo = $calculo + ( $cnpj[$i] * $posicao );

// Decrementa a posição a cada volta do laço
                $posicao--;

// Se a posição for menor que 2, ela se torna 9
                if ($posicao < 2) {
                    $posicao = 9;
                }
            }
// Retorna o cálculo
            return $calculo;
        }

    }

// Faz o primeiro cálculo
    $primeiro_calculo = multiplica_cnpj($primeiros_numeros_cnpj);

// Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
// Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
    $primeiro_digito = ( $primeiro_calculo % 11 ) < 2 ? 0 : 11 - ( $primeiro_calculo % 11 );

// Concatena o primeiro dígito nos 12 primeiros números do CNPJ
// Agora temos 13 números aqui
    $primeiros_numeros_cnpj .= $primeiro_digito;

// O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
    $segundo_calculo = multiplica_cnpj($primeiros_numeros_cnpj, 6);
    $segundo_digito = ( $segundo_calculo % 11 ) < 2 ? 0 : 11 - ( $segundo_calculo % 11 );

// Concatena o segundo dígito ao CNPJ
    $cnpj = $primeiros_numeros_cnpj . $segundo_digito;

// Verifica se o CNPJ gerado é idêntico ao enviado
    if ($cnpj === $cnpj_original) {
        return true;
    }
}

function senha($senha) {
    return md5('estagio' . $senha . '2016'); //colocando concatenacao fica dificil descobrir a criptografia
    return ($senha);
}

function redimensionarFigura($origem, $destino, $largura, $altura) {
    $imgorigem = imagecreatefromjpeg($origem);
    $w = imagesx($imgorigem);
    $h = imagesy($imgorigem);
    $imgdestino = imagecreatetruecolor($largura, $altura);
    imagecopyresampled($imgdestino, $imgorigem, 0, 0, 0, 0, $largura, $altura, $w, $h);
    imagejpeg($imgdestino, $destino, 80);
    imagedestroy($imgdestino);
}

function emailValidate($email) {
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    }
}

/**
 *
 * @param type $data Data para conversão
 * @param type $local DATE_BRASIL, DATE_AMERICA
 * @return type
 */
function formatDate($data, $local) {
    if ($local == DATE_AMERICA) {
        return join("-", (array_reverse(explode('/', $data))));
    }
    if ($local == DATE_BRASIL) {
        return join("/", (array_reverse(explode('-', $data))));
    }
}

function numberValidate($num) {
    $teste = is_numeric($num);
    if ($teste != true) {
        return false;
    } else {
        return true;
    }
}

function getSession($name, $default = null) {
    return (isset($_SESSION[$name])) ? $_SESSION[$name] : $default;
}

function getPost($name, $default = '') {
    return (isset($_POST[$name])) ? trim($_POST[$name]) : $default;
}

function getGet($name, $default = '') {
    return (isset($_GET[$name])) ? trim($_GET[$name]) : $default;
}

function pace() {
    ?><div id="pace"></div> <?php
}

/**
 *
 * @param type $options = css, js, se a tela é login, nome da pagina e icone
 */
function topo($options) {
    ?>
    <!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title><?php echo isset($option['pageName']) ? $option['pageName'] : ''; ?></title>
            <?php headCss(); ?>
            <?php
            if (isset($options['css'])) {
                foreach ($options['css']as $css) {
                    echo "<link href=$css rel='stylesheet'>";
                }
            }
            ?>
            <link rel="icon" type="image/png" href="lib/img/iconehive.png" />
        </head>
        <body>
            <?php if (!(isset($options['login']))) { ?>
                <?php
                include 'nav.php';
                pace();
            }
            ?>


            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-header">
                            <h4><i class="<?php echo isset($options['icon']) ? $options['icon'] : ''; ?>"></i> <?php echo isset($options['pageName']) ? $options['pageName'] : ''; ?></h4>
                        </div>
                    </div>
                </div>
                <div id="modalegg"></div>
                <div id="modalcaixa"></div>
                <div id="modallancamento"></div>
                <?php
                if (getSession('erro')) {
                    echo getMessage('danger');
                    $_SESSION['erro'] = null;
                } else {
                    echo getMessage('success');
                    $_SESSION['erro'] = null;
                }
            }
            ?>
            <?php

            function rodape($options) {
                ?>

            </div>
            <script> var URL = "<?php echo URL; ?>";</script>
            <script src = "js/jquery.js" ></script>
            <script src="js/maskMoney/jquery.maskMoney.js"></script>
            <script src="js/momentjs.js"></script>
            <script src="js/moment.js"></script>
            <script src="lib/bootstrap/js/bootstrap.min.js"></script>
            <?php if (!isset($options['login'])) { ?>
                <script src="js/pace.js" type="text/javascript" ></script>
            <?php } ?>
            <script data-pace-options='{"restartOnRequestAfter": 20, "target": "#pace", "ajax": true}'></script>
            <script src="js/mask.min.js" type="text/javascript"></script>
            <script src="js/formatar_moeda.js" type="text/javascript"></script>
            <script src="js/easterEgg/easteregg.js" type="text/javascript"></script>
            <script src="js/atalhos/shortcut.js" type="text/javascript"></script>
            <script src="js/sistema.js" type="text/javascript"></script>





            <?php
            if (isset($options['js'])) {
                foreach ($options['js'] as $js) {
                    echo "<script src=$js type='text/javascript'></script>";
                }
            }
            ?>
        </body>
    </html>
    <?php
}

function addMessage($msg) {
    $_SESSION['msg'][] = $msg;
}

function getMessage($alert) {
    if (count(getSession('msg')) > 0) {
        ?>
        <div class="alert alert-<?php echo $alert; ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul>
                <?php foreach ($_SESSION['msg'] as $msg) { ?>
                    <li><?php echo $msg; ?></li>
                <?php } ?>
            </ul>
        </div>
        <?php
        $_SESSION['msg'] = array();
    }
}

function erro() {
    $_SESSION['erro'] = true;
}

/**
 *
 * @param array $options['page'] = Pagina a ser renderizada após salvar, $options['origem'] qual 'objeto' foi salvo, $options['tipo'] = 'atualização' ou 'cadastro'
 *
 * ex: salvar cliente - $options['page'] = 'clientes.php' $options['origem'] = cliente $options['tipo'] = 'cadastro'
 */
function sucessInsertUpdate(array $options) {
    ?>

    <div class="modal fade" id="modalUpdateInsert" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $options['tipo'] . ' de ' . $options['origem']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="mensagem">

                    </div>
                    <h4><?php echo $options['tipo'] . ' de ' . $options['origem'] . ' realizado com sucesso ' ?></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="closeModalUpdateInsert">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <link href="./lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script>
                var openPage = '<?php echo $options['page']; ?>';
                $(function() {
                    $('#modalUpdateInsert').modal({
                        show: 'true'
                    });
                });
                $('#closeModalUpdateInsert').on('click', function() {
                    window.location.href = openPage;
                });
    </script>


    <?php
}

/**
 *
 * @param array $options['page'] = Pagina a ser renderizada após erro, $options['origem'] objeto a ser atualizado, $options['tipo'] = 'erro atualização'
 *
 * ex: erro editar cliente - $options['page'] = 'clientes.php' $options['origem'] = cliente $options['tipo'] = 'erro atualização cliente'
 */
function editarErro(array $options) {
    ?>

    <div class="modal fade" id="modalEditarErro" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $options['tipo']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="mensagem">

                    </div>
                    <?php if (isset($options['origem'])) { ?>
                        <h4><?php echo $options['tipo'] . ' de ' . $options['origem']; ?></h4>
                    <?php } else { ?>
                        <h4><?php echo $options['tipo']; ?></h4>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="closeModalEditarErro">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <link href="./lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script>
                var openPage = '<?php echo $options['page']; ?>';
                $(function() {
                    $('#modalEditarErro').modal({
                        show: 'true'
                    });
                });
                $('#closeModalEditarErro').on('click', function() {
                    window.location.href = openPage;
                });
    </script>


    <?php
}

function stringValidate($string) {
    return $resultado = preg_match('/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/', $string);
}
