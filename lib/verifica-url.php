<?php

require 'telasPapel.php';

function permissaoAcesso() {

    $telasPapel = pegarTelasPapel();

    $uri = explode('?', $_SERVER['REQUEST_URI']);
    $request = explode('/', ltrim($uri[0], '/'));
    $pagina = end($request);

    if (!in_array($pagina, $telasPapel)) {
        return false;
    } else {
        return true;
    }
}
