<?php

function pegarTelasPapel() {
    try {
        $con = MyPdo::connect();
        $sql = 'Select telas from tbpapel where idpapel = :idpapel';
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
        $stmt->execute();
        $telas = $stmt->fetch(PDO::FETCH_COLUMN);
    } catch (Exception $e) {
        print_r($e);
    }
    return explode(',', $telas);
}
