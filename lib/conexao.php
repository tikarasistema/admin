<?php

require 'config-bd.php';

class MyPdo
{

    public static $connection = null;

    /**
     *
     * @return PDO
     */
    public static function connect()
    {
        if (!self::$connection) {
            self::$connection = new PDO("mysql:host=" . BD_HOST . ";dbname=" . BD_NOME . ';charset=utf8', BD_USUARIO, BD_SENHA);
            self::$connection->exec("SET CHARACTER SET utf8");
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$connection;
    }

}
