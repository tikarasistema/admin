<?php

$telas = array(
    'usuario' => array(
        'titulo' => 'Usuário',
        'telas' => array(
            'usuarios-cadastrar.php' => 'Cadastrar',
            'usuarios-editar.php' => 'Editar',
            'usuarios.php' => 'Listar'
        )
    ),
    'classificacao' => array(
        'titulo' => 'Classificação',
        'telas' => array(
            'classificacao-cadastrar.php' => 'Cadastrar',
            'classificacao-editar.php' => 'Editar',
            'classificacoes.php' => 'Listar',
        )
    ),
    'cliente' => array(
        'titulo' => 'Cliente',
        'telas' => array(
            'clientes-cadastrar.php' => 'Cadastrar',
            'clientes-editar.php' => 'Editar',
            'clientes.php' => 'Listar',
        )
    ),
    'produto' => array(
        'titulo' => 'Produto',
        'telas' => array(
            'produtos-cadastrar.php' => 'Cadastrar',
            'produtos-editar.php' => 'Editar',
            'produtos.php' => 'Listar',
        )
    ),
    'papel' => array(
        'titulo' => 'Papel',
        'telas' => array(
            'papeis-cadastrar.php' => 'Cadastrar',
            'papeis-editar.php' => 'Editar',
            'papeis.php' => 'Listar',
        )
    ),
    'marca' => array(
        'titulo' => 'Marca',
        'telas' => array(
            'marca-cadastrar.php' => 'Cadastrar',
            'marca-editar.php' => 'Editar',
            'marcas.php' => 'Listar'
        )
    ),
    'fornecedor' => array(
        'titulo' => 'Fornecedor',
        'telas' => array(
            'fornecedor-cadastrar.php' => 'Cadastrar',
            'fornecedor-editar.php' => 'Editar',
            'fornecedores.php' => 'Listar'
        )
    ),
    'condicaopagamento' => array(
        'titulo' => 'Condição Pagamento',
        'telas' => array(
            'condicaopgto-cadastrar.php' => 'Cadastrar',
            'condicaopgto-editar.php' => 'Editar',
            'condicaopgto.php' => 'Listar'
        )
    ),
    'perfil' => array(
        'titulo' => 'Perfil de usuário',
        'telas' => array(
            'perfil.php' => 'Editar',
        )
    ),
    'historico' => array(
        'titulo' => 'Histórico',
        'telas' => array(
            'historico.php' => 'Listar',
            'historico-cadastrar.php' => 'Cadastrar',
            'historico-editar.php' => 'Editar',
        )
    ),
    'lancamento' => array(
        'titulo' => 'Lançamento caixa',
        'telas' => array(
            'lancamento-conta.php' => 'Cadastar',
            'lancamento-conta-editar.php' => 'Editar',
            'lancamentos.php' => 'Listar',
        )
    ),
    'relatorios' => array(
        'titulo' => 'Relatórios',
        'telas' => array(
            'caixa.php' => 'Caixa',
            'produto-mais-vendido-meses.php' => 'Produtos mais vendidos x meses ou dias',
            'estoque-produtos.php' => 'Estoque produto',
            'quantidade-venda-periodo.php' => 'Quantidade de vendas x meses ou dias',
            'relatorio-receber.php' => 'Gerar relatório a receber por cliente',
            'relatorio-receber-geral.php' => 'Gerar relatório a receber todos clientes',
            'relatorio-pagar.php' => 'Gerar relatório a pagar',
            'relatorio-pagar-geral.php' => 'Gerar relatório a pagar todos fornecedores',
        )
    ),
    'vendas' => array(
        'titulo' => 'Vendas',
        'telas' => array(
            'venda-aberta.php' => 'Efetuar venda',
            'vendas.php' => 'Listar',
            'venda-fechada.php' => 'Cancelar venda',
            'venda-cancelada.php' => 'Exibir venda cancelada',
        )
    ),
    'compras' => array(
        'titulo' => 'Compras',
        'telas' => array(
            'compra-aberta.php' => 'Efetuar compra',
            'compras.php' => 'Listar',
            'compra-fechada.php' => 'Cancelar compra',
            'compra-cancelada.php' => 'Exibir compra cancelada',
        )
    ),
    'receber' => array(
        'titulo' => 'Contas à receber',
        'telas' => array(
            'receber.php' => 'Listar',
            'receber-parcela.php' => 'Receber parcela',
            'estornar-parcela.php' => 'Estornar parcela',
            'boleto-receber.php' => 'Gerar boleto',
        )
    ),
    'pagar' => array(
        'titulo' => 'Contas à pagar',
        'telas' => array(
            'pagar.php' => 'Listar',
            'pagar-parcela.php' => 'Pagar parcela',
            'estornar-parcela-pagar.php' => 'Estornar parcela paga',
            'boleto-pagar.php' => 'Gerar boleto',
        )
    ),
    'tema' => array(
        'titulo' => 'Temas',
        'telas' => array(
            'tema.php' => 'Alterar tema',
        )
    ),
);

