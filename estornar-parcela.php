<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$_SESSION['erro'] = false;

$parcela_cr = getGet('parcela_cr', 0);
$idvenda = getGet('idvenda', 0);
$valorEstorno = '';
$parcela = array();

if ($parcela_cr == 0 or $idvenda == 0) {
    editarErro(array(
        'page' => 'vendas.php',
        'origem' => 'a receber',
        'tipo' => 'Erro no recebimento'
    ));
}

function valorPago($parcela, $con) {
    try {
        $sql = "SELECT sum(vlr_pago) as vlr_pago FROM tbamortizacao where idvenda = " . $parcela['idvenda'] . " and parcela_cr = " . $parcela['parcela_cr'];
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $vlr_pago = $stmt->fetch(PDO::FETCH_COLUMN);
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
    }
    return $vlr_pago < 0 ? 0 : $vlr_pago;
}

try {
    $sql = "Select * from tbcontasreceber where idvenda = :idvenda and parcela_cr = :parcela_cr ORDER BY idvenda DESC, parcela_cr ASC";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idvenda', $idvenda);
    $stmt->bindValue(':parcela_cr', $parcela_cr);
    $stmt->execute();
    $parcela = $stmt->fetch(PDO::FETCH_ASSOC);
} catch (Exception $ex) {
    print_r($e);
}
if ($parcela['vlr_parcela'] < 1) {
    sucessInsertUpdate(array(
        'page' => 'clientes.php',
        'origem' => 'cliente',
        'tipo' => 'Recebimento de parcela'
    ));
}


if ($_POST) {
    $valorEstorno = $_POST['valorEstorno'];
    $valorPago = $_POST['valorPago'];

    if ($valorEstorno > $valorPago) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'estorno',
            'tipo' => 'Erro no recebimento, valor estorno deve ser menor ou igual valor pago'
        ));
        exit;
    }
    if ($valorEstorno < 0) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'estorno',
            'tipo' => 'Erro Estorno deve ser maior que zero'
        ));
        exit;
    }
    if ($valorEstorno < 1) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'estorno',
            'tipo' => 'Erro Estorno deve ser maior que zero'
        ));
        exit;
    }
    try {
        $sql = "Insert into tbamortizacao (parcela_cr, dt_pagto, vlr_pago, idusuario, idvenda, tipo) values (:parcela_cr, :dt_pagto, :vlr_pago, :idusuario, :idvenda, :tipo)";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':parcela_cr', $parcela['parcela_cr']);
        $stmt->bindValue(':dt_pagto', date('Y-m-d H:i:s'));
        $stmt->bindValue(':vlr_pago', $valorEstorno * -1);
        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
        $stmt->bindValue(':idvenda', $parcela['idvenda']);
        $stmt->bindValue(':tipo', AMORTIZACAO_ESTORNO);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
    }


    try {
        $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                . "descLancamento, idhistorico, inautomatico) values "
                . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
        $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
        $stmt->bindValue(':tipoLancamento', 2);
        $stmt->bindValue(':vlLancamento', $valorEstorno);
        $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
        $stmt->bindValue(':descLancamento', 'Estorno parcela entrada venda #' . $idvenda);
        $stmt->bindValue(':idhistorico', 6);
        $stmt->bindValue(':inautomatico', 1);
        $stmt->execute();
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
    }

    if ($valorEstorno == $parcela['vrl_recebido']) {
        try {


            $sql = "Update tbcontasreceber set vrl_recebido = :vrl_recebido, situacao = :situacao, dt_pagto = :dt_pagto, vlr_parcela = :vlr_parcela where idvenda = :idvenda and parcela_cr = :parcela_cr";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':vrl_recebido', null);
            $stmt->bindValue(':situacao', PARCELA_ABERTA);
            $stmt->bindValue(':dt_pagto', null);
            $stmt->bindValue(':vlr_parcela', $parcela['vlr_parcela']);
            $stmt->bindValue(':idvenda', $parcela['idvenda']);
            $stmt->bindValue(':parcela_cr', $parcela['parcela_cr']);
            $stmt->execute();
        } catch (Exception $ex) {
            print_r($ex);
        }
        header('location:' . $_SERVER['REQUEST_URI']);
    } else
    if ($valorEstorno <= $parcela['vlr_parcela']) {
        try {
            $sql = "Update tbcontasreceber set vrl_recebido = :vrl_recebido, situacao = :situacao, dt_pagto = :dt_pagto, vlr_parcela = :vlr_parcela where idvenda = :idvenda and parcela_cr = :parcela_cr";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':vrl_recebido', $parcela['vrl_recebido'] - $valorEstorno);
            $stmt->bindValue(':situacao', PARCELA_BAIXADA_PARCIAL_ESTORNO);
            $stmt->bindValue(':dt_pagto', date('Y-m-d'));
            $stmt->bindValue(':vlr_parcela', $parcela['vlr_parcela']);
            $stmt->bindValue(':idvenda', $parcela['idvenda']);
            $stmt->bindValue(':parcela_cr', $parcela['parcela_cr']);
            $stmt->execute();
        } catch (Exception $ex) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
        }
        header('location:' . $_SERVER['REQUEST_URI']);
    }
}

topo(array(
    "pageName" => 'Estornar Parcela',
    "icon" => 'fa fa-money'
));
?>
<div class="row">
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="estornarParcela" name="estornarParcela" role="form" method="post" action="estornar-parcela.php?idvenda=<?php echo $parcela['idvenda']; ?>&parcela_cr=<?php echo $parcela['parcela_cr']; ?>">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="fparcela">Código parcela : #<?php echo $parcela['parcela_cr']; ?></label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="fparcela">Código venda : # <?php echo $parcela['idvenda']; ?></label>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" value="<?php echo $parcela['vrl_recebido']; ?>" name="valorPago">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela" style="color:red;">Valor pago : <?php echo $parcela['vrl_recebido'] == null ? number_format(0, 2, '.', '') : $parcela['vrl_recebido']; ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela">Valor estorno</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control" id="valorEstorno" name="valorEstorno" value="<?php echo $parcela['vrl_recebido'] == null ? number_format(0, 2, '.', '') : $parcela['vrl_recebido']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="estornarParcela">Estornar </button>
                <button type="reset" class="btn btn-danger"  form="estornarParcela">Cancelar </button>
            </div>
        </div>
    </div>

    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Log baixas da parcela</h1>
            </div>
            <div class="panel-body">
                <!--                fazer select na tbamortizacao e mostrar informações da tbamortizacao<br>
                                Ao baixar parcela verificar valor que foi pago, se valor for maior ou igual valor da parcela deve salvar na tbamortização e tbcontas a receber deve fazer um update na valor pago, em parcelas com baixa parcial, ou baixada fazer uma outra tela para poder estornar a parcela para o cliente-->

                <?php
                $totalParcelasAmortizacao = 0;
                $sql = "Select * from tbamortizacao where idvenda = " . $parcela['idvenda'] . " and parcela_cr = " . $parcela['parcela_cr'];
                $stmt = $con->prepare($sql);
                $stmt->execute();
                $parcelasAmortizacao = $stmt->fetchAll(PDO::FETCH_ASSOC);
                ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Parcela</th>
                            <th>Venda</th>
                            <th>Data</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($parcelasAmortizacao as $parcelaAmortizacao) {
                            $totalParcelasAmortizacao = $totalParcelasAmortizacao + $parcelaAmortizacao['vlr_pago'];
                            $data = explode(' ', $parcelaAmortizacao['dt_pagto']);
                            ?>
                            <tr>
                                <td><?php echo $parcelaAmortizacao['idamortizacao']; ?></td>
                                <td><?php echo $parcelaAmortizacao['parcela_cr']; ?></td>
                                <td><?php echo '#' . $parcelaAmortizacao['idvenda']; ?></td>
                                <td><?php echo formatDate($data[0], DATE_BRASIL) . ' - ' . $data[1]; ?></td>
                                <td><?php echo $parcelaAmortizacao['vlr_pago']; ?></td>
                            </tr>
                        <?php } ?>
                        <tr style="color: black;">
                            <td colspan="4" class="text-center">Total</td>
                            <td><?php echo number_format($totalParcelasAmortizacao, 2, '.', ''); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--                        <div class="panel-footer">
                                    </div>-->
        </div>
    </div>
    <?php
    rodape(array(
        "js" => array(
            "js/receber/receber.js",
        )
    ));
