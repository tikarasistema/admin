<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
require 'lib/telas.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$tema = '';
if ($_POST) {
    $tema = $_POST['tema'];
    $con = MyPdo::connect();
    $stmt = $con->prepare("Update tbusuario set tema = :tema where idusuario = :idusuario");
    $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
    $stmt->bindValue(':tema', $tema);
    $stmt->execute();
    $_SESSION['usuario']['tema'] = $tema;
    sucessInsertUpdate(array(
        'page' => 'index.php',
        'origem' => 'usuário',
        'tipo' => 'Atualização'
    ));
}
topo(array(
    'css' => array(
        "css/usuario/usuario.css",
    ),
    'pageName' => ' Alterar tema de usuário',
    'icon' => 'fa fa-theme',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="alterarTema" name="alterarTema" role="form" method="post" action="">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="papel" for="tema">Temas</label>
                                    <select class="form-control" name="tema" id="tema">
                                        <option>Selecione um tema</option>
                                        <?php
                                        sort($temas);
                                        foreach ($temas as $tema) {
                                            ?>
                                            <option value="<?php echo $tema; ?>" <?php echo $_SESSION['usuario']['tema'] == $tema ? 'selected' : ''; ?>><?php echo $tema; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="alterarTema">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="alterarTema">Cancelar </button>

            </div>
        </div>
        </form>
    </div>
</div>

<?php
rodape(array(
    'js' => array(
        'js/condicaopgto/condicaopgto.js',
    )
));
?>
