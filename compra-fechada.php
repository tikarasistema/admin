<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$compra = array();
$idcompra = '';
$idfornecedor = '';
$dataCompra = '';
$situacao = '';
$condpgto = '';
$nome = '';
$con = MyPdo::connect();
$_SESSION['erro'] = false;
$condicaopgto = array();
$totalPago = 0;



if (isset($_GET['idcompra'])) {
    $idcompra = getGet('idcompra');
    $stmt = $con->prepare("Select tbfornecedor.idfornecedor,  nome_fantasia, nome_razao, cpf_cnpj, email, celular, telefone, data_compra, tbcompra.situacao from tbfornecedor inner join tbcompra"
            . " on tbfornecedor.idfornecedor = tbcompra.idfornecedor where idcompra = :idcompra and tbcompra.situacao = " . COMPRA_FECHADA);
    $stmt->bindValue(":idcompra", $idcompra);
    $stmt->execute();
    $compra = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$compra) {
        editarErro(array(
            'page' => 'fornecedores.php',
            'tipo' => 'Compra inválida'
        ));
    } else {
        $dataCompra = date(('d/m/Y H:i:s'), strtotime($compra['data_compra']));
        $situacao = $compra['situacao'];
        $nome = $compra['nome_fantasia'];
        $cpfcnpj = $compra['cpf_cnpj'];
        $email = $compra['email'];
        $celular = $compra['celular'];
        $telefone = $compra['telefone'];
    }
} else {
    editarErro(array(
        'page' => 'index.php',
        'tipo' => 'Compra não pode ser cancelada, compra inválida'
    ));
}
?>
<?php
topo(array(
    'css' => array(
        'css/compra/compra.css',
    ),
    'pageName' => ' Compra fechada',
    'icon' => 'fa fa-cart-plus',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Compra fechada</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label>Data compra: <?php echo $dataCompra; ?></span></label>

                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label> Situação: <?php echo $situacao == COMPRA_FECHADA ? "Compra fechada" : ''; ?></label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <?php
                        $sql = "Select stnome from tbusuario where idusuario = (select idusuario from tbcompra where idcompra = :idcompra)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':idcompra', $idcompra);
                        $stmt->execute();
                        $nomeComprador = $stmt->fetch(PDO::FETCH_COLUMN);
                        ?>
                        <label> Comprador: <?php echo $nomeComprador; ?></label>
                    </div>


                </div><br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Dados Fornecedor</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <label>Fornecedor: <?php echo $nome; ?> </label><br>
                            <label><?php echo $cpfcnpj ? 'Cpf/Cnpj: ' . $cpfcnpj : ''; ?> </label><br>
                            <label><?php echo $celular ? 'Celular: ' . $celular : ''; ?> </label><br>
                            <label><?php echo $telefone ? 'Telefone: ' . $telefone : ''; ?> </label><br>
                            <label><?php echo $email ? 'Email: ' . $email : ''; ?> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $sql = "Select i.*, p.produto from tbitem_compra as i inner join tbproduto as p on i.idproduto = p.idproduto where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $esconder = "class='esconder'";
        ?>
        <div id="modalCancelar"></div>
        <div id="modalCancelar2"></div>
        <div id="modalCancelar3"></div>
        <div id="produtosCompra" <?php echo $itens ? '' : $esconder; ?>>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos compra</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>ID Compra</th>
                                <th>ID Produto</th>
                                <th>Descrição</th>
                                <th>Quantidade</th>
                                <th>Valor</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="produtosCompraListar">
                            <?php foreach ($itens as $item) { ?>
                                <tr class="linhaProdCompra" data-idcompra="<?php echo $item['idcompra']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>">
                                    <td class="idcompra"><?php echo $item['idcompra']; ?></td>
                                    <td class="idproduto"><?php echo $item['idproduto']; ?></td>
                                    <td class="idproduto"><?php echo $item['produto']; ?></td>
                                    <td class="qtde_item"><?php echo $item['qtde_item']; ?></td>
                                    <td class="preco_item"><?php echo $item['preco_item']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Parcelas compra</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Parcela</th>
                                        <th>Compra</th>
                                        <th>Valor</th>
                                        <th>Valor pago</th>
                                        <th>Situacao</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $sql = "select parcela_cp, idcompra, vlr_parcela, vlr_pago, situacao from tbcontaspagar where idcompra = :idcompra ";
                                    $stmt = $con->prepare($sql);
                                    $stmt->bindValue(':idcompra', $idcompra);
                                    $stmt->execute();
                                    $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                    foreach ($parcelas as $parcela) {
                                        $total = $total + $parcela['vlr_parcela'];
                                        $totalPago += $parcela['vlr_pago'] ? $parcela['vlr_pago'] : 0;
                                        ?>
                                        <tr>
                                            <td><?php echo $parcela['parcela_cp']; ?></td>
                                            <td><?php echo $parcela['idcompra']; ?></td>
                                            <td><?php echo $parcela['vlr_parcela']; ?></td>
                                            <td><?php echo $parcela['vlr_pago'] ? $parcela['vlr_pago'] : '0.00'; ?></td>
                                            <td><?php
                                                switch ($parcela['situacao']) {
                                                    case PARCELA_ABERTA :
                                                        echo 'Aberta';
                                                        break;
                                                    case PARCELA_BAIXADA :
                                                        echo 'Paga';
                                                        break;
                                                    case PARCELA_ESTORNADA :
                                                        echo 'Estornada';
                                                        break;
                                                    case PARCELA_BAIXADA_PARCIAL:
                                                        echo 'Baixa parcial';
                                                        break;
                                                    case PARCELA_CARTAO:
                                                        echo 'Cartão';
                                                        break;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2" class="text-center"><strong>Total Compra</strong></td>
                                        <td><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
                                        <td><strong><?php echo number_format($totalPago, 2, '.', ''); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button id="cancelarCompra" type="submit" class="btn btn-danger">Estornar Compra</button>
                        <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$diacompra = explode(' ', $compra['data_compra']);
$diacompra = $diacompra[0];
?>
<script>
    var idcompra = "<?php echo $idcompra; ?>";
    var datacompra = "<?php echo $diacompra; ?>";
    var idfornecedor = "<?php echo $compra['idfornecedor']; ?>";
</script>

<?php
rodape(array(
    'js' => array(
        'js/compra/compra-cancelar.js',
    )
));
?>







