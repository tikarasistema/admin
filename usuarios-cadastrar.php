<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$email = '';
$nome = '';
$senha = '';
$senha2 = '';
$tipo = ''; //papel

$cpfcnpj = '';
$telefone = '';
$celular = '';
$logradouro = '';
$bairro = '';
$numero = '';
$complemento = '';
$cidade = 0;
$estado = 0;
$estadosTeste = '';
$cidadesTeste = '';
$situacao = USUARIO_ATIVO;
$status = ''; //fisicojuridico

$con = MyPdo::connect();
$sqlEstado = "SELECT id FROM estado";
$consulta = $con->query($sqlEstado);
$consulta->execute();

$sqlCidade = "SELECT id FROM cidade";
$consultaCidade = $con->query($sqlCidade);
$consultaCidade->execute();

$sqlCnpj = "Select cpf_cnpj from tbusuario";
$consultaCnpjCpf = $con->query($sqlCnpj);
$consultaCnpjCpf->execute();

$estados = $consulta->fetchAll(PDO::FETCH_COLUMN);
$cidades = $consultaCidade->fetchAll(PDO::FETCH_COLUMN);
$cnpjcpfs = $consultaCnpjCpf->fetchAll(PDO::FETCH_COLUMN);


$con = MyPdo::connect();
$stmt = $con->query("Select idpapel, dspapel from tbpapel where situacao = " . PAPEL_ATIVO);
$stmt->execute();
$papeisDiponiveis = $stmt->fetchAll(PDO::FETCH_ASSOC);



if ($_POST) {
    $nome = getPost('nome');
    $email = getPost('email');
    $senha = getPost('senha');
    $senha2 = getPost('senha2');
    $tipo = getPost('tipo'); //papel

    $cpfcnpj = getPost('cpfcnpj');
    $telefone = getPost('telefone');
    $celular = getPost('celular');
    $logradouro = getPost('logradouro');
    $bairro = getPost('bairro');
    $numero = getPost('numero');
    $estado = getPost('estado');
    $status = PESSOA_FISICA; //fisicojuridico
    //Falta validar
    $complemento = getPost('complemento');
    $cidade = getPost('cidade');
    print
            $_SESSION['erro'] = false;


    if (!$tipo) {
        addMessage('Selecione um papel');
        erro();
    }
    if (empty($nome)) {
        addMessage("Preencha o nome");
        erro();
    }
    if (empty($email)) {
        addMessage("Preencha o email");
        erro();
    }
    if (empty($senha)) {
        addMessage("Preencha a senha");
        erro();
    } elseif (strlen($senha) < 6) {
        addMessage("Senha deve ter mínimo de 6 dígitos");
        erro();
    }
    if ($senha != $senha2) {
        addMessage("As Senhas devem ser iguais");
        erro();
    }

    if ($estado == 0) {
        addMessage("Selecione um estado");
        erro();
    }
    if ($cidade == 0) {
        addMessage("Selecione uma cidade");
        erro();
    }
    if (stringValidate($nome) == 0) {
        addMessage('Nome deve conter apenas letras');
        erro();
    }

    if (in_array($cpfcnpj, $cnpjcpfs)) {
        addMessage('Cpf ou cnpj de usuário já cadastrado insira outro usuário');
        erro();
    }

    if (!in_array($estado, $estados)) {
        addMessage('Selecione um estado válido');
        erro();
    }
    if (!in_array($estado, $cidades)) {
        addMessage('Selecione uma cidade válida');
        erro();
    }

    if ($status == PESSOA_FISICA) {
        $retorno = cpf($cpfcnpj);
        if (true !== $retorno) {
            addMessage('CPF inválido!');
            erro();
        }
    } elseif ($status == PESSOA_JURIDICA) {
        $retorno = cnpj($cpfcnpj);
        if (!$retorno) {
            addMessage('CNPJ inválido!');
            erro();
        }
    }

    if (emailValidate($email) != true) {
        addMessage('Email Inválido');
        erro();
    }
    if (false === numberValidate($numero)) {
        addMessage("Preencha campo número com números ");
        erro();
    }

    if (!empty($cpfcnpj)) {
        if (in_array($cpfcnpj, $cnpjcpfs)) {
            addMessage('Cpf ou cnpj de usuário já cadastrado insira outro usuário');
            erro();
        }
    }

    if ($estado != 0) {
        if (!in_array($estado, $estados)) {
            addMessage('Selecione um estado válido');
            erro();
        }
    }
    if ($cidade != 0) {
        if (!in_array($cidade, $cidades)) {
            addMessage('Selecione uma cidade válida');
            erro();
        }
    }

    if (!empty($cpfcnpj)) {
        if ($status == PESSOA_FISICA) {
            $retorno = cpf($cpfcnpj);
            if (true !== $retorno) {
                addMessage('CPF inválido!');
                erro();
            }
        } elseif ($status == PESSOA_JURIDICA) {
            $retorno = cnpj($cpfcnpj);
            if (!$retorno) {
                addMessage('CNPJ inválido!');
                erro();
            }
        } else {
            addMessage('Informe o tipo');
            erro();
        }
    }
    if (!empty($email)) {
        if (emailValidate($email) != true) {
            addMessage('Email Inválido');
            erro();
        }
    }
    if (!empty($numero)) {
        if (false === numberValidate($numero)) {
            addMessage("Preencha campo número com números ");
            erro();
        }
    }
    if (empty($nome)) {
        addMessage("Preencha o campo nome");
        erro();
    } else
    if (stringValidate($nome) == 0) {
        addMessage('Nome deve conter apenas letras');
        erro();
    }

    $con = MyPdo::connect();
    $sql = "Select UPPER(stemail) from tbusuario where stemail = UPPER(:stemail)";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':stemail', $email);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($consulta) {
        addMessage('Email ja cadastrado, insira um email diferente');
        erro();
    }

    //fazer bindvalue
    if (!getSession('erro')) {
        $senha = senha($senha);
        $db = MyPdo::connect();
        $prepare = $db->prepare('insert into tbusuario (stnome, stemail, stsenha, situacao, intipo,cpf_cnpj, telefone, celular, logradouro, bairro, numero, complemento, cidade, estado, status, tema) values (:stnome, :stemail, :stsenha, :situacao, :intipo, :cpf_cnpj, :telefone, :celular, :logradouro, :bairro, :numero, :complemento, :cidade, :estado, :status, :tema)');
        ;
        $prepare->execute(array(
            ':stnome' => $nome,
            ':stemail' => $email,
            ':stsenha' => $senha,
            ':situacao' => USUARIO_ATIVO,
            ':intipo' => $tipo,
            ':cpf_cnpj' => $cpfcnpj,
            ':telefone' => $telefone,
            ':celular' => $celular,
            ':logradouro' => $logradouro,
            ':bairro' => $bairro,
            ':numero' => $numero,
            ':complemento' => $complemento,
            ':cidade' => $cidade,
            ':estado' => $estado,
            ':status' => $status,
            ':tema' => 'paper',
        ));
        sucessInsertUpdate(array(
            'page' => 'usuarios.php',
            'origem' => 'usuário',
            'tipo' => 'Cadastro'
        ));
    }
}
topo(array(
    'css' => array(
        "css/usuario/usuario.css",
    ),
    'pageName' => ' Cadastrar Usuário',
    'icon' => 'fa fa-user-plus',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarUsuarios" name="cadastrarUsuarios" role="form" method="post" action="usuarios-cadastrar.php">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcliente">Nome*</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite nome " value="<?php echo $nome; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcpfcnpj">CPF / CNPJ*</label>
                                    <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Somente números " value="<?php echo $cpfcnpj; ?>">
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="row">
                                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                                        <div class="form-group">
                                                            <label for="fstatus">Tipo*</label>
                                                            <select class="form-control" name="status" id="fstatus">
                                                                <option value="0">Selecione</option>
                                                                <option value="1" <?php echo $status == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                                                <option value="2" <?php echo $status == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="ftelefone">Telefone*</label>
                                    <input type="text" data-mask="(00) 0000-0000" class="form-control" id="ftelefone" name="telefone" value="<?php echo $telefone; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fcelular">Celular*</label>
                                    <input type="text" class="form-control" id="fcelular" name="celular" value="<?php echo $celular; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Endereço</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="flogradouro">Logradouro*</label>
                                            <input type="text" name="logradouro" id="flogradouro" class="form-control" value="<?php echo $logradouro; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fnumero">Bairro*</label>
                                            <input type="text" name="bairro" id="fbairro" class="form-control" value="<?php echo $bairro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-4">
                                        <div class="form-group">
                                            <label for="fnumero">Número*</label>
                                            <input type="number" name="numero" id="fnumero" class="form-control" value="<?php echo $numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="form-group">
                                            <label for="fcomplemento">Complemento</label>
                                            <input type="text" name="complemento" id="fcomplemento" class="form-control" value="<?php echo $complemento; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="festado">Estado* </label>
                                            <select class="form-control" id="festado" name="estado">
                                                <option value="0">Escolha o estado </option>
                                                <?php
                                                $sql = "SELECT id, nome FROM estado order by nome";
                                                $consulta = $con->query($sql);
                                                $consulta->execute();
                                                while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $resultado['id']; ?>"
                                                            <?php if ($resultado == $resultado['id']) { ?> selected <?php } ?>
                                                            ><?php echo $resultado['nome']; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fcidade">Cidade* </label>
                                            <select class="form-control" id="fcidade" name="cidade">
                                                <option value="0">--Selecione o Estado --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Dados de Autenticação</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email*</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite email válido " value="<?php echo $email; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label for="senha">Senha*</label>
                                                <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite senha ">
                                                <div class="help-block">Min. 6 dígitos.</div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <label for="senha2">Digite Senha Novamente*</label>
                                            <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Digite senha novamente ">
                                            <div class="help-block">Min. 6 dígitos.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="ftipo">Papel*</label>
                                            <select class="form-control" name="tipo" id="ftipo">
                                                <option value="0">Selecione um papel</option>
                                                <?php foreach ($papeisDiponiveis as $value) { ?>
                                                    <option value="<?php echo $value['idpapel']; ?>"><?php echo $value['dspapel']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarUsuarios">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarUsuarios">Cancelar </button>

            </div>
        </div>
    </div>
</div>
<script>
    var cidade = '';
</script>
<?php
rodape(array(
    'js' => array(
        'js/usuario/usuario.js',
    )
));
