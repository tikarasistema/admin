<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$situacao = array();
$filtro = '';
$idfornecedor = getGet('idfornecedor', 0);
try {
    $con = MyPdo::connect();
    $sql = "select nome_razao, cpf_cnpj, telefone, celular, email from tbfornecedor where idfornecedor = :idfornecedor";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idfornecedor', $idfornecedor);
    $stmt->execute();
    $fornecedor = $stmt->fetch(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    var_dump($e);
    exit;
}
if (!$fornecedor) {
    editarErro(array(
        'page' => 'fornecedores.php',
        'origem' => 'Relatorio a pagar',
        'tipo' => 'Fornecedor inválido',
    ));
}
if ($idfornecedor == 0) {
    editarErro(array(
        'page' => 'fornecedores.php',
        'origem' => 'Relatorio a pagar',
        'tipo' => 'Fornecedor inválido',
    ));
}
if ($_POST) {
    $situacao = $_POST['optradio'];
    $condicoes = array();
    $parcelaVencida = '1=2';
    $situacaoPesquisar = join(',', $situacao);

    $filtro = '<br><br>Filtrado por:<strong> <br> ';

    foreach ($situacao as $situacaoFiltro) {
        switch ($situacaoFiltro) {
            case "'A'":
                $filtro .= '<br>Parcela aberta<br>';
                break;
            case "'B'":
                $filtro .= '<br>Parcela paga<br>';
                break;
            case "'P'":
                $filtro .= '<br>Parcela paga parcial<br>';
                break;
            case "'C'":
                $filtro .= '<br>Parcela cartão<br>';
                break;
            case "'E'":
                $filtro .= '<br>Parcela estornada<br>';
                break;
            case "'V'":
                $filtro .= '<br>Parcela vencida<br>';
                break;
        }
    }

    if (in_array("'V'", $situacao)) {
        $parcelaVencida = "((dt_vencimento < CURRENT_DATE) and (situacao in ('" . PARCELA_ABERTA . "', '" . PARCELA_BAIXADA_PARCIAL . "')) and (dt_pagamento < SUBDATE(CURRENT_DATE, INTERVAL 1 DAY ) or dt_pagamento is null))";
    }
}
topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
    ),
    "pageName" => 'Relatório de à pagar ' . '<br><br> Dia atual: ' . date('d/m/Y') . $filtro . '</strong>',
    "icon" => 'fa fa-list'
));
?>
<div class='jumbotron'>
    <strong> Cliente : <?php echo $fornecedor['nome_razao']; ?></strong>
    <hr>
    <strong> CPF/CNPJ : <?php echo $fornecedor['cpf_cnpj']; ?></strong>
    <hr>
    <strong> Telefone : <?php echo $fornecedor['telefone']; ?></strong>
    <hr>
    <strong> Celular : <?php echo $fornecedor['celular']; ?></strong>
    <hr>
    <strong> Email : <?php echo $fornecedor['email']; ?></strong>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title remover-print">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="gerarRelatorio" name="gerarRelatorio" role="form" method="post" action="<?php echo $_SERVER['REQUEST_URI'] . '#parcelas'; ?>">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="pesquisa">Selecione situação das parcelas* </label>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'A'", $situacao) ? 'checked' : ''; ?> type="checkbox" class="metodo" value="'A'" name="optradio[]">Parcela aberta</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'B'", $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="'B'" name="optradio[]">Parcela paga</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'P'", $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="'P'" name="optradio[]">Parcela paga parcial</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'C'", $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="'C'" name="optradio[]">Cartão</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'E'", $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="'E'" name="optradio[]">Parcela estornada</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array("'V'", $situacao) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="'V'" name="optradio[]">Parcela vencida</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php if ($_POST) {
                                    ?>
                                    <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if ($_POST) {
                    try {
                        $con = MyPdo::connect();
                        $sql = "select * from tbcontaspagar where situacao in ($situacaoPesquisar) or $parcelaVencida  and idfornecedor = :idfornecedor order by dt_vencimento";

                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':idfornecedor', $idfornecedor);
                        $stmt->execute();
                        $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    } catch (Exception $e) {
                        var_dump($e);
                        exit;
                    }
                    ?>
                    <div class='col-xs-10' id='parcelas'>
                        <table class="table table-bordered table-hover " >
                            <thead>
                                <tr>
                                    <th>Parcela</th>
                                    <th>Venda</th>
                                    <th>Data Emissão</th>
                                    <th>Data Vencimento</th>
                                    <th>Data Pagamento</th>
                                    <th>Valor parcela</th>
                                    <th>Valor recebido</th>
                                    <th>Valor à pagar</th>
                                    <th>Situação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($parcelas as $parcela) {
                                    $data1 = new DateTime(date('Y-m-d'));
                                    $data2 = new DateTime($parcela['dt_vencimento']);

                                    $intervalo = $data1->diff($data2);

                                    if ($intervalo->days >= 0 and $intervalo->invert == 1) {
                                        if ($parcela['situacao'] == PARCELA_ABERTA or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL) {
                                            if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                                $parcelaAtrasada = 'class = warning';
                                            }
                                            if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                                $parcelaAtrasada = 'class= danger';
                                            }
                                        }
                                    } else {
                                        $parcelaAtrasada = '';
                                    }

                                    if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                        $parcelaAtrasada = 'class= success';
                                    }
                                    if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                        $parcelaAtrasada = '';
                                    }
                                    if ($parcela['situacao'] == PARCELA_CARTAO) {
                                        $parcelaAtrasada = '';
                                    }
                                    if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL) {
                                        if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class = warning';
                                        } else
                                        if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class= danger';
                                        } else
                                            $parcelaAtrasada = 'class=parcial';
                                    }
                                    ?>

                                    <tr <?php echo $parcelaAtrasada; ?>>
                                        <td> <?php echo $parcela['parcela_cp']; ?></td>
                                        <td><?php echo $parcela['idcompra']; ?></td>
                                        <td> <?php echo formatDate($parcela['dt_emissao'], DATE_BRASIL); ?></td>
                                        <td> <?php echo formatDate($parcela['dt_vencimento'], DATE_BRASIL); ?></td>
                                        <td> <?php echo formatDate($parcela['dt_pagamento'], DATE_BRASIL); ?></td>
                                        <td> <?php echo $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL ? number_format($parcela['vlr_parcela'] + $parcela['vlr_pago'], 2, '.', '') : $parcela['vlr_parcela']; ?></td>
                                        <td> <?php echo $parcela['vlr_pago']; ?></td>
                                        <td> <?php
                                            if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                                echo '0.00';
                                            } else {
                                                echo $parcela['vlr_parcela'];
                                            }
                                            ?></td>
                                        <td> <?php
                                            if ($parcela['situacao'] == PARCELA_ABERTA) {
                                                echo 'ABERTA';
                                            }
                                            if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                                echo 'BAIXADA';
                                            }
                                            if ($parcela['situacao'] == PARCELA_CARTAO) {
                                                echo 'CARTÃO';
                                            }
                                            if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                                echo 'ESTORNADA';
                                            }
                                            if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL) {
                                                echo 'BAIXA PARCIAL';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-2 remover-print">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>Legenda</strong>
                            </div>
                            <div class="panel-body">
                                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#d0e9c6;"></i> <strong>Parcela paga</strong><br>
                                <hr>
                                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #d2eafd"></i> <strong>Parcela paga parcialmente</strong>
                                <hr>
                                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#ffd699;"></i> <strong>Vencido entre 1  e 29 dias</strong><br>
                                <hr>
                                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #f7a6a4"></i> <strong>Vencido à mais de 30 dias</strong>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                <button type="reset" class="btn btn-danger"  form="gerarRelatorio">Cancelar </button>
                <?php if ($_POST) {
                    ?>
                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                <?php }
                ?>

            </div>
        </div>
    </div>
</div>
<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js",
    )
));
?>
