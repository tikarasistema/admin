<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$venda = array();
$idvenda = '';
$idcliente = '';
$dataVenda = '';
$situacao = '';
$condpgto = '';
$nome = '';
$con = MyPdo::connect();
$_SESSION['erro'] = false;
$condicaopgto = array();
$totalPago = 0;


if (isset($_GET['idvenda'])) {
//    if (!isset($_SERVER['HTTP_REFERER'])) {
//        $_SERVER['HTTP_REFERER'] = '';
//    }
//
//    $uri = explode('?', $_SERVER['HTTP_REFERER']);
//    $request = explode('/', ltrim($uri[0], '/'));
//    $paginaAnterior = end($request);
//
//    if ($paginaAnterior != "clientes.php") {
//
//        editarErro(array(
//            'page' => 'index.php',
//            'tipo' => 'Venda não pode ser inicializada, cliente inválido'
//        ));
//    }
    $idvenda = getGet('idvenda');
    $stmt = $con->prepare("Select tbcliente.idcliente, nome_razao, cpf_cnpj, celular, email, data_venda, tbvenda.situacao from tbcliente inner join tbvenda"
            . " on tbcliente.idcliente = tbvenda.idcliente where idvenda = :idvenda and tbvenda.situacao = " . VENDA_FECHADA);
    $stmt->bindValue(":idvenda", $idvenda);
    $stmt->execute();
    $venda = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$venda) {
        editarErro(array(
            'page' => 'clientes.php',
            'tipo' => 'Venda não pode ser inicializada, venda inválida'
        ));
    } else {
        $dataVenda = date(('d/m/Y H:i:s'), strtotime($venda['data_venda']));
        $situacao = $venda['situacao'];
        $nome = $venda['nome_razao'];
        $cpfcnpj = $venda['cpf_cnpj'];
        $celular = $venda['celular'];
        $email = $venda['email'];
    }
} else {
    editarErro(array(
        'page' => 'index.php',
        'tipo' => 'Venda não pode ser cancelada, venda inválida'
    ));
}
?>
<?php
topo(array(
    'css' => array(
        'css/venda/venda.css',
    ),
    'pageName' => ' Venda fechada #' . $idvenda,
    'icon' => 'fa fa-money',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Venda fechada</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <label>Data venda: <?php echo $dataVenda; ?></span></label>

                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <label> Situação: <?php echo $situacao == VENDA_FECHADA ? "Venda fechada" : ''; ?></label>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php
                        $sql = "Select stnome from tbusuario where idusuario = (select idusuario from tbvenda where idvenda = :idvenda)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':idvenda', $idvenda);
                        $stmt->execute();
                        $nomeVendedor = $stmt->fetch(PDO::FETCH_COLUMN);
                        ?>
                        <label> Vendedor: <?php echo $nomeVendedor; ?></label>
                    </div>


                </div><br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Dados cliente</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <label>Cliente: <?php echo $nome; ?> </label><br>
                            <label><?php echo $cpfcnpj ? 'Cpf/Cnpj: ' . $cpfcnpj : ''; ?> </label><br>
                            <label><?php echo $celular ? 'Celular: ' . $celular : ''; ?> </label><br>
                            <label><?php echo $email ? 'Email: ' . $email : ''; ?> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $sql = "Select i.*, p.produto from tbitem_venda as i inner join tbproduto as p on i.idproduto = p.idproduto where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $esconder = "class='esconder'";
        ?>
        <div id="modalCancelar"></div>
        <div id="modalCancelar2"></div>
        <div id="modalCancelar3"></div>
        <div id="produtosVenda" <?php echo $itens ? '' : $esconder; ?>>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos venda</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>ID Produto</th>
                                <th>Descrição</th>
                                <th>Quantidade</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody id="produtosVendaListar">
                            <?php foreach ($itens as $item) { ?>
                                <tr class="linhaProdVenda" data-idvenda="<?php echo $item['idvenda']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>">
                                    <td class="idproduto"><?php echo $item['idproduto']; ?></td>
                                    <td class="idproduto"><?php echo $item['produto']; ?></td>
                                    <td class="qtde_item"><?php echo $item['qtde_item']; ?></td>
                                    <td class="preco_item"><?php echo $item['preco_item']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Parcelas venda</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Parcela</th>
                                        <th>Valor</th>
                                        <th>Valor recebido</th>
                                        <th>Situacao</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    $sql = "select parcela_cr, idvenda, vlr_parcela, vrl_recebido, situacao from tbcontasreceber where idvenda = :idvenda ";
                                    $stmt = $con->prepare($sql);
                                    $stmt->bindValue(':idvenda', $idvenda);
                                    $stmt->execute();
                                    $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                    foreach ($parcelas as $parcela) {
                                        $total = $total + $parcela['vlr_parcela'];
                                        $totalPago += $parcela['vrl_recebido'] ? $parcela['vrl_recebido'] : 0;
                                        ?>
                                        <tr>
                                            <td><?php echo $parcela['parcela_cr']; ?></td>
                                            <td><?php echo $parcela['vlr_parcela']; ?></td>
                                            <td><?php echo $parcela['vrl_recebido'] ? $parcela['vrl_recebido'] : '0.00'; ?></td>
                                            <td><?php
                                                switch ($parcela['situacao']) {
                                                    case PARCELA_ABERTA :
                                                        echo 'Aberta';
                                                        break;
                                                    case PARCELA_BAIXADA :
                                                        echo 'Recebida';
                                                        break;
                                                    case PARCELA_ESTORNADA :
                                                        echo 'Estornada';
                                                        break;
                                                    case PARCELA_BAIXADA_PARCIAL:
                                                        echo 'Baixa parcial';
                                                        break;
                                                    case PARCELA_CARTAO:
                                                        echo 'Cartão';
                                                        break;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td class="text-center"><strong>Total Venda</strong></td>
                                        <td><strong><?php echo number_format($total, 2, '.', ''); ?></strong></td>
                                        <td><strong><?php echo number_format($totalPago, 2, '.', ''); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button id="cancelarVenda" type="submit" class="btn btn-danger">Estornar Venda</button>
                        <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$diavenda = explode(' ', $venda['data_venda']);
$diavenda = $diavenda[0];
?>
<script>
    var idvenda = "<?php echo $idvenda; ?>";
    var datavenda = "<?php echo $diavenda; ?>";
    var idcliente = "<?php echo $venda['idcliente']; ?>";
</script>

<?php
rodape(array(
    'js' => array(
        'js/venda/venda-cancelar.js',
    )
));
?>







