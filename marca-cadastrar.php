<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$marca = '';
$_SESSION['erro'] = false;

if ($_POST) {
    $con = MyPdo::connect();
    $marca = getPost('marca');

    $sql = "Select UPPER(dsmarca) from tbmarca where dsmarca = UPPER(:dsmarca)";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dsmarca', $marca);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Marca cadastrada anteriormente");
        erro();
    }

    if (empty($marca)) {
        addMessage("Preecnha campo descrição");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Insert into tbmarca (dsmarca, situacao) values (:dsmarca, :situacao)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dsmarca', $marca);
            $stmt->bindValue(':situacao', MARCA_ATIVA);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'marcas.php',
                'origem' => 'marca',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}

topo(array(
    "pageName" => 'Cadastrar marca',
    "icon" => 'fa fa-tags'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarMarca" name="cadastrarMarca" role="form" method="post" action="marca-cadastrar.php">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fmarca">Descrição*</label>
                                    <input type="text" class="form-control" id="marca" name="marca" placeholder="Digite nome da marca " value="<?php echo $marca; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarMarca">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarMarca">Cancelar </button>
            </div>
        </div>
    </div>
</div>
</div>
<?php rodape(array()); ?>
