<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$erro = '';
$where = '';
$pagina = getGet('p', 1);
$situacao = getGet('situacao', '');
$editar = true;
$condicoes = array();
$con = MyPdo::connect();

$idcliente = getGet('idcliente', 0);
$idvenda = getGet('idvenda', 0);

if ($idcliente == 0) {
    editarErro(array(
        'page' => 'vendas.php',
        'origem' => 'a receber',
        'tipo' => 'Erro no recebimento'
    ));
}

if ($idcliente != 0) {
    $sql = "Select * from tbcliente where idcliente = :idcliente";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcliente', $idcliente);
    $stmt->execute();
    $cliente = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$cliente) {
        editarErro(array(
            'page' => 'receber.php',
            'origem' => 'a receber',
            'tipo' => 'Erro no recebimento'
        ));
    }
}
if ($idvenda != 0) {
    $sql = "Select * from tbvenda where idvenda = :idvenda";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idvenda', $idvenda);
    $stmt->execute();
    $parcela = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$parcela) {
        editarErro(array(
            'page' => 'receber.php',
            'origem' => 'a receber',
            'tipo' => 'Erro no recebimento'
        ));
    }
}

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('receber.php', $telasPermissao)) {
    $editar = false;
}


if ($situacao) {
    if ($situacao != PARCELA_VENCIDA) {
        $condicoes[] = " r.situacao = '$situacao'";
    } elseif ($situacao == PARCELA_VENCIDA) {
        $condicoes[] = " ((dt_vencimento < CURRENT_DATE) and (situacao in ('" . PARCELA_ABERTA . "', '" . PARCELA_BAIXADA_PARCIAL . "')) and (dt_pagto < SUBDATE(CURRENT_DATE, INTERVAL 1 DAY )))";
    }
}
if ($idcliente) {
    $condicoes[] = " r.idcliente = $idcliente";
}
if ($idvenda) {
    $condicoes[] = " r.idvenda = $idvenda";
}

if ($condicoes) {
    $condicoes = join(' and ', $condicoes);
    $where = 'where ' . $condicoes;
}

$totalRegPag = "15";

$inicio = ($pagina - 1) * $totalRegPag;

$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS r.* from tbcontasreceber as r {$where} ORDER BY r.idvenda DESC, r.parcela_cr ASC LIMIT $inicio,$totalRegPag ");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$parcelas = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$parcelas) {
    $erro = 'Sem parcelas para listagem';
}

topo(array(
    "css" => array(
        "css/receber/receber.css"
    ),
    "icon" => "fa fa-usd",
    "pageName" => " Recebimento parcela"
));
?>
<div class="row">
    <div class="col-xs-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Situação</option>
                                <option value="<?php echo PARCELA_ABERTA; ?>" <?php echo $situacao == PARCELA_ABERTA ? 'selected' : ''; ?>>Parcela aberta</option>
                                <option value="<?php echo PARCELA_BAIXADA; ?>" <?php echo $situacao == PARCELA_BAIXADA ? 'selected' : ''; ?>>Parcela recebida</option>
                                <option value="<?php echo PARCELA_CARTAO; ?>" <?php echo $situacao == PARCELA_CARTAO ? 'selected' : ''; ?>>Parcela cartão</option>
                                <option value="<?php echo PARCELA_ESTORNADA; ?>" <?php echo $situacao == PARCELA_ESTORNADA ? 'selected' : ''; ?>>Parcela estornada</option>
                                <option value="<?php echo PARCELA_BAIXADA_PARCIAL; ?>" <?php echo $situacao == PARCELA_BAIXADA_PARCIAL ? 'selected' : ''; ?>>Parcela recebida parcialmente</option>
                                <option value="<?php echo PARCELA_VENCIDA; ?>" <?php echo $situacao == PARCELA_VENCIDA ? 'selected' : ''; ?>>Parcela vencida</option>
                            </select>
                        </div>
                        <button type="button" id="pesquisar" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a type="reset" a="" href="receber.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                        <?php if ($idvenda != 0) { ?>
                                        <!--<a type="button" href="boleto-receber.php?idcliente=<?php echo $idcliente; ?>&idvenda=<?php echo $idvenda; ?>" class="btn btn-warning" style="float:right; margin:0 10px; "><i class="fa fa-money"></i> Gerar Boleto </a>-->
                        <?php } ?>
                        <a type="button" href="relatorio-receber.php?idcliente=<?php echo $idcliente; ?>" class="btn btn-success" style="float:right;"><i class="fa fa-list"></i> Gerar relatório </a>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                                <th>Parcela</th>
                                <th>Venda</th>
                                <th>Data Emissão</th>
                                <th>Data Vencimento</th>
                                <th>Data Pagamento</th>
                                <th>Valor parcela</th>
                                <th>Valor recebido</th>
                                <th>Valor à receber</th>
                                <th>Situação</th>
                                <th class="text-center">Receber parcela</th>
                                <th class="text-center">Estornar parcela</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($parcelas as $parcela) {
                                $data1 = new DateTime(date('Y-m-d'));
                                $data2 = new DateTime($parcela['dt_vencimento']);

                                $intervalo = $data1->diff($data2);

                                if ($intervalo->days >= 0 and $intervalo->invert == 1) {
                                    if ($parcela['situacao'] == PARCELA_ABERTA) {
                                        if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class = warning';
                                        }
                                        if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class= danger';
                                        }
                                    }
                                } else {
                                    $parcelaAtrasada = '';
                                }

                                if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                    $parcelaAtrasada = 'class= success';
                                }
                                if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                    $parcelaAtrasada = '';
                                }
                                if ($parcela['situacao'] == PARCELA_CARTAO) {
                                    $parcelaAtrasada = '';
                                }
                                if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO) {
                                    if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                        $parcelaAtrasada = 'class = warning';
                                    } else
                                    if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                        $parcelaAtrasada = 'class= danger';
                                    } else
                                        $parcelaAtrasada = 'class=parcial';
                                }
                                ?>

                                <tr <?php echo $parcelaAtrasada; ?>>
                                    <td> <?php echo $parcela['parcela_cr']; ?></td>
                                    <td><a title="Detalhes da venda" target="_blank" href="venda-fechada.php?idvenda=<?php echo $parcela['idvenda']; ?>"><?php echo $parcela['idvenda']; ?></a></td>
                                    <td> <?php echo formatDate($parcela['dt_emissao'], DATE_BRASIL); ?></td>
                                    <td> <?php echo formatDate($parcela['dt_vencimento'], DATE_BRASIL); ?></td>
                                    <td> <?php echo formatDate($parcela['dt_pagto'], DATE_BRASIL); ?></td>
                                    <td> <?php echo number_format($parcela['vlr_parcela'], 2, '.', ''); ?></td>
                                    <td> <?php echo $parcela['vrl_recebido']; ?></td>
                                    <td> <?php
                                        if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                            echo '0.00';
                                        } else {
                                            echo number_format($parcela['vlr_parcela'] - $parcela['vrl_recebido'], 2, '.', '');
                                        }
                                        ?>
                                    </td>
                                    <td> <?php
                                        if ($parcela['situacao'] == PARCELA_ABERTA) {
                                            echo 'ABERTA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                            echo 'RECEBIDA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_CARTAO) {
                                            echo 'CARTÃO';
                                        }
                                        if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                            echo 'ESTORNADA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO) {
                                            echo 'BAIXA PARCIAL';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php
                                        if ($parcela['situacao'] == PARCELA_ABERTA or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO) {
                                            ?>
                                            <a href="receber-parcela.php?idvenda=<?php echo $parcela['idvenda']; ?>&parcela_cr=<?php echo $parcela['parcela_cr']; ?>"><i class="fa fa-money"></i></a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA) { ?>
                                            <a href="estornar-parcela.php?idvenda=<?php echo $parcela['idvenda']; ?>&parcela_cr=<?php echo $parcela['parcela_cr']; ?>"><i class="fa fa-sign-out"></i></a>
                                        <?php }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="receber.php?p=<?php echo $anterior; ?>&idcliente=<?php echo $idcliente; ?>&situacao=<?php echo $situacao; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php
                    for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="receber.php?p=<?php echo $proximo; ?>&idcliente=<?php echo $idcliente; ?>&situacao=<?php echo $situacao; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Legenda</strong>
            </div>
            <div class="panel-body">
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#d0e9c6;"></i> <strong>Parcela recebida</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #d2eafd"></i> <strong>Parcela recebida parcialmente</strong>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#ffd699;"></i> <strong>Vencido entre 1  e 29 dias</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #f7a6a4"></i> <strong>Vencido à mais de 30 dias</strong>
            </div>
        </div>
    </div>
</div>



<script>
    var idcliente = '<?php echo $idcliente; ?>';
    var idvenda = '<?php echo $idvenda; ?>';
    var situacao = '<?php echo $situacao; ?>';
</script>
<?php
rodape(array(
    "js" => array(
        "js/receber/receber.js",
    )
));
?>