<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();

$dshistorico = '';
$tipohistorico = '';
$idhistorico = '';
$situacao = '';
$inautomatico = null;

$historicoAtual = array();
$_SESSION['erro'] = false;

if (isset($_GET['cd'])) {
    $idhistorico = $_GET['cd'];

    $sql = "Select * from tbhistorico where idhistorico = :idhistorico";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idhistorico', $idhistorico);
    $stmt->execute();
    $historicoAtual = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$historicoAtual) {
        editarErro(array(
            'page' => 'historico.php',
            'origem' => 'histórico',
            'tipo' => 'Erro na atualização'
        ));
    }
}
if ((!isset($_GET['cd'])) and ( !isset($_POST['cd']))) {
    editarErro(array(
        'page' => 'historico.php',
        'origem' => 'histórico',
        'tipo' => 'Erro na atualização'
    ));
}


if ($_POST) {
    $dshistorico = getPost('dshistorico');
    $tipohistorico = getPost('tipohistorico');
    $idhistorico = getPost('cd');
    $situacao = getPost('situacao');
    if ($situacao) {
        $situacao = HISTORICO_ATIVO;
    } else {
        $situacao = HISTORICO_INATIVO;
    }
    $inautomatico = getPost('inautomatico', null);

    if ($inautomatico) {
        $inautomatico = LANCAMENTO_AUTOMATICO;
    } else {
        $inautomatico = null;
    }
    if (($inautomatico != null) and ( $inautomatico != LANCAMENTO_AUTOMATICO)) {
        addMessage("Selecione valor válido para tipo automatico");
        erro();
    }
    $sql = "Select UPPER(dshistorico) from tbhistorico where dshistorico = UPPER(:dshistorico) and idhistorico"
            . " != :idhistorico";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dshistorico', $dshistorico);
    $stmt->bindValue(':idhistorico', $idhistorico);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Histórico cadastrado anteriormente");
        erro();
    }

    if (empty($dshistorico)) {
        addMessage("Preencha campo descrição");
        erro();
    }

    if (empty($tipohistorico)) {
        addMessage('Selecione tipo de histórico');
        erro();
    }

    if (!in_array($tipohistorico, $tiposLancamentos)) {
        addMessage("Selecione tipo de histórico válido");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Update  tbhistorico set dshistorico = :dshistorico, tipohistorico = :tipohistorico"
                    . ", situacao = :situacao, inautomatico = :inautomatico where idhistorico = :idhistorico";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dshistorico', $dshistorico);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':tipohistorico', $tipohistorico);
            $stmt->bindValue(':idhistorico', $idhistorico);
            $stmt->bindValue(':inautomatico', $inautomatico);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'historico.php',
                'origem' => 'histórico',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
} else {
    $dshistorico = $historicoAtual['dshistorico'];
    $tipohistorico = $historicoAtual['tipohistorico'];
    $idhistorico = $historicoAtual['idhistorico'];
    $situacao = $historicoAtual['situacao'];
    $inautomatico = $historicoAtual['inautomatico'];
}
topo(array(
    "pageName" => 'Cadastrar Histórico',
    "icon" => 'fa fa-signal'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <div class="col-xs-12">
                    <form id="editarHistorico" name="editarHistorico" role="form" method="post" action="historico-editar.php">
                        <input type="hidden" name="cd" value="<?php echo $idhistorico; ?>">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fhistorico">Descrição*</label>
                                    <input type="text" class="form-control" id="historico" name="dshistorico" placeholder="Digite nome do histórico " value="<?php echo $dshistorico; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="tipohistorico">Tipo de histórico*</label>
                                    <select class="form-control" name="tipohistorico" id="tipohistorico">
                                        <option value="0">Selecione tipo de histórico</option>
                                        <option value="<?php echo LANCAMENTO_CREDITO; ?>" <?php echo $tipohistorico == LANCAMENTO_CREDITO ? 'selected' : ''; ?>>Crédito</option>
                                        <option value="<?php echo LANCAMENTO_DEBITO; ?>"<?php echo $tipohistorico == LANCAMENTO_DEBITO ? 'selected' : ''; ?>>Débito</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="situacao"><input id="situacao" name="situacao" type="checkbox" <?php
                                            echo $situacao == HISTORICO_ATIVO ? 'checked' : '';
                                            ?>> Histórico ativo?
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--                        <div class="row">
                                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>   <input form="editarHistorico" type="checkbox" name="inautomatico" <?php echo $inautomatico ? 'checked' : ''; ?>  > Histórico automático?</label>
                                                            </div>
                                                            <div class="help-block">Selecionar esta opção apenas para históricos automaticos EX: baixa de parcela</div>
                                                        </div>
                                                    </div>
                                                </div>-->
                    </form>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="editarHistorico">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="editarHistorico">Cancelar </button>
            </div>
        </div>
    </div>
</div>
</div>
<?php rodape(array()); ?>

