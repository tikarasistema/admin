<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$con = MyPdo::connect();
$desclancamento = '';
$idhistorico = '';
$vlLancamento = '';
$_SESSION['erro'] = false;
if ($_POST) {
    $desclancamento = getPost('descLancamento');
    $idhistorico = getPost('idhistorico');
    $vlLancamento = getPost('vlLancamento');

    $sql = "select idhistorico from tbhistorico";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $historicos = $stmt->fetchAll(PDO::FETCH_COLUMN);

    if (empty($desclancamento)) {
        addMessage("Preencha descrição de lançamento");
        erro();
    }

    if (!in_array($idhistorico, $historicos)) {
        addMessage('Selecione histórico válido');
        erro();
    }

    if (!is_numeric($vlLancamento)) {
        addMessage("Valor de lançamento deve ser numérico");
        erro();
    }

    if ($vlLancamento < 0) {
        addMessage("Valor de lançamento deve ser positivo");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "select tipohistorico, inautomatico from tbhistorico where idhistorico = :idhistorico";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':idhistorico', $idhistorico);
            $stmt->execute();
            $historico = $stmt->fetch(PDO::FETCH_ASSOC);

            $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                    . "descLancamento, idhistorico, inautomatico) values "
                    . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
            $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
            $stmt->bindValue(':tipoLancamento', $historico['tipohistorico']);
            $stmt->bindValue(':vlLancamento', $vlLancamento);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->bindValue(':descLancamento', $desclancamento);
            $stmt->bindValue(':idhistorico', $idhistorico);
            $stmt->bindValue(':inautomatico', $historico['inautomatico']);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'lancamentos.php',
                'origem' => 'lançamento',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}
topo(array(
    "pageName" => 'Cadastrar Lançamento',
    "icon" => 'fa fa-money'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <div class="col-xs-12">
                    <form id="cadastrarLancamento" name="cadastrarLancamento" role="form" method="post" action="lancamento-conta.php">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fhistorico">Descrição*</label>
                                    <input type="text" class="form-control" id="descLancamento" name="descLancamento" placeholder="Digite descrição do lançamento" value="<?php echo $desclancamento; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fhistorico">Valor lançamento*</label>
                                    <input type="text" class="form-control" id="vlLancamento" name="vlLancamento" placeholder="Digite valor do lançamento" value="<?php echo $vlLancamento; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="idhistorico">Histórico* </label>
                                    <select class="form-control" id="idhistorico" name="idhistorico">
                                        <option value="0">Escolha o histórico </option>
                                        <?php
                                        $sql = "SELECT * FROM tbhistorico where inautomatico is NULL and situacao != " . HISTORICO_INATIVO . " order by dshistorico ";
                                        $consulta = $con->query($sql);
                                        $consulta->execute();
                                        while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <option value="<?php echo $resultado['idhistorico']; ?>"
                                                    <?php if ($idhistorico == $resultado['idhistorico']) { ?> selected <?php } ?>
                                                    ><?php echo $resultado['dshistorico']; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarLancamento">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarLancamento">Cancelar </button>
            </div>
        </div>
    </div>
</div>
</div>
<?php
rodape(array(
    'js' => array(
        'js/lancamento/lancamento.js'
    )
));
?>