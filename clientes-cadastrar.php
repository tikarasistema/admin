
<?php
$navegador = explode('/', $_SERVER['HTTP_USER_AGENT']);
$navegador = strripos($navegador[2], 'Chrome');

require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$nome = '';
$rgie = '';
$cpfcnpj = '';
$nasc = '';
$sexo = '';
$tipo = '';
$telefone = '';
$celular = '';
$email = '';
$logradouro = '';
$bairro = '';
$numero = '';
$complemento = '';
$cidade = 0;
$generico = '';
$estado = 0;
$estadosTeste = '';
$cidadesTeste = '';
$situacao = CLIENTE_ATIVO;
$_SESSION['erro'] = false;

$con = MyPdo::connect();
$sqlEstado = "SELECT id FROM estado";
$consulta = $con->query($sqlEstado);
$consulta->execute();

$sqlCidade = "SELECT id FROM cidade";
$consultaCidade = $con->query($sqlCidade);
$consultaCidade->execute();

$sqlCnpj = "Select cpf_cnpj from tbcliente";
$consultaCnpjCpf = $con->query($sqlCnpj);
$consultaCnpjCpf->execute();

$estados = $consulta->fetchAll(PDO::FETCH_COLUMN);
$cidades = $consultaCidade->fetchAll(PDO::FETCH_COLUMN);
$cnpjcpfs = $consultaCnpjCpf->fetchAll(PDO::FETCH_COLUMN);


if ($_POST) {

    $hoje = strtotime(date("Y-m-d"));
    $nome = getPost('cliente');
    $cpfcnpj = getPost('cpfcnpj');
    $rgie = getPost('rgie');
    $sexo = getPost('sexo');
    $tipo = getPost('tipo');
    $telefone = getPost('telefone');
    $celular = getPost('celular');
    $email = getPost('email');
    $logradouro = getPost('logradouro');
    $bairro = getPost('bairro');
    $numero = getPost('numero');
    $estado = getPost('estado');
    $nasc = getPost('nasc');
    $nasc2 = strtotime($nasc);

//Falta validar
    $complemento = getPost('complemento');
    $cidade = getPost('cidade');
    $generico = getPost('generico');

    if (!$generico) {

        if ($estado == 0) {
            addMessage("Selecione um estado");
            erro();
        }
        if ($cidade == 0) {
            addMessage("Selecione uma cidade");
            erro();
        }
        if (stringValidate($nome) == 0) {
            addMessage('Nome deve conter apenas letras');
            erro();
        }

        if (in_array($cpfcnpj, $cnpjcpfs)) {
            addMessage('Cpf ou cnpj de cliente ja cadastrado insira outro cliente');
            erro();
        }
        if (!in_array($tipo, $tipos)) {
            addMessage('Selecione um tipo válido');
            erro();
        }
        if (!in_array($estado, $estados)) {
            addMessage('Selecione um estado válido');
            erro();
        }
        if (!in_array($estado, $cidades)) {
            addMessage('Selecione uma cidade válida');
            erro();
        }
        if (!in_array($sexo, $sexos)) {
            addMessage('Selecione um sexo válido');
            erro();
        }

        if ($tipo == PESSOA_FISICA) {
            $retorno = cpf($cpfcnpj);
            if (true !== $retorno) {
                addMessage('CPF inválido!');
                erro();
            }
        } elseif ($tipo == PESSOA_JURIDICA) {
            $retorno = cnpj($cpfcnpj);
            if (!$retorno) {
                addMessage('CNPJ inválido!');
                erro();
            }
        }

        if ($nasc2 >= $hoje) {
            addMessage('Data Nascimento inválida');
            erro();
        }

        if (empty($nasc)) {
            addMessage('Preencha campo data de nascimento');
            erro();
        }
        if (empty($nome)) {
            addMessage("Preencha Nome");
            erro();
        }

        if (emailValidate($email) != true) {
            addMessage('Email Inválido');
            erro();
        }
        if (false === numberValidate($numero)) {
            addMessage("Preencha campo número com números ");
            erro();
        }
    } else {
        if (!empty($cpfcnpj)) {
            if (in_array($cpfcnpj, $cnpjcpfs)) {
                addMessage('Cpf ou cnpj de cliente ja cadastrado insira outro cliente');
                erro();
            }
        }
        if (!empty($tipo)) {
            if (!in_array($tipo, $tipos)) {
                addMessage('Selecione um tipo válido');
                erro();
            }
        }
        if ($estado != 0) {
            if (!in_array($estado, $estados)) {
                addMessage('Selecione um estado válido');
                erro();
            }
        }
        if ($cidade != 0) {
            if (!in_array($cidade, $cidades)) {
                addMessage('Selecione uma cidade válida');
                erro();
            }
        }
        if (!empty($sexo)) {
            if (!in_array($sexo, $sexos)) {
                addMessage('Selecione um sexo válido');
                erro();
            }
        }
        if (!empty($cpfcnpj)) {
            if ($tipo == PESSOA_FISICA) {
                $retorno = cpf($cpfcnpj);
                if (true !== $retorno) {
                    addMessage('CPF inválido!');
                    erro();
                }
            } elseif ($tipo == PESSOA_JURIDICA) {
                $retorno = cnpj($cpfcnpj);
                if (!$retorno) {
                    addMessage('CNPJ inválido!');
                    erro();
                }
            } else {
                addMessage('Informe tipo');
                erro();
            }
        }
        if (!empty($nasc)) {
            if ($nasc2 >= $hoje) {
                addMessage('Data Nascimento inválida');
                erro();
            }
        }
        if (!empty($email)) {
            if (emailValidate($email) != true) {
                addMessage('Email Inválido');
                erro();
            }
        }
        if (!empty($numero)) {
            if (false === numberValidate($numero)) {
                addMessage("Preencha campo número com números ");
                erro();
            }
        }
        if (empty($nome)) {
            addMessage("Preeencha o campo nome");
            erro();
        } else
        if (stringValidate($nome) == 0) {
            addMessage('Nome deve conter apenas letras');
            erro();
        }
    }
    if (!getSession('erro')) {
        if ($cidade == 0) {
            $cidade = null;
        }
        if ($estado == 0) {
            $estado = null;
        }
        $nasc = formatDate($nasc, DATE_BRASIL);
        try {
            $sql = "Insert into tbcliente (estado, nome_razao, cpf_cnpj, rg_ie, nasc, sexo, tipo, telefone, celular,"
                    . "email, logradouro, bairro, numero, complemento, cidade, situacao) values(:estado, :nome, :cpf_cnpj,"
                    . " :rg_ie, :nasc, :sexo, :tipo, :telefone, :celular, :email,"
                    . ":logradouro, :bairro, :numero, :complemento, :cidade, :situacao)";
            $stmt = $con->prepare($sql);
            $stmt->bindParam(":nome", $nome);
            $stmt->bindParam(":cpf_cnpj", $cpfcnpj);
            $stmt->bindParam(":rg_ie", $rgie);
            $stmt->bindParam(":nasc", $nasc);
            $stmt->bindParam(":sexo", $sexo);
            $stmt->bindParam(":tipo", $tipo);
            $stmt->bindParam(":telefone", $telefone);
            $stmt->bindParam(":celular", $celular);
            $stmt->bindParam(":email", $email);
            $stmt->bindParam(":logradouro", $logradouro);
            $stmt->bindParam(":bairro", $bairro);
            $stmt->bindParam(":numero", $numero);
            $stmt->bindParam(":complemento", $complemento);
            $stmt->bindParam(":cidade", $cidade);
            $stmt->bindParam(":situacao", $situacao);
            $stmt->bindParam(":estado", $estado);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'clientes.php',
                'origem' => 'cliente',
                'tipo' => 'Cadastro'
            ));
//            addMessage("Cadastro realizado com sucesso");
        } catch (Exception $e) {
            addMessage("Erro ao cadastrar");
            $_SESSION['erro'] = true;
            throw new Exception("Erro ao cadastrar" . $e);
        }
    }
}
?>


<?php
topo(array(
    'css' => array(
        "css/cliente/cliente.css",
    ),
    'pageName' => ' Cadastrar Cliente',
    'icon' => 'fa fa-user',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarCliente" name="cliente-cadastrar" role="form" method="post" action="clientes-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input form="cadastrarCliente" type="checkbox" name="generico" <?php echo $generico ? 'checked' : ''; ?>  >Cadastro Genérico?
                                        </label>
                                    </div>
                                    <div class="help-block">Ao selecionar esta opção, somente os campos preenchidos serão validados. </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fcliente">Nome / Razão Social*</label>
                                    <input type="text" class="form-control" id="fcliente" name="cliente" placeholder="Nome completo " value="<?php echo $nome; ?>">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-6  col-md-6">
                                    <div class="form-group">
                                        <label for="ftipo">Tipo*</label>
                                        <select class="form-control" name="tipo" id="ftipo">
                                            <option value="0">Selecione</option>
                                            <option value="1" <?php echo $tipo == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                            <option value="2" <?php echo $tipo == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="fcpfcnpj">CPF / CNPJ*</label>
                                        <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Somente números " value="<?php echo $cpfcnpj; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="frgie">RG / IE*</label>
                                    <input type="text" class="form-control" id="frgie" name="rgie" placeholder="Somente números "  value="<?php echo $rgie; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fnasc">Nascimento*</label>
                                    <input type="date" class="form-control" id="fnasc" name="nasc"  value="<?php echo $nasc; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4  col-md-4">
                                <div class="form-group">
                                    <label for="fsexo">Gênero*</label>
                                    <select class="form-control"    name="sexo" id="fsexo">
                                        <option value="0">Selecione</option>
                                        <option value="1" <?php echo $sexo == SEXO_MASCULINO ? 'selected' : ''; ?>>Masculino</option>
                                        <option value="2" <?php echo $sexo == SEXO_FEMININO ? 'selected' : ''; ?>>Feminino</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="ftelefone">Telefone*</label>
                                    <input type="text" data-mask="(00) 0000-0000" class="form-control" id="ftelefone" name="telefone" value="<?php echo $telefone; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fcelular">Celular*</label>
                                    <input type="text" class="form-control" id="fcelular" name="celular" value="<?php echo $celular; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="fcelular">Email*</label>
                                    <input type="email" class="form-control" id="femail" name="email" placeholder="email@exemple.com" value="<?php echo $email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Endereço</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="flogradouro">Logradouro*</label>
                                            <input type="text" name="logradouro" id="flogradouro" class="form-control" value="<?php echo $logradouro; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fnumero">Bairro*</label>
                                            <input type="text" name="bairro" id="fbairro" class="form-control" value="<?php echo $bairro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-4">
                                        <div class="form-group">
                                            <label for="fnumero">Número*</label>
                                            <input type="number" name="numero" id="fnumero" class="form-control" value="<?php echo $numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="form-group">
                                            <label for="fcomplemento">Complemento</label>
                                            <input type="text" name="complemento" id="fcomplemento" class="form-control" value="<?php echo $complemento; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="festado">Estado* </label>
                                            <select class="form-control" id="festado" name="estado">
                                                <option value="0">Escolha o estado </option>
                                                <?php
                                                $sql = "SELECT id, nome FROM estado order by nome";
                                                $consulta = $con->query($sql);
                                                $consulta->execute();
                                                while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $resultado['id']; ?>"
                                                            <?php if ($resultado == $resultado['id']) { ?> selected <?php } ?>
                                                            ><?php echo $resultado['nome']; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fcidade">Cidade* </label>
                                            <select class="form-control" id="fcidade" name="cidade">
                                                <option value="0">--Selecione o Estado --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarCliente">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarCliente">Cancelar </button>

            </div>
        </div>
    </div>
</div>
<script>
    var cidade = '';
</script>
<?php
rodape(array(
    'js' => array(
        'js/cliente/cliente.js',
    )
));
?>
