<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
topo(array(
));
?>
<div class="jumbotron">
    <div class="container">
        <h3 class="text-center"><?php echo EMPRESA ?></h3>
        <div class="text-center" style="margin-top: 50px;">
            <div class="btn-group">
                <a class="btn btn-primary btn-lg" role="button" href="clientes.php">
                    <i class="fa fa-user fa-lg"></i> Clientes
                </a>
            </div>

            <div class="btn-group">
                <a class="btn btn-primary btn-lg" role="button" href="produtos.php">
                    <i class="fa fa-inbox fa-lg"></i> Produtos
                </a>
            </div>
            <div class="btn-group">
                <a class="btn btn-primary btn-lg" role="button" href="fornecedores.php">
                    <i class="fa fa-truck fa-lg"></i> Fornecedores
                </a>
            </div>
            <div class="btn-group">
                <a class="btn btn-primary btn-lg" role="button" href="vendas.php">
                    <i class="fa fa-money fa-lg"></i> Vendas
                </a>
            </div>
            <div class="btn-group">
                <a class="btn btn-primary btn-lg" role="button" href="compras.php">
                    <i class="fa fa-cart-plus fa-lg"></i> Compras
                </a>
            </div>
        </div>
    </div>
</div>
<?php
$label = array();
$data = array();
$con = MyPdo::connect();
$sql = "select p.produto, sum(i.qtde_item) as quantidade, i.idproduto from tbitem_venda as i inner join tbproduto as p on i.idproduto = p.idproduto  inner join tbvenda as v on i.idvenda = v.idvenda where date(v.data_venda) BETWEEN SUBDATE(CURRENT_DATE, INTERVAL 1 MONTH ) AND CURRENT_DATE and v.situacao in (1,0) group by i.idproduto order by quantidade desc limit 10";
$produtos = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC);

foreach ($produtos as $produto) {
    $label[] = '#' . $produto['idproduto'] . '-' . $produto['produto'];
    $data[] = $produto['quantidade'];
}

$dia = array();
$vendas = array();
$con = MyPdo::connect();
$sql = "select date(data_venda) as data, count(idvenda) as vendas from tbvenda where date(data_venda) BETWEEN SUBDATE(CURRENT_DATE, INTERVAL 1 MONTH ) AND CURRENT_DATE group by date(data_venda) order by date(data_venda) asc limit 10";
$qtdVendas = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC);

foreach ($qtdVendas as $qtdVenda) {
    $dia[] = formatDate($qtdVenda['data'], DATE_BRASIL);
    $vendas[] = $qtdVenda['vendas'];
}
?>
<script type="text/javascript">
    var vendas = <?php echo json_encode($vendas); ?>;
    var dia = <?php echo json_encode($dia); ?>;
    var dados = <?php echo json_encode($data); ?>;
    var label = <?php echo json_encode($label); ?>;
</script>
<div class="row">
    <div class="col-xs-12">
        <h3 class="title text-center"><i class="fa fa-dashboard"></i> Dashboard</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Produtos mais vendidos no mês <a title="Gerar relatório"href="produto-mais-vendido-meses.php" style="float: right; color: #2196f3;"><i class="fa fa-share" aria-hidden="true"></i></a></h1>
            </div>
            <div class="panel-body">
                <!--<div class="col-xs-12 col-md-12">-->
                <div class="box-chart">
                    <canvas id="prodMaisVendido" style="width:100%;"></canvas>
                </div>
                <!--</div>-->
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Quantidade de vendas por dia no ultimo mês<a title="Gerar relatório"href="quantidade-venda-periodo.php" style="float: right; color: #2196f3;"><i class="fa fa-share" aria-hidden="true"></i></a></h1>
            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-md-12">
                    <div class="box-chart">
                        <canvas id="qtdVendasMes" style="width:100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
rodape(array(
    'js' => array(
        'js/charts/chartsjs/Chart.min.js',
        'js/charts/produtos-mais-vendidos.js',
        'js/charts/qtd_vendas_dia_mes.js',
        'js/index/index.js',
    )
));
?>
