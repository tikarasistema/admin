<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';


$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$_SESSION['erro'] = false;

$parcela_cp = getGet('parcela_cp', 0);
$idcompra = getGet('idcompra', 0);
$valorPago = '';
$parcela = array();
$cartao = '';

if ($parcela_cp == 0 or $idcompra == 0) {
    editarErro(array(
        'page' => 'compras.php',
        'origem' => 'a pagar',
        'tipo' => 'Erro no pagamento'
    ));
}

try {
    $sql = "Select * from tbcontaspagar where idcompra = :idcompra and parcela_cp = :parcela_cp ORDER BY idcompra DESC, parcela_cp ASC";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcompra', $idcompra);
    $stmt->bindValue(':parcela_cp', $parcela_cp);
    $stmt->execute();
    $parcela = $stmt->fetch(PDO::FETCH_ASSOC);
} catch (Exception $ex) {
    print_r($e);
}
if ($parcela['vlr_parcela'] < 1) {
    sucessInsertUpdate(array(
        'page' => 'forncedores.php',
        'origem' => 'fornecedor',
        'tipo' => 'Pagamento de parcela'
    ));
}

function valorPago($parcela, $con)
{
    try {
        $sql = "SELECT sum(vlr_pago) as vlr_pago FROM tbamortizacao_pagar where idcompra = " . $parcela['idcompra'] . " and parcela_cp = " . $parcela['parcela_cp'];
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $vlr_pago = $stmt->fetch(PDO::FETCH_COLUMN);
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
    }
    return $vlr_pago;
}

if ($_POST) {
    $valorPago = $_POST['valor'];
    $cartao = getPost('cartao');

    if (!$valorPago) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'a pagar',
            'tipo' => 'Erro no pagamento, valor pago inválido'
        ));
        exit;
    }
    if ($valorPago > $parcela['vlr_parcela']) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'a pagar',
            'tipo' => 'Erro no pagamento valor do pagamento deve ser menor ou igual parcela'
        ));
        exit;
    }
    if ($valorPago > $parcela['vlr_parcela'] - valorPago($parcela, $con)) {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'a pagar',
            'tipo' => 'Erro no pagamento valor da pago deve ser menor ou igual parcela'
        ));
        exit;
    }


    try {
        if ($cartao) {
            $sql = "Insert into tbamortizacao_pagar (parcela_cp, dt_pagto, vlr_pago, idusuario, idcompra, tipo) values (:parcela_cp, :dt_pagto, :vlr_pago, :idusuario, :idcompra, :tipo)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':parcela_cp', $parcela['parcela_cp']);
            $stmt->bindValue(':dt_pagto', date('Y-m-d H:i:s'));
            $stmt->bindValue(':vlr_pago', $valorPago);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->bindValue(':idcompra', $parcela['idcompra']);
            $stmt->bindValue(':tipo', AMORTIZACAO_BAIXA_CARTAO);
            $stmt->execute();
        } else
        if (!$cartao) {
            $sql = "Insert into tbamortizacao_pagar (parcela_cp, dt_pagto, vlr_pago, idusuario, idcompra, tipo) values (:parcela_cp, :dt_pagto, :vlr_pago, :idusuario, :idcompra, :tipo)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':parcela_cp', $parcela['parcela_cp']);
            $stmt->bindValue(':dt_pagto', date('Y-m-d H:i:s'));
            $stmt->bindValue(':vlr_pago', $valorPago);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->bindValue(':idcompra', $parcela['idcompra']);
            $stmt->bindValue(':tipo', AMORTIZACAO_BAIXA);
            $stmt->execute();
        }
    } catch (Exception $ex) {
        die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
    }


    if ($cartao != 1) {
        try {
            $sql = "Insert into tbcaixa (dataAbertura, dataLancamento, tipoLancamento, vlLancamento, idusuario, "
                    . "descLancamento, idhistorico, inautomatico) values "
                    . "(:dataAbertura, :dataLancamento, :tipoLancamento, :vlLancamento, :idusuario, :descLancamento, :idhistorico, :inautomatico)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dataAbertura', (date("Y-m-d")));
            $stmt->bindValue(':dataLancamento', (date("Y-m-d H:i:s")));
            $stmt->bindValue(':tipoLancamento', 2);
            $stmt->bindValue(':vlLancamento', $valorPago);
            $stmt->bindValue(':idusuario', $_SESSION['usuario']['idusuario']);
            $stmt->bindValue(':descLancamento', 'Baixa parcela compra #' . $idcompra);
            $stmt->bindValue(':idhistorico', 7);
            $stmt->bindValue(':inautomatico', 1);
            $stmt->execute();
        } catch (Exception $ex) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
        }
    }

    if (($parcela['vlr_parcela'] - valorPago($parcela, $con)) == 0) {
        if ($valorPago > $parcela['vlr_parcela']) {
            editarErro(array(
                'page' => $_SERVER['HTTP_REFERER'],
                'origem' => 'a receber',
                'tipo' => 'Erro no recebimento valor da recebido deve ser menor ou igual parcela'
            ));
            exit;
        }
        try {
            if ($cartao) {
                $sql = "Update tbcontaspagar set vlr_pago = :vlr_pago, situacao = :situacao, dt_pagamento = :dt_pagamento, vlr_parcela = :vlr_parcela where idcompra = :idcompra and parcela_cp = :parcela_cp";
                $stmt = $con->prepare($sql);
                $stmt->bindValue(':vlr_pago', valorPago($parcela, $con));
                $stmt->bindValue(':situacao', PARCELA_CARTAO);
                $stmt->bindValue(':dt_pagamento', date('Y-m-d'));
                $stmt->bindValue(':vlr_parcela', $parcela['vlr_parcela']);
                $stmt->bindValue(':idcompra', $parcela['idcompra']);
                $stmt->bindValue(':parcela_cp', $parcela['parcela_cp']);
                $stmt->execute();
            } else {
                $sql = "Update tbcontaspagar set vlr_pago = :vlr_pago, situacao = :situacao, dt_pagamento = :dt_pagamento, vlr_parcela = :vlr_parcela where idcompra = :idcompra and parcela_cp = :parcela_cp";
                $stmt = $con->prepare($sql);
                $stmt->bindValue(':vlr_pago', valorPago($parcela, $con));
                $stmt->bindValue(':situacao', PARCELA_BAIXADA);
                $stmt->bindValue(':dt_pagamento', date('Y-m-d'));
                $stmt->bindValue(':vlr_parcela', $parcela['vlr_parcela']);
                $stmt->bindValue(':idcompra', $parcela['idcompra']);
                $stmt->bindValue(':parcela_cp', $parcela['parcela_cp']);
                $stmt->execute();
            }
        } catch (Exception $ex) {
            print_r($ex);
        }
        header('location:' . $_SERVER['REQUEST_URI']);
    } else
    if ($valorPago < $parcela['vlr_parcela']) {
        try {
            $sql = "Update tbcontaspagar set vlr_pago = :vlr_pago, situacao = :situacao, dt_pagamento = :dt_pagamento, vlr_parcela = :vlr_parcela where idcompra = :idcompra and parcela_cp = :parcela_cp";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':vlr_pago', valorPago($parcela, $con));
            $stmt->bindValue(':situacao', PARCELA_BAIXADA_PARCIAL);
            $stmt->bindValue(':dt_pagamento', date('Y-m-d'));
            $stmt->bindValue(':vlr_parcela', $parcela['vlr_parcela']);
            $stmt->bindValue(':idcompra', $parcela['idcompra']);
            $stmt->bindValue(':parcela_cp', $parcela['parcela_cp']);
            $stmt->execute();
        } catch (Exception $ex) {
            die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($ex, true) . "</pre>");
        }
        header('location:' . $_SERVER['REQUEST_URI']);
    }
}

topo(array(
    "pageName" => 'Pagar Parcela',
    "icon" => 'fa fa-money'
));
?>
<div class="row">
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="baixarParcela" name="baixarParcela" role="form" method="post" action="pagar-parcela.php?idcompra=<?php echo $parcela['idcompra']; ?>&parcela_cp=<?php echo $parcela['parcela_cp']; ?>">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="fparcela">Código parcela : #<?php echo $parcela['parcela_cp']; ?></label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="fparcela">Código compra : # <?php echo $parcela['idcompra']; ?></label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela">Data vencimento : <?php echo formatDate($parcela['dt_vencimento'], DATE_BRASIL); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela" style="color:red;">Valor à pagar : <?php echo number_format($parcela['vlr_parcela'] - valorPago($parcela, $con), 2, '.', ''); ?></label>
                                </div>
                            </div>
                        </div>
                        <?php
                        $valorAPagar = $parcela['vlr_parcela'] - valorPago($parcela, $con);
                        ?>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="fparcela">Valor do pagamento</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control" id="valor" name="valor" value="<?php echo number_format($valorAPagar, 2, '.', ''); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="cartao" name="cartao" value="1">Cartão</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="baixarParcela">Baixar </button>
                <button type="reset" class="btn btn-danger"  form="baixarParcela">Cancelar </button>
            </div>
        </div>
    </div>

    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Log baixas da parcela</h1>
            </div>
            <div class="panel-body">
                <!--                fazer select na tbamortizacao e mostrar informações da tbamortizacao<br>
                                Ao baixar parcela verificar valor que foi pago, se valor for maior ou igual valor da parcela deve salvar na tbamortização e tbcontas a receber deve fazer um update na valor pago, em parcelas com baixa parcial, ou baixada fazer uma outra tela para poder estornar a parcela para o fornecedor-->

                <?php
                $totalParcelasAmortizacao = 0;
                $sql = "Select * from tbamortizacao_pagar where idcompra = " . $parcela['idcompra'] . " and parcela_cp = " . $parcela['parcela_cp'];
                $stmt = $con->prepare($sql);
                $stmt->execute();
                $parcelasAmortizacao = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if ($parcelasAmortizacao) {
                    ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Parcela</th>
                                <th>Compra</th>
                                <th>Data</th>
                                <th>Tipo</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($parcelasAmortizacao as $parcelaAmortizacao) {
                                $totalParcelasAmortizacao = $totalParcelasAmortizacao + $parcelaAmortizacao['vlr_pago'];
                                $data = explode(' ', $parcelaAmortizacao['dt_pagto']);
                                ?>
                                <tr>
                                    <td><?php echo $parcelaAmortizacao['idamortizacao_pagar']; ?></td>
                                    <td><?php echo $parcelaAmortizacao['parcela_cp']; ?></td>
                                    <td><?php echo '#' . $parcelaAmortizacao['idcompra']; ?></td>
                                    <td><?php echo formatDate($data[0], DATE_BRASIL) . ' - ' . $data[1]; ?></td>
                                    <td><?php
                                        switch ($parcelaAmortizacao['tipo']) {
                                            case AMORTIZACAO_BAIXA:
                                                echo 'Baixa dinheiro';
                                                break;
                                            case AMORTIZACAO_BAIXA_CARTAO:
                                                echo 'Baixa Cartão';
                                                break;
                                            case AMORTIZACAO_ESTORNO:
                                                echo 'Estorno';
                                                break;
                                        }
                                        ?></td>
                                    <td><?php echo $parcelaAmortizacao['vlr_pago']; ?></td>
                                </tr>
                            <?php } ?>
                            <tr style="color: black;">
                                <td colspan="4" class="text-center">Total</td>
                                <td><?php echo number_format($totalParcelasAmortizacao, 2, '.', ''); ?></td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
            <!--                        <div class="panel-footer">
                                    </div>-->
        </div>
    </div>
    <?php
    rodape(array(
        "js" => array(
            "js/pagar/pagar.js",
        )
    ));
