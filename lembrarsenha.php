<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';

$email = '';
$enviado = '';
if ($_POST) {
    $email = getPost('email');
    try {
        $senhaenviar = strtoupper(substr(md5('estagio2016' . rand(5, 10)), 7, 8));
        $senhasalvar = senha($senhaenviar);
        $db = MyPdo::connect();

        $select = $db->prepare('select * from tbusuario where stemail = :stemail');
        $select->execute(array(
            ':stemail' => $email,
        ));
        $usuario = $select->fetchAll(PDO::FETCH_ASSOC);
        if (!$usuario) {
            addMessage("Email não cadastrado");
            erro();
        } else {
            $enviado = lembrarSenha($senhaenviar, $email);
        }
    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }

    if (!$enviado) {
        addMessage("Erro ao enviar email!");
        erro();
    } else {
        $stmt = $db->prepare('UPDATE tbusuario SET stsenha = :stsenha where stemail = :stemail');
        $stmt->execute(array(
            ':stsenha' => $senhasalvar,
            ':stemail' => $email,
        ));
    }
}
topo(array(
    'login' => '1',
    'css' => array(
        'css/login/lembrarsenha.css',
    )
));
?>
<div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4  col-lg-offset-4 col-lg-4 lembrarsenha">
    <h3 class="form-signin-heading">Lembrar senha</h3>
    <div id="modal-email" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Envio de email</h4>
                </div>
                <div class="modal-body">
                    <p>Email Enviado com sucesso para <?php echo $email; ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <form class="form-signin" role="form" method="post" action="lembrarsenha.php" >
        <div class="form-group">
            <label for="femail">Email</label>
            <input type="email" class="form-control" id="femail" name="email" value="<?php echo $email ?>" placeholder="Digite seu email aqui">
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
        <button type="reset" class="btn btn-danger">Cancelar</button>
    </form>
</div>
<script> var enviado = <?php echo ($enviado) ? 1 : 0; ?>;</script>
<?php
rodape(array
    ("js" => array(
        "js/login/lembrarsenha.js"
    ),
    "login" => "1"
));

