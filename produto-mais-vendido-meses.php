<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
define('MESES', 1);
define('DIAS', 2);
$periodo = '';
$qtdMeses = '';
$situcoesSelecionadas = array();
$situacoes = '';
$situacao = '';
$filtros = '';
$situacoesVenda = array(VENDA_ABERTA, VENDA_CANCELADA, VENDA_FECHADA);
if ($_POST) {
    $qtdMeses = getPost('meses');
    $periodo = isset($_POST['optradio']) ? $_POST['optradio'] : '';
    $situcoesSelecionadas = isset($_POST['situacao']) ? $_POST['situacao'] : '';
    $filtros = '<br><br>Filtrado por:<strong> <br> ';
    foreach ($situcoesSelecionadas as $situacaoSelecionada) {
        switch ($situacaoSelecionada) {
            case VENDA_ABERTA:
                $filtros .= ' <br>Venda aberta<br> ';
                break;
            case VENDA_CANCELADA:
                $filtros .= ' <br>Venda cancelada<br>';
                break;
            case VENDA_FECHADA:
                $filtros .= ' <br>Venda fechada<br> ';
                break;
        }
    }
    if ($situcoesSelecionadas) {
        $situacoes = join(',', $situcoesSelecionadas);
    } else {
        $situcoesSelecionadas = array();
    }
    if (($situacoes == '') or ( $qtdMeses == '') or $periodo == '') {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'Relatório produto mais vendido',
            'tipo' => 'Erro na consulta selecione dados válidos',
        ));
    }
    if ($periodo == MESES) {
        $tipo = 'meses';
    }
    if ($periodo == DIAS) {
        $tipo = 'dias';
    }
} else {
    $qtdMeses = 'x';
    $tipo = ' meses ou dias';
}
$label = array();
$data = array();
topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
        "css/relatorios/produto/produto.css"
    ),
    "pageName" => 'Relatório de produtos mais vendidos nos ultimos ' . $qtdMeses . ' ' . $tipo . '<br><br> Dia atual: ' . date('d/m/Y') . $filtros . '</strong>',
    "icon" => 'fa fa-list'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title remover-print">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="gerarRelatorio" name="gerarRelatorio" role="form" method="post" action="produto-mais-vendido-meses.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="pesquisa">Método de pesquisa* </label>
<!--                                    <select class="form-control" id="pesquisa" name="pesquisa">
                                        <option value="1">Código de barra</option>
                                        <option value="2">Descrição</option>
                                    </select>-->
                                    <div class="radio">
                                        <label><input checked="" <?php echo $periodo == MESES ? 'checked' : ''; ?> type="radio" class="metodo" value="1" name="optradio">Meses</label>
                                    </div>
                                    <div class="radio">
                                        <label><input <?php echo $periodo == DIAS ? 'checked' : ''; ?>  type="radio" class="metodo" value="2" name="optradio">Dias</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="fclassificacao">Quantidade de meses ou dias*</label>
                                    <input type="number" min="1" class="form-control" id="meses" value="<?php echo $qtdMeses; ?>" name="meses">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="pesquisa">Selecionar status das vendas* </label>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array(VENDA_ABERTA, $situcoesSelecionadas) ? 'checked' : ''; ?> type="checkbox" class="metodo" value="1" name="situacao[]">Venda aberta</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array(VENDA_FECHADA, $situcoesSelecionadas) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="0" name="situacao[]">Venda Fechada</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input <?php echo in_array(VENDA_CANCELADA, $situcoesSelecionadas) ? 'checked' : ''; ?>  type="checkbox" class="metodo" value="2" name="situacao[]">Venda Cancelada</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php if ($_POST) {
                                    ?>
                                    <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if ($_POST) {
                    try {
                        $qtdMeses = getPost('meses');
                        $optradio = getPost('optradio');

                        if ($optradio == MESES) {
                            $metodo = 'MONTH';
                            $relatorioTipo = 'Mêses';
                        }
                        if ($optradio == DIAS) {
                            $metodo = 'DAY';
                            $relatorioTipo = 'Dias';
                        }

                        $con = MyPdo::connect();
                        $sql = "select p.produto, sum(i.qtde_item) as quantidade, i.idproduto from tbitem_venda as i inner join tbproduto as p on i.idproduto = p.idproduto  inner join tbvenda as v on i.idvenda = v.idvenda where date(v.data_venda) BETWEEN SUBDATE(CURRENT_DATE, INTERVAL :meses $metodo ) AND CURRENT_DATE and v.situacao in ($situacoes) group by i.idproduto order by quantidade desc ";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':meses', $qtdMeses);
                        $stmt->execute();
                        $produtos = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    } catch (Exception $e) {
                        //var_dump($e);
                        exit;
                    }
                    ?>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h1 class="panel-title remover-print">Produtos mais vendidos nos ultimos <?php echo $qtdMeses . ' - ' . $relatorioTipo; ?></h1>
                                </div>
                                <div class="panel-body">
                                    <!--<div class="col-xs-12 col-md-12">-->
                                    <div class="box-chart">
                                        <canvas id="prodMaisVendido"></canvas>
                                    </div>
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($produtos as $produto) {
                                $label[] = '#' . $produto['idproduto'] . '-' . $produto['produto'];
                                $data[] = (int) $produto['quantidade'];
                                ?>
                                <tr>
                                    <td> <?php echo $produto['idproduto']; ?></td>
                                    <td> <?php echo $produto['produto']; ?></td>
                                    <td> <?php echo $produto['quantidade']; ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                <button type="reset" class="btn btn-danger"  form="gerarRelatorio">Cancelar </button>
                <?php if ($_POST) {
                    ?>
                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                <?php }
                ?>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dados = <?php echo json_encode($data); ?>;
    var label = <?php echo json_encode($label); ?>;
</script>
<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js",
        'js/charts/chartsjs/Chart.min.js',
        "js/relatorios/produto/produto.js",
    )
));
?>