<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$erro = '';
$where = '';
$pagina = getGet('p', 1);
$query = getGet('q', '');
$editar = true;

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('lancamento-conta-editar.php', $telasPermissao)) {
    $editar = false;
}

if ($query) {
    $where = "and descLancamento like '$query%'";
}

$totalRegPag = "10";

$inicio = ($pagina - 1) * $totalRegPag;
$hoje = (date("Y-m-d"));
$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS c.*, u.stnome, h.dshistorico from tbcaixa as c inner join tbhistorico as h on c.idhistorico = h.idhistorico inner join tbusuario as u on c.idusuario = u.idusuario where dataAbertura = '$hoje' {$where}  order by c.dataLancamento desc LIMIT $inicio,$totalRegPag");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;
$lancamentos = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$lancamentos) {
    $erro = 'Sem lançamentos para listagem';
}
topo(array(
    "css" => array(
        "css/caixa/caixa.css"
    ),
    "icon" => "fa fa-money",
    "pageName" => " Pesquisar Lançamentos dia " . formatDate($hoje, DATE_BRASIL)
));
?>
<div id="modalExcluirLancamento"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="">
                    <div class="col-xs-11">
                        <div class="form-group">
                            <label class="sr-only" for="fquery">Pesquisa</label>
                            <input value="<?php echo $query; ?>" type="search" class="form-control" id="fquery" name="q" placeholder="Pesquisar">
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a  href="lancamentos.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                    </div>
                    <div class="col-xs-1">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class=" fa fa-plus fa-fw"></i>
                            </button>
                            <ul class="dropdown-menu slidedown">
                                <li>
                                    <a href="lancamento-conta.php">
                                        <i class="fa fa-plus fa-fw"></i> Novo lançamento
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Descrição</th>
                                <th class="text-center">Data Abertura de caixa</th>
                                <th class="text-center">Data lançamento</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Valor</th>
                                <th class="text-center">Usuário</th>
                                <th class="text-center"> Histórico</th>
                                <th class="text-center"> Editar</th>
                                <th class="text-center">Exluir</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lancamentos as $lancamento) { ?>
                                <tr class="text-center" data-id="<?php echo $lancamento['idlancamento']; ?>">
                                    <td> <?php echo $lancamento['idlancamento']; ?></td>
                                    <td> <?php echo $lancamento['descLancamento']; ?></td>
                                    <td> <?php echo formatDate($lancamento['dataAbertura'], DATE_BRASIL); ?></td>
                                    <?php
                                    $data = explode(" ", $lancamento['dataLancamento']);
                                    $hora = $data[1];
                                    $data = formatDate($data[0], DATE_BRASIL);
                                    ?>
                                    <td> <?php echo $data . ' ' . $hora; ?></td>
                                    <td> <?php echo $lancamento['tipoLancamento'] == LANCAMENTO_CREDITO ? 'Crédito' : 'Débito'; ?></td>
                                    <td> <?php echo 'R$ ' . number_format($lancamento['vlLancamento'], 2, ',', '.'); ?></td>
                                    <td> <?php echo $lancamento['stnome']; ?></td>
                                    <td> <?php echo $lancamento['dshistorico']; ?></td>
                                    <td>
                                        <?php
                                        if ($lancamento['inautomatico'] == 1) {
                                            echo '<a title = "Lançamento automatico" href="javascript::;" ><i class="fa fa-exclamation-triangle"></i></a>';
                                        } else {
                                            ?>
                                            <a title = "Editar" href="lancamento-conta-editar.php?id=<?php echo $lancamento['idlancamento']; ?>" ><i class="fa fa-edit"></i></a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($lancamento['inautomatico'] == 1) {
                                            echo '<a title = "Lançamento automatico" href="javascript::;" ><i class="fa fa-exclamation-triangle"></i></a>';
                                        } else {
                                            ?>
                                            <a title = "Excluir Lançamento" href="#" data-id="<?php echo $lancamento['idlancamento']; ?>" class="lancamemtoExcluir"><i class="fa fa-trash-o"></i></a>
                                        <?php } ?>
                                    </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php echo $erro; ?>
            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="lancamentos.php?p=<?php echo $anterior; ?>&q=<?php echo $query; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="lancamentos.php?p=<?php echo $proximo; ?>&q=<?php echo $query; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>



<?php
rodape(array(
    "js" => array(
        "js/lancamento/lancamento.js",
    )
));
?>
<script>
    var query = '<?php echo $query; ?>';
</script>

