<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$erro = '';
$where = '';
$pagina = getGet('p', 1);
$query = getGet('q', '');
$situacao = getGet('situacao', '');
$editar = true;
$condicoes = array();

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('venda-aberta.php', $telasPermissao)) {
    $editar = false;
}



if ($query) {
    $condicoes[] = " nome_razao like '$query%'";
}
if ($situacao) {
    $condicoes[] = " v.situacao = $situacao";
}

if ($condicoes) {
    $condicoes = join(' or ', $condicoes);
    $where = 'where ' . $condicoes;
}

$totalRegPag = "15";

$inicio = ($pagina - 1) * $totalRegPag;

$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS v.*, c.nome_razao as nome_cliente from tbvenda as v inner join tbcliente as c on v.idcliente = c.idcliente {$where} order by v.idvenda desc LIMIT $inicio,$totalRegPag ");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$vendas = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$vendas) {
    $erro = 'Sem vendas para listagem';
}

topo(array(
    "css" => array(
        "css/venda/venda.css"
    ),
    "icon" => "fa fa-money",
    "pageName" => " Pesquisar Vendas"
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="">
                    <div class="col-xs-11">
                        <div class="form-group">
                            <label class="sr-only" for="fquery">Pesquisa</label>
                            <input value="<?php echo $query; ?>" type="search" class="form-control" id="fquery" name="q" placeholder="Pesquisar">
                            <select name="situacao" class="form-control">
                                <option value="">Situação</option>
                                <option value="<?php echo VENDA_ABERTA; ?>" <?php echo $situacao == VENDA_ABERTA ? 'selected' : ''; ?>>Venda aberta</option>
                                <option value="<?php echo VENDA_CANCELADA; ?>" <?php echo $situacao == VENDA_CANCELADA ? 'selected' : ''; ?>>Venda cancelada</option>
                                <option value="<?php echo VENDA_FECHADA; ?>" <?php echo $situacao == VENDA_FECHADA ? 'selected' : ''; ?>>Venda fechada</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a type="reset" a="" href="vendas.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                    </div>
                    <div class="col-xs-1">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class=" fa fa-plus fa-fw"></i>
                            </button>
                            <ul class="dropdown-menu slidedown">
                                <li>
                                    <a href="clientes.php">
                                        <i class="fa fa-plus fa-fw"></i> Nova Venda
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Data</th>
                                <th>Situação</th>
                                <th>Cliente</th>
                                <th>Opções</th>
                                <th>Receber</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vendas as $venda) { ?>
                                <?php
                                $data = explode(' ', $venda['data_venda']);
                                ?>
                                <tr>
                                    <td> <?php echo $venda['idvenda']; ?></td>
                                    <td> <?php echo formatDate($data[0], DATE_BRASIL) . '-' . $data[1]; ?></td>
                                    <td> <?php
                                        if ($venda['situacao'] == VENDA_ABERTA) {
                                            ?>
                                            <span href="#"class="btn btn-success btn-xs situacao">Aberta</span>
                                            <?php
                                        }
                                        if ($venda['situacao'] == VENDA_FECHADA) {
                                            ?>
                                            <span href="#"class="btn btn-warning btn-xs situacao">Fechada</span>
                                            <?php
                                        }
                                        if ($venda['situacao'] == VENDA_CANCELADA) {
                                            ?>
                                            <span href="#"class="btn btn-danger btn-xs situacao">Cancelada</span>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td> <?php echo $venda['nome_cliente']; ?></td>
                                    <?php
                                    if ($editar) {
                                        if ($venda['situacao'] == VENDA_ABERTA) {
                                            ?>
                                            <td><a href="venda-aberta.php?idvenda=<?php echo $venda['idvenda']; ?>" title="Editar venda"><i class="fa fa-play"></i></a></td>
                                            <?php
                                        }
                                        if ($venda['situacao'] == VENDA_FECHADA) {
                                            ?>
                                            <td><a href="venda-fechada.php?idvenda=<?php echo $venda['idvenda']; ?>" title="Estornar"><i class="fa fa-undo"></i></a></td>
                                            <?php
                                        }
                                        if ($venda['situacao'] == VENDA_CANCELADA) {
                                            ?>
                                            <td><a href="venda-cancelada.php?idvenda=<?php echo $venda['idvenda']; ?>" title="Exibir venda"><i class="fa fa-list"></i></a></td>
                                            <?php
                                        }
                                    }
                                    if ($venda['situacao'] == VENDA_FECHADA) {
                                        ?>
                                        <td><a href="receber.php?idvenda=<?php echo $venda['idvenda']; ?>&idcliente=<?php echo $venda['idcliente']; ?>" title="Receber parcela"><i class="fa fa-list-alt"></i></a></td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="vendas.php?p=<?php echo $anterior; ?>&q=<?php echo $query; ?>&situacao=<?php echo $situacao; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php
                    for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="vendas.php?p=<?php echo $proximo; ?>&q=<?php echo $query; ?>&situacao=<?php echo $situacao; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>



<script>
    var query = '<?php echo $query; ?>';
    var situacao = '<?php echo $situacao; ?>';
    var editar = '<?php echo $editar; ?>';
</script>
<?php
rodape(array(
    "js" => array(
        "js/venda/venda.js",
    )
));
?>

