<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
require 'lib/telas.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}


$con = MyPdo::connect();
$papel = '';
$telasPermissao = '';
$_SESSION['erro'] = false;

if ($_POST) {
    $papel = getPost('papel');

    if (isset($_POST['urls'])) {
        $telasPermissao = join(',', $_POST['urls']);
    }

    if (!$papel) {
        addMessage('Preencha o campo Descrição');
        erro();
    }

    try {
        $stmt = $con->prepare("Select UPPER(dspapel) from tbpapel where dspapel = UPPER(:dspapel)");
        $stmt->bindValue(':dspapel', $papel);
        $stmt->execute();
        $consulta = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        echo $e;
    }

    if ($consulta) {

        addMessage('Papel ja existe, insira papel com descrição diferente');
        erro();
    }

    if (!getSession('erro')) {
        try {
            $stmt = $con->prepare("Insert into tbpapel (dspapel, telas, situacao) values (:dspapel, :telas, :situacao)");
            $stmt->bindValue(':dspapel', $papel);
            $stmt->bindValue(':telas', $telasPermissao);
            $stmt->bindValue(':situacao', PAPEL_ATIVO);
            $stmt->execute();

            sucessInsertUpdate(array(
                'page' => 'papeis.php',
                'origem' => 'papel',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            echo $e;
        }
    }
}

topo(array(
    'css' => array(
        "css/papel/papel.css",
    ),
    'pageName' => ' Cadastrar Papel',
    'icon' => 'fa fa-key',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarPapeis" name="cadastrarPapeis" role="form" method="post" action="papeis-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="papel">Descrição*</label>
                                    <input type="text" class="form-control" id="papel" name="papel" placeholder="Descrição Papel " value="<?php echo $papel; ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h1 class="panel-title">Telas</h1>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <?php foreach ($telas as $tela) { ?>
                                                        <div class="col-xs-12 col-sm-6  col-md-6">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h1 class="panel-title"><?php echo $tela['titulo']; ?></h1>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <?php
                                                                    foreach ($tela['telas'] as $link => $titulo) {
                                                                        ?>
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input type="checkbox" value="<?php echo $link; ?>" <?php echo (in_array($link, explode(',', $telasPermissao))) ? 'checked' : ''; ?> name="urls[]">
                                                                                <?php echo " $titulo"; ?>
                                                                            </label>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        </form>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="cadastrarPapeis">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="cadastrarPapeis">Cancelar </button>

        </div>
    </div>
</div>

<?php
rodape(array(
    'js' => array(
        'js/papel/papel.js',
    )
));
?>