<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
require 'lib/telas.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$con = MyPdo::connect();
$cd = getGet('cd');
$situacao = '';
$dspapel = '';
$codigo = '';
$telasPermissao = '';

try {
    $stmt = $con->prepare("Select * from tbpapel where idpapel = :idpapel");
    $stmt->bindValue(':idpapel', $cd);
    $stmt->execute();
    $papel = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$papel) {
        editarErro(array(
            'page' => 'papeis.php',
            'origem' => 'papel',
            'tipo' => 'Erro na atualização'
        ));
    }
} catch (Exception $e) {
    echo $e;
}
if ((!isset($_GET['cd'])) and ( !isset($_POST['codigo']))) {
    editarErro(array(
        'page' => 'papeis.php',
        'origem' => 'papel',
        'tipo' => 'Erro na atualização'
    ));
}
if ($_POST) {
    $dspapel = getPost('papel');
    $situacao = getPost('situacao');
    $codigo = getPost('codigo');
    if ($situacao) {
        $situacao = PAPEL_ATIVO;
    } else {
        $situacao = PAPEL_INATIVO;
    }


    if (isset($_POST['urls'])) {
        $telasPermissao = join(',', $_POST['urls']);
    } else {
        addMessage('Selecione uma opção');
        erro();
    }

    if (!$papel) {
        addMessage('Preencha o campo Descrição');
        erro();
    }

    try {
        $stmt = $con->prepare("Select UPPER(dspapel) from tbpapel where dspapel = UPPER(:dspapel)");
        $stmt->bindValue(':dspapel', $dspapel);
        $stmt->execute();
        $consulta = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        echo $e;
    }

    if (($consulta['UPPER(dspapel)'] != strtoupper($papel['dspapel'])) and ( $consulta)) {
        addMessage('Papel ja existe, insira papel com descrição diferente');
        erro();
    }
    if (!getSession('erro')) {
        try {
            $stmt = $con->prepare("Update tbpapel set dspapel = :dspapel, telas = :telas, situacao = :situacao where"
                    . " idpapel = :cd");
            $stmt->bindValue(':dspapel', $dspapel);
            $stmt->bindValue(':telas', $telasPermissao);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':cd', $codigo);
            $stmt->execute();

            sucessInsertUpdate(array(
                'page' => 'papeis.php',
                'origem' => 'papel',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            echo $e;
        }
    }
}


topo(array(
    "pageName" => 'Editar Papel #' . $cd,
    "icon" => 'fa fa-key',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="editarPapeis" name="editarPapeis" role="form" method="post" action="papeis-editar.php?cd=<?php echo $cd; ?>">
                    <input type="hidden" value="<?php echo $cd; ?>" name="codigo">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="papel">Descrição*</label>
                                    <input type="text" class="form-control" id="papel" name="papel" placeholder="Descrição Papel " value="<?php echo $dspapel ? $dspapel : $papel['dspapel']; ?>">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h1 class="panel-title">Telas</h1>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <?php foreach ($telas as $tela) { ?>
                                                            <div class="col-xs-12 col-sm-6  col-md-6">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h1 class="panel-title"><?php echo $tela['titulo']; ?></h1>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <?php
                                                                        foreach ($tela['telas'] as $link => $titulo) {
                                                                            ?>
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" value="<?php echo $link; ?>" <?php
                                                                                    if ($telasPermissao) {
                                                                                        echo (in_array($link, explode(',', $telasPermissao))) ? 'checked' : '';
                                                                                    } else {
                                                                                        echo(in_array($link, explode(',', $papel['telas']))) ? 'checked' : '';
                                                                                    }
                                                                                    ?> name="urls[]">
                                                                                           <?php echo " $titulo"; ?>
                                                                                </label>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input form="editarPapeis" type="checkbox" name="situacao" <?php echo (PAPEL_ATIVO == $papel['situacao']) ? 'checked' : ''; ?> > Ativar Papel?
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary" form="editarPapeis">Salvar </button>
                    <button type="reset" class="btn btn-danger"  form="editarPapeis">Cancelar </button>

                </div>
            </div>
        </div>
    </div>

    <?php
    rodape(array(
        'js' => array(
            'js/papel/papel.js',
        )
    ));
    ?>
