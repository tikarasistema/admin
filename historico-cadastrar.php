<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$dshistorico = '';
$tipohistorico = '';
$inautomatico = null;
$_SESSION['erro'] = false;
if ($_POST) {
    $con = MyPdo::connect();
    $dshistorico = getPost('dshistorico');
    $tipohistorico = getPost('tipohistorico');
    $inautomatico = getPost('inautomatico', null);

    if ($inautomatico) {
        $inautomatico = LANCAMENTO_AUTOMATICO;
    } else {
        $inautomatico = null;
    }

    $sql = "Select UPPER(dshistorico) from tbhistorico where dshistorico = UPPER(:dshistorico)";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dshistorico', $dshistorico);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Histórico cadastrado anteriormente");
        erro();
    }

    if ($inautomatico != null and $inautomatico != LANCAMENTO_AUTOMATICO) {
        addMessage("Selecione valor válido para tipo automatico");
        erro();
    }

    if (empty($dshistorico)) {
        addMessage("Preencha campo descrição");
        erro();
    }

    if (empty($tipohistorico)) {
        addMessage('Selecione tipo de histórico');
        erro();
    }

    if (!in_array($tipohistorico, $tiposLancamentos)) {
        addMessage("Selecione tipo de histórico válido");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Insert into tbhistorico (dshistorico, tipohistorico, situacao, inautomatico) values (:dshistorico, :tipohistorico, :situacao, :inautomatico)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dshistorico', $dshistorico);
            $stmt->bindValue(':situacao', HISTORICO_ATIVO);
            $stmt->bindValue(':tipohistorico', $tipohistorico);
            $stmt->bindValue(':inautomatico', $inautomatico);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'historico.php',
                'origem' => 'histórico',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}
topo(array(
    "pageName" => 'Cadastrar Histórico',
    "icon" => 'fa fa-signal'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <div class="col-xs-12">
                    <form id="cadastrarHistorico" name="cadastrarHistorico" role="form" method="post" action="historico-cadastrar.php">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fhistorico">Descrição*</label>
                                    <input type="text" class="form-control" id="historico" name="dshistorico" placeholder="Digite nome do histórico " value="<?php echo $dshistorico; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="tipohistorico">Tipo de histórico*</label>
                                    <select class="form-control" name="tipohistorico" id="tipohistorico">
                                        <option value="0">Selecione tipo de histórico</option>
                                        <option value="<?php echo LANCAMENTO_CREDITO; ?>" <?php echo $tipohistorico == LANCAMENTO_CREDITO ? 'selected' : ''; ?>>Crédito</option>
                                        <option value="<?php echo LANCAMENTO_DEBITO; ?>"<?php echo $tipohistorico == LANCAMENTO_DEBITO ? 'selected' : ''; ?>>Débito</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="row">
                                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>   <input form="cadastrarHistorico" type="checkbox" name="inautomatico" <?php echo $inautomatico ? 'checked' : ''; ?>  > Histórico automático?</label>
                                                            </div>
                                                            <div class="help-block">Selecionar esta opção apenas para históricos automaticos EX: baixa de parcela</div>
                                                        </div>
                                                    </div>
                                                </div>-->

                    </form>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarHistorico">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarHistorico">Cancelar </button>
            </div>
        </div>
    </div>
</div>
</div>
<?php rodape(array()); ?>