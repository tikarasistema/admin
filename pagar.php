<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$erro = '';
$where = '';
$pagina = getGet('p', 1);
$situacao = getGet('situacao', '');
$editar = true;
$condicoes = array();
$con = MyPdo::connect();

$idfornecedor = getGet('idfornecedor', 0);
$idcompra = getGet('idcompra', 0);

if ($idfornecedor == 0) {
    editarErro(array(
        'page' => 'compras.php',
        'origem' => 'a pagar',
        'tipo' => 'Erro no pagamento'
    ));
}

if ($idfornecedor != 0) {
    $sql = "Select * from tbfornecedor where idfornecedor = :idfornecedor";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idfornecedor', $idfornecedor);
    $stmt->execute();
    $fornecedor = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$fornecedor) {
        editarErro(array(
            'page' => 'pagar.php',
            'origem' => 'a pagar',
            'tipo' => 'Erro no pagamento'
        ));
    }
}
if ($idcompra != 0) {
    $sql = "Select * from tbcompra where idcompra = :idcompra";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcompra', $idcompra);
    $stmt->execute();
    $parcela = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$parcela) {
        editarErro(array(
            'page' => 'pagar.php',
            'origem' => 'a pagar',
            'tipo' => 'Erro no pagamento'
        ));
    }
}

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('pagar.php', $telasPermissao)) {
    $editar = false;
}


if ($situacao) {
    if ($situacao != PARCELA_VENCIDA) {
        $condicoes[] = " p.situacao = '$situacao'";
    } elseif ($situacao == PARCELA_VENCIDA) {
        $condicoes[] = " ((dt_vencimento < CURRENT_DATE) and (situacao in ('" . PARCELA_ABERTA . "', '" . PARCELA_BAIXADA_PARCIAL . "')) and (dt_pagamento < SUBDATE(CURRENT_DATE, INTERVAL 1 DAY )))";
    }
}
if ($idfornecedor) {
    $condicoes[] = " p.idfornecedor = $idfornecedor";
}
if ($idcompra) {
    $condicoes[] = " p.idcompra = $idcompra";
}

if ($condicoes) {
    $condicoes = join(' and ', $condicoes);
    $where = 'where ' . $condicoes;
}

$totalRegPag = "15";

$inicio = ($pagina - 1) * $totalRegPag;

$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS p.* from tbcontaspagar as p {$where} ORDER BY p.idcompra DESC, p.parcela_cp ASC LIMIT $inicio,$totalRegPag ");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$parcelas = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$parcelas) {
    $erro = 'Sem parcelas para listagem';
}

topo(array(
    "css" => array(
        "css/compra/compra.css"
    ),
    "icon" => "fa fa-usd",
    "pageName" => " Pagamento parcela"
));
?>
<div class="row">
    <div class="col-xs-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <select name="situacao" id="situacao" class="form-control">
                                <option value="">Situação</option>
                                <option value="<?php echo PARCELA_ABERTA; ?>" <?php echo $situacao == PARCELA_ABERTA ? 'selected' : ''; ?>>Parcela aberta</option>
                                <option value="<?php echo PARCELA_BAIXADA; ?>" <?php echo $situacao == PARCELA_BAIXADA ? 'selected' : ''; ?>>Parcela paga</option>
                                <option value="<?php echo PARCELA_CARTAO; ?>" <?php echo $situacao == PARCELA_CARTAO ? 'selected' : ''; ?>>Parcela cartão</option>
                                <option value="<?php echo PARCELA_ESTORNADA; ?>" <?php echo $situacao == PARCELA_ESTORNADA ? 'selected' : ''; ?>>Parcela estornada</option>
                                <option value="<?php echo PARCELA_BAIXADA_PARCIAL; ?>" <?php echo $situacao == PARCELA_BAIXADA_PARCIAL or $situacao == PARCELA_BAIXADA_PARCIAL_ESTORNO ? 'selected' : ''; ?>>Parcela Paga parcialmente</option>
                                <option value="<?php echo PARCELA_VENCIDA; ?>" <?php echo $situacao == PARCELA_VENCIDA ? 'selected' : ''; ?>>Parcela vencida</option>
<!--                                <option value="<?php echo PARCELA_BAIXADA_PARCIAL_CARTAO; ?>" <?php echo $situacao == PARCELA_VENCIDA ? 'selected' : ''; ?>>Parcela baixada parcial cartão</option>-->
                            </select>
                        </div>
                        <button type="button" id="pesquisar" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a type="reset" a="" href="pagar.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                        <?php if ($idcompra != 0) { ?>
                                            <!--<a type="button" href="boleto-pagar.php?idfornecedor=<?php echo $idfornecedor; ?>&idcompra=<?php echo $idcompra; ?>" class="btn btn-warning" style="float:right; margin:0 10px; "><i class="fa fa-money"></i> Gerar Boleto </a>-->
                        <?php } ?>
                        <a type="button" href="relatorio-pagar.php?idfornecedor=<?php echo $idfornecedor; ?>" class="btn btn-success" style="float:right;"><i class="fa fa-list"></i> Gerar relatório </a>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                                <th>Parcela</th>
                                <th>Compra</th>
                                <th>Data Emissão</th>
                                <th>Data Vencimento</th>
                                <th>Data Pagamento</th>
                                <th>Valor parcela</th>
                                <th>Valor pago</th>
                                <th>Valor à pagar</th>
                                <th>Situação</th>
                                <th class="text-center">Pagar parcela</th>
                                <th class="text-center">Estornar parcela</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($parcelas as $parcela) {
                                $data1 = new DateTime(date('Y-m-d'));
                                $data2 = new DateTime($parcela['dt_vencimento']);

                                $intervalo = $data1->diff($data2);

                                if ($intervalo->days >= 0 and $intervalo->invert == 1) {
                                    if ($parcela['situacao'] == PARCELA_ABERTA) {
                                        if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class = warning';
                                        }
                                        if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                            $parcelaAtrasada = 'class= danger';
                                        }
                                    }
                                } else {
                                    $parcelaAtrasada = '';
                                }

                                if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                    $parcelaAtrasada = 'class= success';
                                }
                                if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                    $parcelaAtrasada = '';
                                }
                                if ($parcela['situacao'] == PARCELA_CARTAO) {
                                    $parcelaAtrasada = '';
                                }
                                if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_CARTAO) {
                                    if ($intervalo->days >= 1 and $intervalo->invert == 1) {
                                        $parcelaAtrasada = 'class = warning';
                                    } else
                                    if ($intervalo->days >= 31 and $intervalo->invert == 1) {
                                        $parcelaAtrasada = 'class= danger';
                                    } else
                                        $parcelaAtrasada = 'class=parcial';
                                }
                                ?>

                                <tr <?php echo $parcelaAtrasada; ?>>
                                    <td> <?php echo $parcela['parcela_cp']; ?></td>
                                    <td><a target="_blank" href="compra-fechada.php?idcompra=<?php echo $parcela['idcompra']; ?>"><?php echo $parcela['idcompra']; ?></a></td>
                                    <td> <?php echo formatDate($parcela['dt_emissao'], DATE_BRASIL); ?></td>
                                    <td> <?php echo formatDate($parcela['dt_vencimento'], DATE_BRASIL); ?></td>
                                    <td> <?php echo formatDate($parcela['dt_pagamento'], DATE_BRASIL); ?></td>
                                    <td> <?php echo number_format($parcela['vlr_parcela'], 2, '.', ''); ?></td>
                                    <td> <?php echo $parcela['vlr_pago']; ?></td>
                                    <td> <?php
                                        if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                            echo '0.00';
                                        } else {
                                            echo number_format($parcela['vlr_parcela'] - $parcela['vlr_pago'], 2, '.', '');
                                        }
                                        ?>
                                    </td>
                                    <td> <?php
                                        if ($parcela['situacao'] == PARCELA_ABERTA) {
                                            echo 'ABERTA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_CARTAO) {
                                            echo 'PARCIAL CARTÃO';
                                        }
                                        if ($parcela['situacao'] == PARCELA_BAIXADA) {
                                            echo 'BAIXADA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_CARTAO) {
                                            echo 'CARTÃO';
                                        }
                                        if ($parcela['situacao'] == PARCELA_ESTORNADA) {
                                            echo 'ESTORNADA';
                                        }
                                        if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO) {
                                            echo 'BAIXA PARCIAL';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php
                                        if ($parcela['situacao'] == PARCELA_ABERTA or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO) {
                                            ?>
                                            <a href="pagar-parcela.php?idcompra=<?php echo $parcela['idcompra']; ?>&parcela_cp=<?php echo $parcela['parcela_cp']; ?>"><i class="fa fa-money"></i></a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($parcela['situacao'] == PARCELA_BAIXADA_PARCIAL_ESTORNO or $parcela['situacao'] == PARCELA_BAIXADA_PARCIAL or $parcela['situacao'] == PARCELA_BAIXADA) { ?>
                                            <a href="estornar-parcela-pagar.php?idcompra=<?php echo $parcela['idcompra']; ?>&parcela_cp=<?php echo $parcela['parcela_cp']; ?>"><i class="fa fa-sign-out"></i></a>
                                        <?php }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="pagar.php?p=<?php echo $anterior; ?>&idfornecedor=<?php echo $idfornecedor; ?>&situacao=<?php echo $situacao; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php
                    for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="pagar.php?p=<?php echo $proximo; ?>&idfornecedor=<?php echo $idfornecedor; ?>&situacao=<?php echo $situacao; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Legenda</strong>
            </div>
            <div class="panel-body">
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#d0e9c6;"></i> <strong>Parcela paga</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #d2eafd"></i> <strong>Parcela paga parcialmente</strong>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color:#ffd699;"></i> <strong>Vencido entre 1  e 29 dias</strong><br>
                <hr>
                <i class="fa fa-stop fa-2" aria-hidden="true" style="color: #f7a6a4"></i> <strong>Vencido à mais de 30 dias</strong>
            </div>
        </div>
    </div>
</div>



<script>
    var idfornecedor = '<?php echo $idfornecedor; ?>';
    var idcompra = '<?php echo $idcompra; ?>';
    var situacao = '<?php echo $situacao; ?>';
</script>
<?php
rodape(array(
    "js" => array(
        "js/pagar/pagar.js",
    )
));
?>