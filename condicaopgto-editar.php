<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
require 'lib/telas.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

define('SEM_ENTRADA', 0);
define('COM_ENTRADA', 1);

$con = MyPdo::connect();
$condicaopgto = '';
$entrada = '';
$qtdparcela = '';
$idcondicaopgto = '';
$_SESSION['erro'] = false;
if (isset($_GET['cd'])) {
    $idcondicaopgto = $_GET['cd'];

    $sql = "Select * from tbcondpgto where idcondpgto = :idcondpgto";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idcondpgto', $idcondicaopgto);
    $stmt->execute();
    $condicaopgtoAtual = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$condicaopgtoAtual) {
        editarErro(array(
            'page' => 'condicaopgto.php',
            'origem' => 'condição pagamento',
            'tipo' => 'Erro na atualização'
        ));
    }
}
if ((!isset($_GET['cd'])) and ( !isset($_POST['cd']))) {
    editarErro(array(
        'page' => 'condicaopgto.php',
        'origem' => 'condição pagamento',
        'tipo' => 'Erro na atualização'
    ));
}

if ($_POST) {
    $condicaopgto = $_POST['condicaopgto'];
    $entrada = $_POST['entrada'];
    if ($entrada) {
        $entrada = COM_ENTRADA;
    } else {
        $entrada = SEM_ENTRADA;
    }
    $qtdparcela = $_POST['qtdparcelas'];
    $idcondicaopgto = $_POST['cd'];
    $situacao = getPost('situacao');
    if ($situacao) {
        $situacao = CONDPGTO_ATIVO;
    } else {
        $situacao = CONDPGTO_INATIVO;
    }

    try {
        $sql = 'Select UPPER(ds_condicao) from tbcondpgto where ds_condicao = UPPER(:ds_condicao) and '
                . 'idcondpgto != :idcondpgto';
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':ds_condicao', $condicaopgto);
        $stmt->bindValue(':idcondpgto', $idcondicaopgto);
        $stmt->execute();
        $condicaopgtoSelect = $stmt->fetch(PDO::FETCH_COLUMN);
    } catch (Exception $e) {
        die($e);
    }

    if ($condicaopgtoSelect) {
        addMessage('Condição pagamento ja existe, insira condição pagamento diferente');
        erro();
    }

    if (empty($condicaopgto)) {
        addMessage('Campo descrição esta vazio, insira uma descrição');
        erro();
    }
    if (($entrada != COM_ENTRADA) and ( $entrada != SEM_ENTRADA)) {
        addMessage('Campo entrada esta com valor inválido, selecione um valor válido');
        erro();
    }
    if (empty($qtdparcela)) {
        addMessage('Campo quantidade de parcelas esta vazio, digite quantidade');
        erro();
    }
    if ($qtdparcela < 0) {
        addMessage('Campo quantidade de parcelas esta com valor inválido, digite valor válido');
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Update tbcondpgto  set ds_condicao = :ds_condicao , qtde_parcela = :qtde_parcela , entrada = :entrada, situacao = :situacao where idcondpgto = :id";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':ds_condicao', $condicaopgto);
            $stmt->bindValue(':qtde_parcela', $qtdparcela);
            $stmt->bindValue(':entrada', $entrada);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindValue(':id', $idcondicaopgto);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'condicaopgto.php',
                'origem' => 'condição pagamento',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            die('Erro ao atualizar condição de pagamento ' . $e);
        }
    }
} else {
    $condicaopgto = $condicaopgtoAtual['ds_condicao'];
    $entrada = $condicaopgtoAtual['entrada'];
    $idcondicaopgto = $condicaopgtoAtual['idcondpgto'];
    $qtdparcela = $condicaopgtoAtual['qtde_parcela'];
}

topo(array(
    'css' => array(
        "css/condicaopgto/condicaopgto.css",
    ),
    'pageName' => ' Editar Condição Pagamento #' . $idcondicaopgto,
    'icon' => 'fa fa-credit-card',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="editarCondpgto" name="editarCondpgto" role="form" method="post" action="condicaopgto-editar.php">
                    <input type="hidden" name="cd" value="<?php echo $idcondicaopgto; ?>">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="form-group">
                                    <label for="papel">Descrição*</label>
                                    <input type="text" class="form-control" id="condicaopgto" name="condicaopgto" placeholder="Descrição condição pagamento " value="<?php echo $condicaopgto ? $condicaopgto : $condicaopgtoAtual['ds_condicao']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input form="editarCondpgto" type="checkbox" name="entrada" <?php echo $entrada == COM_ENTRADA ? 'checked' : ''; ?>  > Entrada?
                                        </label>
                                    </div>
                                    <div class="help-block" >Ao selecionar esta opção, condição de pagamento terá entrada.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="papel">Quantidade de parcelas*</label>
                                    <input type="number" min="0" class="form-control" id="papel" name="qtdparcelas"  value="<?php echo $qtdparcela ? $qtdparcela : $condicaopgtoAtual['qtde_parcela']; ?>">
                                    <div class="help-block" >Informar a quantidade total de parcelas, Ex: entrada + 2 informar 3.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="fcondicaopgto"><input id="fcondicaopgto" name="situacao" type="checkbox" <?php
                                            if ($_GET) {
                                                echo $condicaopgtoAtual['situacao'] == CONDPGTO_ATIVO ? 'checked' : '';
                                            } elseif ($_POST) {
                                                echo $situacao == CONDPGTO_ATIVO ? 'checked' : '';
                                            }
                                            ?>> Condição de pagamento ativo?
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        </form>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="editarCondpgto">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="editarCondpgto">Cancelar </button>

        </div>
    </div>
</div>

<?php
rodape(array(
    'js' => array(
        'js/condicaopgto/condicaopgto.js',
    )
));
?>
