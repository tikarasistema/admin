<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$compra = array();
$idcompra = '';
$idfornecedor = '';
$dataCompra = '';
$situacao = '';
$condpgto = '';
$nome = '';
$con = MyPdo::connect();
$_SESSION['erro'] = false;
$condicaopgto = array();



if (isset($_GET['idcompra'])) {
//    if (!isset($_SERVER['HTTP_REFERER'])) {
//        $_SERVER['HTTP_REFERER'] = '';
//    }
//
//    $uri = explode('?', $_SERVER['HTTP_REFERER']);
//    $request = explode('/', ltrim($uri[0], '/'));
//    $paginaAnterior = end($request);
//
//    if ($paginaAnterior != "clientes.php") {
//
//        editarErro(array(
//            'page' => 'index.php',
//            'tipo' => 'Compra não pode ser inicializada, cliente inválido'
//        ));
//    }
    $idcompra = getGet('idcompra');
    $stmt = $con->prepare("Select tbfornecedor.idfornecedor, nome_fantasia, nome_razao, cpf_cnpj, email, celular, telefone, data_compra, tbcompra.situacao from tbfornecedor inner join tbcompra"
            . " on tbfornecedor.idfornecedor = tbcompra.idfornecedor where idcompra = :idcompra and tbcompra.situacao = " . COMPRA_ABERTA);
    $stmt->bindValue(":idcompra", $idcompra);
    $stmt->execute();
    $compra = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$compra) {
        editarErro(array(
            'page' => 'fornecedores.php',
            'tipo' => 'Compra não pode ser inicializada, compra inválida'
        ));
    } else {
        $dataCompra = date(('d/m/Y H:i:s'), strtotime($compra['data_compra']));
        $situacao = $compra['situacao'];
        $nome = $compra['nome_fantasia'];
        $cpfcnpj = $compra['cpf_cnpj'];
        $email = $compra['email'];
        $celular = $compra['celular'];
        $telefone = $compra['telefone'];
    }
} else {
    editarErro(array(
        'page' => 'index.php',
        'tipo' => 'Compra não pode ser inicializada, fornecedor inválido'
    ));
}
?>
<?php
topo(array(
    'css' => array(
        'css/compra/compra.css',
    ),
    'pageName' => ' Nova compra',
    'icon' => 'fa fa-cart-plus',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Nova compra</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label>Data compra: <?php echo $dataCompra; ?></span></label>

                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label>Fornecedor-Nome fantasia: <?php echo $nome; ?> </label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <label> Situação: <?php echo $situacao == COMPRA_ABERTA ? "Compra aberta" : ''; ?></label>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <?php
                        $sql = "Select stnome from tbusuario where idusuario = (select idusuario from tbcompra where idcompra = :idcompra)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':idcompra', $idcompra);
                        $stmt->execute();
                        $nomeComprador = $stmt->fetch(PDO::FETCH_COLUMN);
                        ?>
                        <label> Comprador: <?php echo $nomeComprador; ?></label>
                    </div>

                </div><br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Dados Fornecedor</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <label>Fornecedor: <?php echo $nome; ?> </label><br>
                            <label><?php echo $cpfcnpj ? 'Cpf/Cnpj: ' . $cpfcnpj : ''; ?> </label><br>
                            <label><?php echo $celular ? 'Celular: ' . $celular : ''; ?> </label><br>
                            <label><?php echo $telefone ? 'Telefone: ' . $telefone : ''; ?> </label><br>
                            <label><?php echo $email ? 'Email: ' . $email : ''; ?> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <form id="pesquisarProduto" >
                <div class="panel-heading">
                    <h1 class="panel-title">Pesquisar Produtos</h1>
                </div>
                <div class="panel-body">
                    <div id="modalErro"></div>
                    <div id="modalRemoverProd"></div>
                    <div id="modalCancelar"></div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="pesquisa">Método de pesquisa* </label>
<!--                                    <select class="form-control" id="pesquisa" name="pesquisa">
                                        <option value="1">Código de barra</option>
                                        <option value="2">Descrição</option>
                                    </select>-->
                                    <div class="radio">
                                        <label><input checked type="radio" class="metodo" value="1" name="optradio">Código de barra</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" class="metodo" value="2" name="optradio">Descrição</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="produto">Código de barra / Descrição*</label>
                                    <input type="text" class="form-control" id="produto" name="produto" placeholder="Digite código de barra ou descrição do produto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button id="btPesquisaProdutos" type="submit" class="btn btn-primary">Pesquisar</button>
                    <a title="Cadastrar produto" href="http://estagio.dev/admin/produtos-cadastrar.php" target="_blank" class="btn btn-success" style="float: right;">Cadastrar produto</a>
                </div>
            </form>
        </div>
        <div id="produtos" class="esconder">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos encontrados</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descricao</th>
                                <th>Valor compra</th>
                                <th>Quantidade</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="produtosListar">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        $sql = "Select i.*, p.produto from tbitem_compra as i inner join tbproduto as p on i.idproduto = p.idproduto where idcompra = :idcompra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idcompra', $idcompra);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $esconder = "class='esconder'";
        ?>
        <div id="produtosCompra" <?php echo $itens ? '' : $esconder; ?>>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos compra</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>ID Compra</th>
                                <th>ID Produto</th>
                                <th>Descrição</th>
                                <th>Quantidade</th>
                                <th>Valor Compra</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="produtosCompraListar">
                            <?php foreach ($itens as $item) { ?>
                                <tr class="linhaProdCompra" data-idcompra="<?php echo $item['idcompra']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>">
                                    <td class="idcompra"><?php echo $item['idcompra']; ?></td>
                                    <td class="idproduto"><?php echo $item['idproduto']; ?></td>
                                    <td class="idproduto"><?php echo $item['produto']; ?></td>
                                    <td class="qtde_item"><?php echo $item['qtde_item']; ?></td>
                                    <td class="preco_item"><?php echo $item['preco_item']; ?></td>
                                    <td><a href="#" title="Remover produto" class="removerProduto" data-idcompra="<?php echo $item['idcompra']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>" data-descricao="<?php echo $item['produto']; ?>"><i class="fa fa-trash-o"></i></a></td>
                                    <td class="subTotalProdValor"><input type='hidden' value="<?php echo $item['preco_item'] * $item['qtde_item']; ?>" class='subTotalProd'></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="condPgto" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Condição de pagamento</h1>
                </div>
                <div class="panel-body">
                    <form id="condicaoPgto">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalCompra">Sub-total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="totalcompra"  value="0.00" name="totalcompra">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalCompra">Desconto</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="desconto" value="0.00" name="desconto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalCompra">Total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="subtotal" value="0.00" name="desconto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="pesquisa">Condição pagamento* </label>
                                        <select class="form-control" id="condicaoPagamento" name="pesquisa" required min="1">
                                            <option data-qtdparcela="-1" value="0">Selecione condição pagamento</option>
                                            <?php
                                            $sql = "Select * from tbcondpgto where situacao != " . CONDPGTO_INATIVO;
                                            $stmt = $con->query($sql);
                                            $condicoesPgto = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                            foreach ($condicoesPgto as $condicaopgto) {
                                                ?>
                                                <option data-qtdparcela="<?php echo $condicaopgto['qtde_parcela']; ?>" data-entrada="<?php echo $condicaopgto['entrada']; ?>" value="<?php echo $condicaopgto['idcondpgto']; ?>"><?php echo $condicaopgto['ds_condicao']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row esconder" id="entrada" >
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalCompra">Entrada*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input id="valorEntrada" type="text" class="form-control" value="0.00" name="entrada">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="cartao" value="1">Cartão</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="panel-footer">
                        <button id="finalizarCompra" type="submit" class="btn btn-primary">Fechar Compra</button>
                        <button id="cancelarCompra" type="submit" class="btn btn-danger"style="margin-left: 5%;">Cancelar Compra</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $diacompra = explode(' ', $compra['data_compra']);
    $diacompra = $diacompra[0];
    ?>
    <script>
        var idcompra = "<?php echo $idcompra; ?>";
        var datacompra = "<?php echo $diacompra; ?>";
        var idfornecedor = "<?php echo $compra['idfornecedor']; ?>";
    </script>

    <?php
    rodape(array(
        'js' => array(
            'js/compra/compra.js',
        )
    ));
    ?>





