$(function() {
    $("#paginas").on("change", function() {
        window.location.href = 'classificacoes.php?p=' + $("#paginas").val() + '&q=' + query;
    });
    var id;

    $(".situacao").off('click').on('click', function(e) {
        e.preventDefault();
        id = ($(this).data('id'));
    });

    $("#btnAlterarStatus").on("click", function() {
        $.ajax({
            type: "POST",
            url: "./api/classificacao-status.php",
            data: {classificacao: id},
            dataType: "json",
            success: function(json) {
                if (json.erro) {
                    showErrorMsg();
                } else {
                    alteraStatus();
                }
            }
        });
    });

    function alteraStatus() {
        if ($(".situacao[data-id='" + id + "']").hasClass("btn-success")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-success");
            $(".situacao[data-id='" + id + "']").addClass("btn-danger");
            $(".situacao[data-id='" + id + "']").html('inativo');
        } else if ($(".situacao[data-id='" + id + "']").hasClass("btn-danger")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-danger");
            $(".situacao[data-id='" + id + "']").addClass("btn-success");
            $(".situacao[data-id='" + id + "']").html('ativo');
        }
    }
    function showErrorMsg() {
        var html = '';
        html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Erro ao alterar Status</div>';
        $(".erroStatus").html(html);
        $(".alert").addClass("alert-danger");
    }
    var tipomarca;
    var idbuscamarca;

    $(".btnTipoMarca").off('click').on("click", function(e) {
        idbuscamarca = ($(this).data('idbuscamarca'));
        $("#marcaSelect").html('<option>Aguarde...</option>');
        $("#btnSalvarRelacionamento").attr('disabled', 'disabled');
        classificacao(idbuscamarca, popularOptions);
    });
    function mostrarSelecionados() {

        popularOptionsMostrarMarcas();

        $(".marcasSelecionadas").on('click', function() {
            popularOptionsMostrarMarcas();
        });
    }
    function popularOptionsMostrarMarcas() {
        var html = '';
        var marcasSalvar = $('input[type="checkbox"]:checked');
        if (marcasSalvar.length < 1) {
            html = '<a class="list-group-item"> Nenhuma marca selecionada</a>';
        }
        marcasSalvar.each(function() {
            html += '<a class="list-group-item">' + $(this).parent().text() + ' <i class="fa fa-check" aria-hidden="true"></i></a>';
        });
        $("#opcoesSelecionadas").html(html);
    }

    function popularOptions() {
        $.ajax({
            type: "POST",
            url: "./api/marcas.php",
            dataType: "json",
            success: function(marcas) {
                if (marcas.length < 1) {
                    $("#marcaSelect").html('<option>Sem itens para listagem</option>');
                }
                var html = ' ';
                var checked = '';
                for (var i = 0; i < marcas.length; i++) {
                    if (tipomarca.length > 0) {
                        checked = '';
                        for (var x = 0; x < tipomarca.length; x++) {
                            if (tipomarca[x].idmarca == marcas[i].idmarca) {
                                checked = 'checked';
                            }
                        }
                    }
                    html += "<div class='checkbox'> <label><input class='marcasSelecionadas' " + checked + " name='opcoes[]'type='checkbox' value=" + marcas[i].idmarca + " >" + marcas[i].dsmarca + "</label></div>";
//                html += '<option value="' + marcas[i].idmarca + '" ' + selected + '>' + marcas[i].dsmarca + '</option>';
//                $("#marcaSelect").html(html);
                    $("#btnSalvarRelacionamento").removeAttr('disabled');
                    $("#opcoes").html(html);
                    mostrarSelecionados();
                }
            }
        });
    }
    function classificacao(tipo, callback) {
        $.ajax({
            type: "POST",
            url: "./api/tipomarca.php",
            data: {tipo: tipo},
            dataType: "json",
            success: function(marcaativa) {
                tipomarca = marcaativa;
                callback();
            }
        });
    }

    $('#btnSalvarRelacionamento').on('click', function() {
        var marcasSalvar = $('input[type="checkbox"]:checked');

        var cdsClassificacao = [];

        marcasSalvar.each(function() {
            cdsClassificacao.push($(this).val());
        })
        $(".modal-content .mensagem").html('<img style="margin-left: 270px;"src="img/loading.gif" alt="Loading" height="70" width="70">');
        $.ajax({
            type: "POST",
            url: "./api/tipomarca-salvar.php",
            data: {marcas: cdsClassificacao, classificacao: idbuscamarca},
            dataType: "json",
            success: function(retorno) {
                if (retorno.sucesso) {
                    saveSuccess(retorno.sucesso);
                }

            }
        });
    });



    function saveSuccess(msg) {
        var html = '';
        html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>' + msg + '</li></ul></div>';
        $(".modal-content .mensagem").html(html);
        $(".alert").addClass("alert-success");
        $(".mensagem").removeClass('esconderModal');
    }

    $("#fecharModal").on('click', function() {
        $(".mensagem").addClass('esconderModal');
    });

});