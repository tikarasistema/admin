$(function() {
    $('#vlLancamento').maskMoney({thousands: '', decimal: '.'});

    var id;

    $(".lancamemtoExcluir").off('click').on('click', function(e) {
        e.preventDefault();
        id = ($(this).data('id'));
        abrirModal();
        console.log(id);
    });

    function abrirModal() {
        $("#modalExcluirLancamento").html('<div class="modal fade" tabindex="-1" id="modalExcluirLancamentoAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Exclusão de lançamento</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja excluir lançamento #' + id + ' ? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="excluirLancamento">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button></div></div></div></div>');
        $('#modalExcluirLancamentoAbrir').modal('show');
        $("#excluirLancamento").on("click", function() {
            $(".mensagem").html('<img style="margin-left: 240px;"src="img/loading.gif" alt="Loading" height="70" width="70">');
            $.ajax({
                type: "POST",
                url: "./api/excluirLancamento.php",
                data: {id: id},
                dataType: "json",
                success: function(retorno) {
                    console.log(retorno);
                    if (retorno.erro) {
                        msgLancamento(retorno.erro, 'danger');
                    }
                    if (retorno.errousuario) {
                        msgLancamento(retorno.errousuario, 'danger');
                    }
                    if (retorno.success) {
                        msgLancamento(retorno.success, 'success');
                    }
                }
            });
        });



        function msgLancamento(msg, tipo) {
            $(".mensagem").html('<div class="alert  alert-dismissable alert-' + tipo + '" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>' + msg + '</li></ul></div>');
            if (tipo == 'success') {
                $("#modalExcluirLancamentoAbrir").on('hidden.bs.modal', function() {
                    $("tr[data-id=" + id + "]").children('td, th')
                            .animate({padding: 0})
                            .wrapInner('<div />')
                            .children()
                            .slideUp("slow", function() {
                                $(this).closest('tr').remove();
                            });
                });

            }
        }
    }


})
