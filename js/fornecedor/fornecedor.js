$(function () {
    carregaCidades($("#estado").val());
    /**
     * Numero celular com 9 digitos mais DDD
     *
     *
     */
    var celular9digitos = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    $('#fcelular').mask(celular9digitos);
    $("#festado").on("change", function () {
        $("#fcidade").html('<option> Aguarde...</option>');
        carregaCidades($("#estado").val());
    });
    $("#paginas").on("change", function () {
        window.location.href = 'fornecedores.php?p=' + $("#paginas").val() + '&q=' + query;
    });
    var id;
    $(".situacao").off('click').on('click', function (e) {
        e.preventDefault();
        id = ($(this).data('id'));
    });
    $("#btnAlterarStatus").on("click", function () {
        $.ajax({
            type: "POST",
            url: "./api/fornecedor-status.php",
            data: {fornecedor: id},
            dataType: "json",
            success: function (json) {
                console.log(json);
                if (json.erro) {
                    showErrorMsg();
                } else {
                    alteraStatus();
                }
            }
        });
    });
    function alteraStatus() {
        if ($(".situacao[data-id='" + id + "']").hasClass("btn-success")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-success");
            $(".situacao[data-id='" + id + "']").addClass("btn-danger");
            $(".situacao[data-id='" + id + "']").html('inativo');
        } else if ($(".situacao[data-id='" + id + "']").hasClass("btn-danger")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-danger");
            $(".situacao[data-id='" + id + "']").addClass("btn-success");
            $(".situacao[data-id='" + id + "']").html('ativo');
        }
    }
    function showErrorMsg() {
        var html = '';
        html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Erro ao alterar Status</div>';
        $(".erroStatus").html(html);
        $(".alert").addClass("alert-danger");
    }


    function carregaCidades(estado) {
        data = {estado: $("#festado").val()};
        $.ajax({
            type: "POST",
            url: "./api/carrega-cidades.php",
            data: data,
            dataType: "json",
            success: function (json) {
                console.log(cidade)
                var options = "";
                var selected = "";
                $.each(json, function (key, value) {
                    if (cidade == value.id) {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                    options += '<option ' + selected + ' value="' + value.id + '">' + value.nome + '</option>';
                });
                if (json.length == 0) {
                    options += '<option  value="0">--Selecione o Estado--</option>';
                }
                $("#fcidade").html(options);
            }
        });
    }
    var idFornecedor;
    $(".compra").on("click", function (e) {
        e.preventDefault();
        var nomeFornecedor;
        idFornecedor = ($(this).data('id'));
        nomeFornecedor = ($(this).data('nome'));
        $("#modalCompra").html('<div class="modal fade" tabindex="-1" id="modalCompraAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Nova compra</h4></div><div class="modal-body"><div id="loading"></div><div id="erro"></div><h5>Deseja abrir nova compra para fornecedor <strong>' + nomeFornecedor + '</strong> ? </h5></div><div class="modal-footer"><a href="#" type="button" class="btn btn-primary" id="abrirCompra">Confirmar</a><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button></div></div></div></div>');
        $('#modalCompraAbrir').modal('show');
        $(document).off("click", "#abrirCompra").on("click", "#abrirCompra", function (e) {
            e.preventDefault();
            $("#loading").html('<img src="img/loading.gif" style="margin-left: 230px;">');
            $("#abrirCompra").attr('disabled', 'disabled');
            $.ajax({
                type: "POST",
                url: "./api/nova_compra.php",
                data: {idfornecedor: idFornecedor},
                dataType: "json",
                success: function (retorno) {
                    console.log(retorno);
                    if (retorno.sucesso == 1) {
                        window.location = URL + "/compra-aberta.php?idcompra=" + retorno.idcompra;
                    }
                    if (retorno.erro == 1) {
                        $("#loading").html('');
                        $("#erro").html('<div class="alert alert-danger alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>' + retorno.erroMsg + '</li></ul></div>');
                    }
                }
            });
        })
    })



});