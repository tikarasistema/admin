$(function() {
    if ($("#classificacao").val() != 0) {
        carregaMarcas($("#classificacao").val(), $("#cdProduto").val());
    }
    $("#classificacao").on("change", function() {
        $("#marca").html('<option> Aguarde...</option>');
        carregaMarcas($("#classificacao").val(), $("#cdProduto").val());
    });
    $("#gerarcdbarra").on("click", function() {
        if ($('#gerarcdbarra:checked').length > 0) {
            var d = new Date();
            var n = d.getTime();
            $("#fcodigobarra").val(n);
        } else {
            $("#fcodigobarra").val('');
        }

    })
    var estoque = function(val) {
        return val.replace(/\D/g, '').length === 5 ? '000000' : '000009';
    };
    $('#festoque').mask(estoque);




    $("#paginas").on("change", function() {
        window.location.href = 'produtos.php?p=' + $("#paginas").val() + '&q=' + query;
    });
    var id;

    $(".situacao").off('click').on('click', function(e) {
        e.preventDefault();
        id = ($(this).data('id'));
    });

    $("#btnAlterarStatus").on("click", function() {
        $.ajax({
            type: "POST",
            url: "./api/produto-status.php",
            data: {produto: id},
            dataType: "json",
            success: function(json) {
                console.log(json);
                if (json.erro) {
                    showErrorMsg();
                } else {
                    alteraStatus();
                }
            }
        });
    });

    function carregaMarcas(tipo, produto) {
        var data = {classificacao: tipo};
        if (produto != undefined) {
            data.produto = produto;
        }
        $.ajax({
            type: "POST",
            url: "./api/carrega-marcas.php",
            data: data,
            dataType: "json",
            success: function(json) {
                console.log(json);
                var options = "";
                var selected = '';
                $("#marca").html('<option value="0">Marca</option>');

                $.each(json, function(key, value) {
                    // console.log(value);
                    if (marca == value.idmarca) {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                    options += '<option ' + selected + ' value="' + value.idmarca + '">' + value.dsmarca + '</option>';
                });
                $("#marca").append(options);
            }
        });
    }
    function alteraStatus() {
        if ($(".situacao[data-id='" + id + "']").hasClass("btn-success")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-success");
            $(".situacao[data-id='" + id + "']").addClass("btn-danger");
            $(".situacao[data-id='" + id + "']").html('inativo');
        } else if ($(".situacao[data-id='" + id + "']").hasClass("btn-danger")) {
            $(".situacao[data-id='" + id + "']").removeClass("btn-danger");
            $(".situacao[data-id='" + id + "']").addClass("btn-success");
            $(".situacao[data-id='" + id + "']").html('ativo');
        }
    }
    function showErrorMsg() {
        var html = '';
        html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Erro ao alterar Status</div>';
        $(".erroStatus").html(html);
        $(".alert").addClass("alert-danger");
    }

    $("#lucro").on('keyup', function() {
        var precocompra = $("#precocompra").val();
        var lucro = $("#lucro").val();
        $("#precovenda").val((precocompra * lucro / 100) + parseFloat(precocompra));
        if ($("#lucro").val() < 0) {
            $("#lucro").val('');
        }
    });

    $("#precovenda").on('keyup', function() {
        if ($("#precovenda").val() < 0) {
            $("#precovenda").val('');
        }
        var precocompra = $("#precocompra").val();
        var precovenda = $("#precovenda").val();
        var diferenca = precovenda - precocompra;
        $("#lucro").val((100 * diferenca) / precocompra);
    });

    $("#precocompra").on('keyup', function() {
        if ($("#precocompra").val() < 0) {
            $("#precocompra").val('');
        }
    });
    $("#estoque").on('keyup', function() {
        if ($("#estoque").val() < 0) {
            $("#estoque").val('');
        }
    });
});