$(function () {
    $("#totalcompra").maskMoney({thousands: '', decimal: '.'});
    $("#desconto").maskMoney({thousands: '', decimal: '.'});
    $("#subtotal").maskMoney({thousands: '', decimal: '.'});
    var produto;
    var metodo;
    var foco = 0;
    var mensagem;
    var total = 0;
    var idproduto;
    var qtde_item;
    var preco_item;
    var erroParcelas;
    somarTotal();
    $("#pesquisarProduto").submit(function (e) {
        e.preventDefault();
        produto = $("#produto").val();
        metodo = $('input[name=optradio]:checked', '#pesquisarProduto').val();
        if (produto.length < 1) {
            mensagem = "Campo descrição esta vazio, digite algo para efetuar a pesquisa";
            erroModal(mensagem);
            return;
        }
        $("#btPesquisaProdutos").html('Aguarde <i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
        $.ajax({
            type: "POST",
            url: "./api/busca-produtos.php",
            data: {produto: produto, metodo: metodo},
            dataType: "json",
            success: function (retorno) {
                $("#btPesquisaProdutos").html("Pesquisar");
                console.log(retorno);
                if (retorno.sucesso == 1) {
                    listarProdutos(retorno.produtos);
                }
            }
        });
    })
    //PAGINAÇÃO VENDA
    $("#paginas").on("change", function () {
        window.location.href = 'compras.php?p=' + $("#paginas").val() + '&q=' + query + '&situacao=' + situacao;
    });
    function listarProdutos(produtos) {
        var html = '';
        if (produtos.length > 0) {
            for (var i = 0; i < produtos.length; i++) {
                html += '<tr data-idproduto="' + produtos[i].idproduto + '"><td>' + produtos[i].idproduto + '</td><td>' + produtos[i].produto + '</td><td><input data-idproduto="' + produtos[i].idproduto + '" class="valor-compra" type="text" value="' + produtos[i].precocompra + '"></td><td><input class="qtd-compra" min="1" data-idproduto="' + produtos[i].idproduto + '"type="number"></td><td><button data-idproduto="' + produtos[i].idproduto + '" type="button" title="Adicionar produto a compra" class="btAdd btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button></td></tr>';
            }
        } else {
            html = 'Sem produtos para listagem';
        }
        $("#produtosListar").html(html);
        html = '';
        $("#produtos").slideDown("slow");
        $(".valor-compra").maskMoney({thousands: '', decimal: '.'});
        if (foco < 1) {
            focarDiv("#produtos");
        }
        foco = 1;
        $(".btAdd").off('click').on('click', function () {
            idproduto = ($(this).data('idproduto'));
            qtde_item = $(".qtd-compra[data-idproduto=" + idproduto + "]").val();
            preco_item = $(".valor-compra[data-idproduto=" + idproduto + "]").val();
            if (qtde_item.length < 1 || preco_item.length < 1) {
                erroModal("Campo quantidade ou valor compra estão vazios");
                return;
            }
            if (qtde_item < 1 || preco_item < 1) {
                erroModal("Campo quantidade ou valor compra inválidos");
                return;
            }
            adcionarProdCompra(idproduto, qtde_item, preco_item);
        });
    }
    $(".removerProduto").off('click').on('click', function (e) {
        e.preventDefault();
        var idProdRemover = $(this).data('idproduto');
        var idCompraRemover = $(this).data('idcompra');
        var descricao = $(this).data('descricao');
        $("#modalRemoverProd").html(modalRemoverProduto(idProdRemover, descricao, idCompraRemover));
        $('#modalRemoverProdAbrir').modal('show');
        var idprodremover = $(this).data('idproduto');
        var idcompraremover = $(this).data('idcompra');
        $(".remover").on("click", function () {
            $("button[data-removerprod='" + idprodremover + '' + idcompraremover + "']").append('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
            $.ajax({
                type: "POST",
                url: "./api/remover-prod-compra.php",
                data: {idproduto: idprodremover, idcompra: idcompraremover},
                dataType: "json",
                success: function (retorno) {
                    if (retorno.sucesso == 1) {
                        $("#modalRemoverProdAbrir").modal('hide');
                        removerLista(idprodremover);
                    }
                }
            });
        });
    });
    function modalRemoverProduto(idProdRemover, descricao, idCompraRemover) {
        var retorno = '<div class="modal fade" tabindex="-1" id="modalRemoverProdAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"> Remover Produto</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Remover produto #' + idProdRemover + ' - ' + descricao + ' ? </h5></div><div class="modal-footer"><button data-removerProd="' + idProdRemover + '' + idCompraRemover + '" data-idcompra="' + idCompraRemover + '" data-idproduto="' + idProdRemover + '" type="button" class="btn btn-primary remover" >Remover</button><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button></div></div></div></div>'
        return retorno;
    }
    function removerLista(idprod) {
        $("tr.linhaProdCompra[data-idproduto=" + idprod + "]").children('td, th')
                .animate({padding: 0})
                .wrapInner('<div />')
                .children()
                .slideUp("slow", function () {
                    $(this).closest('tr').remove();
                    somarTotal();
                });
//        var linha = $("tr.linhaProdCompra[data-idproduto=" + idprod + "]").children('.subTotalProdValor');
//        console.log(linha);
//        var valorRemover = linha.children('.subTotalProd').val();
//        total = total - valorRemover;
    }
    function focarDiv(id) {
        $('html,body').animate({scrollTop: $(id).offset().top}, 1000);
    }

    function adcionarProdCompra(idproduto, qtde_item, preco_item) {
        $("button[data-idproduto='" + idproduto + "']").html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
        $.ajax({
            type: "POST",
            url: "./api/salva-item-compra.php",
            data: {idproduto: idproduto, idcompra: idcompra, qtde_item: qtde_item, preco_item: preco_item},
            dataType: "json",
            success: function (retorno) {
                $(".removerbutton[data-idproduto='" + idproduto + "']").html('<i class="fa fa-plus"></i>');
                console.log(retorno);
                if (retorno.sucesso == 1) {
                    $("button[data-idproduto='" + idproduto + "']").html('<i class="fa fa-plus" aria-hidden="true"></i>');
                    listarProdutosSalvos(retorno.produto, retorno.tipo);
                }
            }
        });
    }
    function listarProdutosSalvos(produto, tipo) {
        var html = '';
        if (produto.idcompra > 0) {
            html = "<tr class='linhaProdCompra' data-idcompra='" + produto.idcompra + "' data-idproduto='" + produto.idproduto + "'><td class='idcompra'>" + produto.idcompra + "</td><td class='idproduto'>" + produto.idproduto + "</td><td>" + produto.descricao + "</td><td class='qtde_item'>" + produto.qtde_item + "</td><td class='preco_item'>" + produto.preco_item + "</td><td><a href='#' title='Remover produto' class='removerProduto' data-descricao='" + produto.descricao + "' data-idcompra='" + produto.idcompra + "' data-idproduto='" + produto.idproduto + "'><i class='fa fa-trash-o'></i></a></td><td class='subTotalProdValor'><input type='hidden' value='" + produto.preco_item * produto.qtde_item + "' class='subTotalProd'></td></tr>";
        } else {
            html = 'Sem produtos para listagem';
        }

        if (tipo == 1) {
            $("#produtosCompra").removeClass('esconder');
            $("#produtosCompraListar").prepend(html);
            $(".linhaProdCompra").removeClass("addCompra");
            $(".linhaProdCompra[data-idproduto=" + produto.idproduto + "]").addClass("addCompra");
        } else if (tipo == 2) {
//            console.log(produto)
//            console.log($(".linhaProdCompra").data("idproduto"))
//            console.log($(".linhaProdCompra").data("idvenda"))

            $(".linhaProdCompra[data-idproduto=" + produto.idproduto + "]").remove();
            $("#produtosCompraListar").prepend(html);
            $(".linhaProdCompra").removeClass("addCompra");
            $(".linhaProdCompra[data-idproduto=" + produto.idproduto + "]").addClass("addCompra");
//            if (($(".linhaProdCompra").data("idproduto") == produto.idproduto) && ($(".linhaProdCompra").data("idvenda") == produto.idvenda)) {
//            alert('oi')
//                $(".linhaProdCompra[data-idvenda='" + produto.idvenda + "'][data-idproduto='" + produto.idproduto + "']").remove();
//                $("#produtosCompraListar").prepend(html);
//            }
        }
        $(".removerProduto").off('click').on('click', function (e) {
            e.preventDefault();
            var idProdRemover = $(this).data('idproduto');
            var idCompraRemover = $(this).data('idcompra');
            var descricao = $(this).data('descricao');
            $("#modalRemoverProd").html(modalRemoverProduto(idProdRemover, descricao, idCompraRemover));
            $('#modalRemoverProdAbrir').modal('show');
            var idprodremover = $(this).data('idproduto');
            var idcompraremover = $(this).data('idcompra');
            $(".remover").on("click", function () {
                $("button[data-removerprod='" + idprodremover + '' + idcompraremover + "']").append('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
                $.ajax({
                    type: "POST",
                    url: "./api/remover-prod-compra.php",
                    data: {idproduto: idprodremover, idcompra: idcompraremover},
                    dataType: "json",
                    success: function (retorno) {
                        if (retorno.sucesso == 1) {
                            $("#modalRemoverProdAbrir").modal('hide');
                            removerLista(idprodremover);
                        }
                    }
                });
            });
        });
        total = 0;
        somarTotal();
    }

    function somarTotal() {
        total = 0;
        $(".subTotalProd").each(function () {
            total = parseFloat($(this).val()) + total;
            console.log(total);
        });
        $("#totalcompra").val(total.toFixed(2));
        var totalCompra = parseFloat($("#totalcompra").val());
        var totalDesconto = parseFloat($("#desconto").val());
        $("#subtotal").val((totalCompra - totalDesconto).toFixed(2));
    }

    function erroModal(msg) {
        $("#modalErro").html('<div class="modal fade" tabindex="-1" id="modalErroAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Erro</h4></div><div class="modal-body"><div class="mensagem"></div><h5>' + msg + ' </h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalErroAbrir').modal('show');
    }


    $("#condicaoPagamento").on("change", function () {
        var selected = $(this).find('option:selected');
        var entradaSelected = selected.data('entrada');
        if (entradaSelected) {
            $("#entrada").removeClass('esconder');
            $("#valorEntrada").maskMoney({thousands: '', decimal: '.'});
        } else {
            $("#entrada").addClass('esconder');
        }

    });
    var desconto = $("#desconto").val() > 0 ? $("#desconto").val() : 0;
    total = $("#totalcompra").val();
    total = parseFloat(total - desconto);
    $("#subtotal").val(total.toFixed(2));
    $("#desconto").on('keyup', function () {
        var desconto = $("#desconto").val() > 0 ? $("#desconto").val() : 0;
        var total = $("#totalcompra").val();
        total = parseFloat(total - desconto);
        $("#subtotal").val(total.toFixed(2));
    });
    $("#subtotal").on('keyup', function () {
        var total = $("#totalcompra").val();
        var subtotal = $("#subtotal").val();
        console.log(subtotal);
        var desconto = parseFloat(total - subtotal);
        $("#desconto").val(desconto.toFixed(2));
    });
    $("#finalizarCompra").on("click", function () {
        $("#modalCancelar").html('<div class="modal fade" tabindex="-1" id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Finalizar compra</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente finalizar compra? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="finalizarCompraBtn" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalCancelarAbrir').modal('show');

        $("#finalizarCompraBtn").on("click", function () {
            $("#finalizarCompra").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            gerarParcelas();
        });
    });
    $("#cancelarCompra").on("click", function () {
        $("#modalCancelar").html('<div class="modal fade" tabindex="-1" id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Finalizar compra</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente finalizar compra? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="cancelarCompraBtn" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalCancelarAbrir').modal('show');

        $("#cancelarCompraBtn").on("click", function () {
            $("#cancelarCompra").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            $.ajax({
                type: "POST",
                url: "./api/cancelar-compra.php",
                data: {idcompra: idcompra},
                dataType: "json",
                success: function (retorno) {
                    $("#modalCancelar").html('<div class="modal fade"  id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Finalizar compra</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente finalizar compra? </h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" id="fecharModalCancelarB" data-dismiss="modal">Fechar</button></div></div></div></div>');
                    $('#modalCancelarAbrir').modal('show');

                    $("#fecharModalCancelarB").on("click", function () {
                        window.location = URL + '/compras.php';
                    });
                }
            });
        });
    });

    function gerarParcelas() {
        var desconto = $("#desconto").val();
        var totalCompra = $("#totalcompra").val();
        var total = totalCompra - desconto;
        var selected = $('#condicaoPagamento').find('option:selected');
        var entradaSelected = selected.data('entrada');
        var qtdParcelas = selected.data('qtdparcela');
        var condicaopgto = $('#condicaoPagamento').val();
        var cartao = $('#cartao').is(":checked") == 1 ? 1 : 0;
        var erro = 0;
        var totalValidar = $("#subtotal").val();
        if (entradaSelected == 1) {
            var entrada = $("#valorEntrada").val();
            total = total - entrada;
        } else {
            entrada = 0;
        }
        if (entradaSelected == 1) {
            qtdParcelas = qtdParcelas - 1;
        }
        if (qtdParcelas < 0) {
            $("#finalizarCompra").html('Fechar compra');
            erroMsg('Quantidade de parcelas inválida!');
            erro = 1;
            return;
        }
        if (totalCompra > 0) {

        } else {
            $("#finalizarCompra").html('Fechar compra');
            erroMsg('Total compra deve ser maior que 0!');
            erro = 1;
            return;
        }
        if (desconto < 0) {
            $("#finalizarCompra").html('Fechar compra');
            erroMsg('Desconto Inválido!');
            erro = 1;
            return;
        }
        if (total < 0 || total == 0) {
            $("#finalizarCompra").html('Fechar compra');
            erroMsg('Valor total Inválido!');
            erro = 1;
            return;
        }
        if (erro != 1) {
            $.ajax({
                type: "POST",
                url: "./api/salvar-parcelas-compra.php",
                data: {total: total, entrada: entrada, qtdParcelas: qtdParcelas, tementrada: entradaSelected, idcompra: idcompra, datacompra: datacompra, idfornecedor: idfornecedor, condpgto: condicaopgto, cartao: cartao, desconto: desconto, totalValidar: totalValidar},
                dataType: "json",
                success: function (retorno) {
                    $("#finalizarCompra").html('Fechar compra');
                    if (retorno.erro == 1) {
                        erroMsg(retorno.erroMsg);
                        return;
                        erroParcelas = 1;
                    }
                    if (retorno.sucesso == 1) {
                        receberEstoque();
                    }
                }
            });
        }
    }

    function receberEstoque() {
        $.ajax({
            type: "POST",
            url: "./api/receber-estoque.php",
            data: {idcompra: idcompra},
            dataType: "json",
            success: function (retorno) {
                $("#finalizarCompra").html('Fechar compra');
                if (retorno.erro == 1) {
                    erroMsg(retorno.msgErro);
                    return;
                } else {
                    $("#modalegg").html('<div class="modal fade" id="modalEasterEgg2" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Sucesso</h4></div><div class="modal-body"><div class="mensagem"><h4>Compra salva com sucesso</h4></div></div><div class="modal-footer"><a type="button" href="' + URL + '/compras.php" class="btn btn-primary" data-dismiss="modal id="sucessoCompraBt">Fechar</a></div></div></div></div>');
                    $('#modalEasterEgg2').modal('show');
                }
                // gerarParcelas();
            }
        });
    }

    function erroMsg(msg) {
        $("#modalegg").html('<div class="modal fade" id="modalEasterEgg" role="dialog" tabindex=-1><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Erro</h4></div><div class="modal-body"><div class="mensagem"><h4>' + msg + '</h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalEasterEggGif').html('<img src="js/easterEgg/img/easteregg.gif" alt="" height="300px" style="margin-left:135px;"/>');
        $('#modalEasterEgg').modal('show');
    }

});

