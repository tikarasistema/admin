$(function () {
    $("#cancelarCompra").on("click", function () {

        $("#modalCancelar").html('<div class="modal fade" tabindex="-1" id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Cancelar compra</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente cancelar compra? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="cancelarCompraBtn" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalCancelarAbrir').modal('show');

        $("#cancelarCompraBtn").on("click", function () {
            $("#cancelarCompra").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            $.ajax({
                type: "POST",
                url: "./api/cancelar-compra-fechada.php",
                data: {idcompra: idcompra},
                dataType: "json",
                success: function (retorno) {
                    if ((retorno.parcelas != '')) {
                        var parcelas = (retorno.parcelas).length;
                    }
                    var total = 0;
                    for (var i = 0; i < parcelas; i++) {
                        total += parseFloat(retorno.parcelas[i].vlr_pago);
                    }
                    var mensagemTot = total > 0 ? 'Total a ser reembolsado R$' + total : '';
                    $("#modalCancelar3").html('<div class="modal fade"  id="modalCancelarAbrir3" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Cancelar compra</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Compra cancelada ' + mensagemTot + '</h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="fecharModalCancelarB">Fechar</button></div></div></div></div>');
                    $('#modalCancelarAbrir3').modal('show');

                    $("#fecharModalCancelarB").on("click", function () {
                        window.location = URL + '/compras.php';
                    });
                }
            });
        });
    });
});