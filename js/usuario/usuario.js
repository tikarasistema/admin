$("#paginas").on("change", function() {
    window.location.href = 'usuarios.php?p=' + $("#paginas").val() + '&q=' + query;
});
var id;

$(".situacao").off('click').on('click', function(e) {
    e.preventDefault();
    id = ($(this).data('id'));
});

$("#btnAlterarStatus").on("click", function() {
    $.ajax({
        type: "POST",
        url: "./api/usuario-status.php",
        data: {usuario: id},
        dataType: "json",
        success: function(json) {
            console.log(json);
            if (json.erro) {
                showErrorMsg();
            } else {
                alteraStatus();
            }
        }
    });
});

function alteraStatus() {
    if ($(".situacao[data-id='" + id + "']").hasClass("btn-success")) {
        $(".situacao[data-id='" + id + "']").removeClass("btn-success");
        $(".situacao[data-id='" + id + "']").addClass("btn-danger");
        $(".situacao[data-id='" + id + "']").html('inativo');
    } else if ($(".situacao[data-id='" + id + "']").hasClass("btn-danger")) {
        $(".situacao[data-id='" + id + "']").removeClass("btn-danger");
        $(".situacao[data-id='" + id + "']").addClass("btn-success");
        $(".situacao[data-id='" + id + "']").html('ativo');
    }
}
function showErrorMsg() {
    var html = '';
    html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Erro ao alterar Status</div>';
    $(".erroStatus").html(html);
    $(".alert").addClass("alert-danger");
}

$("#fnumero").on('keyup', function() {
    if ($("#fnumero").val() < 0) {
        ("#fnumero").val('');
    }
})

$(function() {

    var celular9digitos = function(val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };

    $('#fcelular').mask(celular9digitos);

    $("#festado").on("change", function() {
        $("#fcidade").html('<option> Aguarde...</option>');
        carregaCidades($("#estado").val());

    });

    function carregaCidades(estado) {
        data = {estado: $("#festado").val()};
        $.ajax({
            type: "POST",
            url: "./api/carrega-cidades.php",
            data: data,
            dataType: "json",
            success: function(json) {
                var options = "";
                var selected = "";
                $.each(json, function(key, value) {
                    if (cidade == value.id) {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }
                    options += '<option ' + selected + ' value="' + value.id + '">' + value.nome + '</option>';
                });
                if (json.length == 0) {
                    options += '<option  value="0">--Selecione o Estado--</option>';
                }
                $("#fcidade").html(options);
            }
        });
    }


});