$(document).ready(function() {

    var ctx = document.getElementById("prodMaisVendido").getContext('2d');
    var BarChart = new Chart(ctx).Bar(data, options);

    var vendasMes = document.getElementById("qtdVendasMes").getContext('2d');
    var LineChart = new Chart(vendasMes).Line(desgraca, opcoes);

    // var ctx = document.getElementById("prodMaisVendido").getContext('2d');
    // var LineChart = new Chart(ctx).Line(data, options);
});
//passando configuração dos graficos
var options = {
    responsive: true
};

var data = {
    labels: this.label,
    datasets: [
        {
            label: "período 1",
            fillColor: "rgba(33, 150, 243, 0.2)",
            strokeColor: "rgba(33, 150, 243, 0.77)",
            pointColor: "rgba(33, 150, 243, 0.77)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: this.dados
        },
    ]
};

var opcoes = {
    responsive: true
};

var desgraca = {
    labels: this.dia,
    datasets: [
        {
            label: "período 1",
            fillColor: "rgba(0, 255, 0, 0.2)",
            strokeColor: "rgba(0, 255, 0, 0.59)",
            pointColor: "rgba(0, 255, 0, 0.59)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: this.vendas
        },
    ]
};