$(document).ready(function () {

    verificarCaixa();
    function verificarCaixa() {
        $.ajax({
            type: "POST",
            url: "./api/caixa/verificar-caixa.php",
            dataType: "json",
            success: function (caixa) {
                if (caixa.aberto == false) {
                    iniciarCaixa();
                    $("#vlLancamento").on('keydown', function (e) {
                        if (e.keyCode == 27) {
                            location.reload();
                        }
                    });

                } else {
                    console.log("CAIXA ABERTO");
                }
            }
        });
    }

    function iniciarCaixa() {
        $("#modalcaixa").html('<div class="modal fade" id="modalAbrirCaixa" role="dialog" data-backdrop="static"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Abertura de caixa</h4></div><div class="modal-body"><div class="mensagem"></div><div class="caixa"></div><h5 style="text-align: center;font-family: sans-serif;">Insira saldo inicial disponível em caixa</h5><div class="input-group"><span class="input-group-addon">$</span><input type="text" class="form-control" id="vlLancamento"></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="abrirCaixa">Abrir</button><button class="hidden btn btn-danger" type="button" data-dismiss="modal" id="fecharModal">Fechar</button></div></div></div></div>');
        $('#modalAbrirCaixa').modal('show');
        $('#vlLancamento').maskMoney({thousands: '', decimal: '.'});

        $("#abrirCaixa").on("click", function () {
            $(".mensagem").html('<img style="margin-left: 240px;"src="img/loading.gif" alt="Loading" height="70" width="70">');
            var saldoInicial = $("#vlLancamento").val();
            $.ajax({
                type: "POST",
                url: "./api/caixa/abrirCaixa.php",
                data: {saldoInicial: saldoInicial},
                dataType: "json",
                success: function (retorno) {
                    console.log(retorno);
                    if (retorno.erro) {
                        msgLancamento(retorno.erro, 'danger');
                    }
                    if (retorno.errousuario) {
                        msgLancamento(retorno.errousuario, 'danger');
                    }
                    if (retorno.success) {
                        msgLancamento(retorno.success, 'success');
                        $("#abrirCaixa").remove();
                        $("#fecharModal").removeClass("hidden");
                        $("#modalAbrirCaixa").on('hidden.bs.modal', function () {
                            verificarCaixa();
                        });
                    }
                }
            });
        });

        function msgLancamento(msg, tipo) {
            $(".mensagem").html('<div class="alert  alert-dismissable alert-' + tipo + '" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><ul><li>' + msg + '</li></ul></div>');
        }
    }

    var egg = new Egg("up,up,down,down,left,right,a,b,a", function () {
        $("#modalegg").html('<div class="modal fade" id="modalEasterEgg" role="dialog" tabindex=-1><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Desenvolvedores</h4></div><div class="modal-body"><div class="mensagem"></div><h2 style="text-align: center;font-family: sans-serif;">Desenvolvido por <strong>Guilherme Henrique Rodrigues</strong> e <strong>Marcelo Miyashita</strong></h2><div id="modalEasterEggGif"></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalEasterEggGif').html('<img src="js/easterEgg/img/easteregg.gif" alt="" height="300px" style="margin-left:135px;"/>');
        $('#modalEasterEgg').modal('show');
    }).listen();

    var egg = new Egg("d,c,e,m,a,r,v,e,l", function () {
        $("#modalegg").html('<div class="modal fade" id="modalEasterEgg" role="dialog" tabindex=-1><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Desenvolvedores</h4></div><div class="modal-body"><div class="mensagem"></div><h2 style="text-align: center;font-family: sans-serif;">Desenvolvido por <strong>Guilherme Henrique Rodrigues</strong> e <strong>Marcelo Miyashita</strong></h2><div id="modalEasterEggGif"></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalEasterEggGif').html('<img src="js/easterEgg/img/dcemarvel.gif" alt="" height="400px" style="margin-left:150px;"/>');
        $('#modalEasterEgg').modal('show');
    }).listen();

    $.Shortcuts.start();

    $.Shortcuts.add({
        type: 'hold',
        mask: 'Ctrl+Alt+v',
        handler: function () {
            window.location = 'vendas.php';
        },
        list: 'another'
    });
    $.Shortcuts.add({
        type: 'hold',
        mask: 'Ctrl+Alt+c',
        handler: function () {
            window.location = 'compras.php';
        },
        list: 'another'
    });
    $.Shortcuts.add({
        type: 'hold',
        mask: 'Ctrl+Alt+p',
        handler: function () {
            window.location = 'produtos.php';
        },
        list: 'another'
    });


    $.Shortcuts.start('another');

    $(".imprimir").on("click", function () {
        window.print();
    });

})


