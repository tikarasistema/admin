$(function () {
    $("#cancelarVenda").on("click", function () {

        $("#modalCancelar3").html('<div class="modal fade" tabindex="-1" id="modalCancelarAbrir3" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Cancelar venda</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente cancelar venda? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="cancelarVendaBtn" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalCancelarAbrir3').modal('show');

        $("#cancelarVendaBtn").on("click", function () {
            $("#cancelarVenda").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            $.ajax({
                type: "POST",
                url: "./api/cancelar-venda-fechada.php",
                data: {idvenda: idvenda},
                dataType: "json",
                success: function (retorno) {
                    if ((retorno.parcelas != '')) {
                        var parcelas = (retorno.parcelas).length;
                    }
                    var total = 0;
                    for (var i = 0; i < parcelas; i++) {
                        total += parseFloat(retorno.parcelas[i].vrl_recebido);
                    }
                    var mensagemTot = total > 0 ? 'Total a ser devolvido R$' + total : '';
                    $("#modalCancelar2").html('<div class="modal fade"  id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"> Venda cancelada</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Venda cancelada ' + mensagemTot + '</h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="fecharModalCancelar">Fechar</button></div></div></div></div>');
                    $('#modalCancelarAbrir').modal('show');
                    $("#fecharModalCancelar").on("click", function () {
                        window.location = URL + '/vendas.php';
                    });
                }
            });
        });
    });
});