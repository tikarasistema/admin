$(function () {
    $("#totalvenda").maskMoney({thousands: '', decimal: '.'});
    $("#desconto").maskMoney({thousands: '', decimal: '.'});
    $("#subtotal").maskMoney({thousands: '', decimal: '.'});
    var produto;
    var metodo;
    var foco = 0;
    var mensagem;
    var total = 0;
    var idproduto;
    var qtde_item;
    var preco_item;
    var erroParcelas;
    somarTotal();
    $("#pesquisarProduto").submit(function (e) {
        e.preventDefault();
        produto = $("#produto").val();
        metodo = $('input[name=optradio]:checked', '#pesquisarProduto').val();
        if (produto.length < 1) {
            mensagem = "Campo descrição esta vazio, digite algo para efetuar a pesquisa";
            erroModal(mensagem);
            return;
        }
        $("#btPesquisaProdutos").html('Aguarde <i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
        $.ajax({
            type: "POST",
            url: "./api/busca-produtos.php",
            data: {produto: produto, metodo: metodo},
            dataType: "json",
            success: function (retorno) {
                $("#btPesquisaProdutos").html("Pesquisar");
                console.log(retorno);
                if (retorno.sucesso == 1) {
                    listarProdutos(retorno.produtos);
                }
            }
        });
    })
    $("#paginas").on("change", function () {
        window.location.href = 'vendas.php?p=' + $("#paginas").val() + '&q=' + query + '&situacao=' + situacao;
    });
    function listarProdutos(produtos) {
        var html = '';
        if (produtos.length > 0) {
            for (var i = 0; i < produtos.length; i++) {
                html += '<tr data-idproduto="' + produtos[i].idproduto + '"><td>' + produtos[i].idproduto + '</td><td>' + produtos[i].produto + '</td><td>' + produtos[i].precocompra + '</td><td><input data-idproduto="' + produtos[i].idproduto + '" class="valor-venda" type="text" value="1' + produtos[i].precovenda + '"></td><td><input class="qtd-venda" min="1" data-idproduto="' + produtos[i].idproduto + '"type="number"></td><td><button data-idproduto="' + produtos[i].idproduto + '" type="button" title="Adicionar produto a venda" class="btAdd btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button></td></tr>';
            }
        } else {
            html = 'Sem produtos para listagem';
        }
        $("#produtosListar").html(html);
        html = '';
        $("#produtos").slideDown("slow");
        $(".valor-venda").maskMoney({thousands: '', decimal: '.'});
        if (foco < 1) {
            focarDiv("#produtos");
        }
        foco = 1;
        $(".btAdd").off('click').on('click', function () {
            idproduto = ($(this).data('idproduto'));
            qtde_item = $(".qtd-venda[data-idproduto=" + idproduto + "]").val();
            preco_item = $(".valor-venda[data-idproduto=" + idproduto + "]").val();
            if (qtde_item.length < 1 || preco_item.length < 1) {
                erroModal("Campo quantidade ou valor estão vazios");
                return;
            }
            if (qtde_item < 1 || preco_item < 1) {
                erroModal("Campo quantidade ou valor inválidos");
                return;
            }
            adcionarProdVenda(idproduto, qtde_item, preco_item);
        });
    }
    $(".removerProduto").off('click').on('click', function (e) {
        e.preventDefault();
        var idProdRemover = $(this).data('idproduto');
        var idVendaRemover = $(this).data('idvenda');
        var descricao = $(this).data('descricao');
        $("#modalRemoverProd").html(modalRemoverProduto(idProdRemover, descricao, idVendaRemover));
        $('#modalRemoverProdAbrir').modal('show');
        var idprodremover = $(this).data('idproduto');
        var idvendaremover = $(this).data('idvenda');
        $(".remover").on("click", function () {
            $("button[data-removerprod='" + idprodremover + '' + idvendaremover + "']").append('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
            $.ajax({
                type: "POST",
                url: "./api/remover-prod-venda.php",
                data: {idproduto: idprodremover, idvenda: idvendaremover},
                dataType: "json",
                success: function (retorno) {
                    if (retorno.sucesso == 1) {
                        $("#modalRemoverProdAbrir").modal('hide');
                        removerLista(idprodremover);
                    }
                }
            });
        });
    });
    function modalRemoverProduto(idProdRemover, descricao, idVendaRemover) {
        var retorno = '<div class="modal fade" tabindex="-1" id="modalRemoverProdAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"> Remover Produto</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Remover produto #' + idProdRemover + ' - ' + descricao + ' ? </h5></div><div class="modal-footer"><button data-removerProd="' + idProdRemover + '' + idVendaRemover + '" data-idvenda="' + idVendaRemover + '" data-idproduto="' + idProdRemover + '" type="button" class="btn btn-primary remover" >Remover</button><button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button></div></div></div></div>'
        return retorno;
    }
    function removerLista(idprod) {
        $("tr.linhaProdVenda[data-idproduto=" + idprod + "]").children('td, th')
                .animate({padding: 0})
                .wrapInner('<div />')
                .children()
                .slideUp("slow", function () {
                    $(this).closest('tr').remove();
                    somarTotal();
                });
//        var linha = $("tr.linhaProdVenda[data-idproduto=" + idprod + "]").children('.subTotalProdValor');
//        console.log(linha);
//        var valorRemover = linha.children('.subTotalProd').val();
//        total = total - valorRemover;
    }
    function focarDiv(id) {
        $('html,body').animate({scrollTop: $(id).offset().top}, 1000);
    }

    function adcionarProdVenda(idproduto, qtde_item, preco_item) {
        $("button[data-idproduto='" + idproduto + "']").html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
        $.ajax({
            type: "POST",
            url: "./api/salva-item-venda.php",
            data: {idproduto: idproduto, idvenda: idvenda, qtde_item: qtde_item, preco_item: preco_item},
            dataType: "json",
            success: function (retorno) {
                $(".removerbutton[data-idproduto='" + idproduto + "']").html('<i class="fa fa-plus"></i>');
                console.log(retorno);
                if (retorno.sucesso == 1) {
                    $("button[data-idproduto='" + idproduto + "']").html('<i class="fa fa-plus" aria-hidden="true"></i>');
                    listarProdutosSalvos(retorno.produto, retorno.tipo);
                }
                if (retorno.estoque == 1) {
                    $("#modalErro").html('<div class="modal fade" tabindex="-1" id="modalErroAbrirEstoque" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Erro</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Estoque disponível é inferior ao adicionado em venda, <strong> quantidade disponível: ' + retorno.qtdEmEstoque + '</strong> </h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
                    $('#modalErroAbrirEstoque').modal('show');
                    $(".btAdd").html('<i class="fa fa-plus" aria-hidden="true"></i>');
                    return;
                }
            }
        });
    }
    function listarProdutosSalvos(produto, tipo) {
        var html = '';
        if (produto.idvenda > 0) {
            html = "<tr class='linhaProdVenda' data-idvenda='" + produto.idvenda + "' data-idproduto='" + produto.idproduto + "'><td class='idproduto'>" + produto.idproduto + "</td><td>" + produto.descricao + "</td><td class='qtde_item'>" + produto.qtde_item + "</td><td class='preco_item'>" + produto.preco_item + "</td><td><a href='#' title='Remover produto' class='removerProduto' data-descricao='" + produto.descricao + "' data-idvenda='" + produto.idvenda + "' data-idproduto='" + produto.idproduto + "'><i class='fa fa-trash-o'></i></a></td><td class='subTotalProdValor'><input type='hidden' value='" + produto.preco_item * produto.qtde_item + "' class='subTotalProd'></td></tr>";
        } else {
            html = 'Sem produtos para listagem';
        }

        if (tipo == 1) {
            $("#produtosVenda").removeClass('esconder');
            $("#produtosVendaListar").prepend(html);
            $(".linhaProdVenda").removeClass("addVenda");
            $(".linhaProdVenda[data-idproduto=" + produto.idproduto + "]").addClass("addVenda");
        } else if (tipo == 2) {
//            console.log(produto)
//            console.log($(".linhaProdVenda").data("idproduto"))
//            console.log($(".linhaProdVenda").data("idvenda"))

            $(".linhaProdVenda[data-idproduto=" + produto.idproduto + "]").remove();
            $("#produtosVendaListar").prepend(html);
            $(".linhaProdVenda").removeClass("addVenda");
            $(".linhaProdVenda[data-idproduto=" + produto.idproduto + "]").addClass("addVenda");
//            if (($(".linhaProdVenda").data("idproduto") == produto.idproduto) && ($(".linhaProdVenda").data("idvenda") == produto.idvenda)) {
//            alert('oi')
//                $(".linhaProdVenda[data-idvenda='" + produto.idvenda + "'][data-idproduto='" + produto.idproduto + "']").remove();
//                $("#produtosVendaListar").prepend(html);
//            }
        }
        $(".removerProduto").off('click').on('click', function (e) {
            e.preventDefault();
            var idProdRemover = $(this).data('idproduto');
            var idVendaRemover = $(this).data('idvenda');
            var descricao = $(this).data('descricao');
            $("#modalRemoverProd").html(modalRemoverProduto(idProdRemover, descricao, idVendaRemover));
            $('#modalRemoverProdAbrir').modal('show');
            var idprodremover = $(this).data('idproduto');
            var idvendaremover = $(this).data('idvenda');
            $(".remover").on("click", function () {
                $("button[data-removerprod='" + idprodremover + '' + idvendaremover + "']").append('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i>');
                $.ajax({
                    type: "POST",
                    url: "./api/remover-prod-venda.php",
                    data: {idproduto: idprodremover, idvenda: idvendaremover},
                    dataType: "json",
                    success: function (retorno) {
                        if (retorno.sucesso == 1) {
                            $("#modalRemoverProdAbrir").modal('hide');
                            removerLista(idprodremover);
                        }
                    }
                });
            });
        });
        total = 0;
        somarTotal();
    }

    function somarTotal() {
        total = 0;
        $(".subTotalProd").each(function () {
            total = parseFloat($(this).val()) + total;
            console.log(total);
        });
        $("#totalvenda").val(total.toFixed(2));
        var totalVenda = parseFloat($("#totalvenda").val());
        var totalDesconto = parseFloat($("#desconto").val());
        $("#subtotal").val((totalVenda - totalDesconto).toFixed(2));
    }

    function erroModal(msg) {
        $("#modalErro").html('<div class="modal fade" tabindex="-1" id="modalErroAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Erro</h4></div><div class="modal-body"><div class="mensagem"></div><h5>' + msg + ' </h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalErroAbrir').modal('show');
    }


    $("#condicaoPagamento").on("change", function () {
        var selected = $(this).find('option:selected');
        var entradaSelected = selected.data('entrada');
        if (entradaSelected) {
            $("#entrada").removeClass('esconder');
            $("#valorEntrada").maskMoney({thousands: '', decimal: '.'});
        } else {
            $("#entrada").addClass('esconder');
        }

    });
    var desconto = $("#desconto").val() > 0 ? $("#desconto").val() : 0;
    total = $("#totalvenda").val();
    total = parseFloat(total - desconto);
    $("#subtotal").val(total.toFixed(2));
    $("#desconto").on('keyup', function () {
        var desconto = $("#desconto").val() > 0 ? $("#desconto").val() : 0;
        var total = $("#totalvenda").val();

        console.log(desconto);
        console.log(total);
        if (parseFloat(desconto) > parseFloat(total)) {
            console.log(total);
            $("#desconto").css("border-color", "red");
        } else {
            $("#desconto").css("border-color", "#cccccc");
        }
        total = parseFloat(total - desconto);


        $("#subtotal").val(total.toFixed(2));
    });
    $("#subtotal").on('keyup', function () {
        var total = $("#totalvenda").val()
        var subtotal = $("#subtotal").val();
        console.log(subtotal);
        var desconto = parseFloat(total - subtotal) > 0 ? parseFloat(total - subtotal) : 0.00;
        $("#desconto").val(desconto.toFixed(2));
    });
    $("#finalizarVenda").on("click", function () {

        $("#modalConfirm").html('<div class="modal fade" tabindex="-1" id="modalConfirmAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Finalizar venda</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente finalizar venda? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="confirmarVenda" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalConfirmAbrir').modal('show');

        $("#confirmarVenda").on("click", function () {
            $("#finalizarVenda").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            gerarParcelas();
        });
    });
    $("#cancelarVenda").on("click", function () {

        $("#modalCancelar").html('<div class="modal fade" tabindex="-1" id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><i class="fa fa-warning" aria-hidden"true"=""></i> Cancelar venda</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Deseja Realmente cancelar venda? </h5></div><div class="modal-footer"><button type="button" class="btn btn-primary" id="cancelarVendaBtn" data-dismiss="modal">Confirmar</button><button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalCancelarAbrir').modal('show');

        $("#cancelarVendaBtn").on("click", function () {

            $("#cancelarVenda").prepend('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> ');
            $.ajax({
                type: "POST",
                url: "./api/cancelar-venda.php",
                data: {idvenda: idvenda},
                dataType: "json",
                success: function (retorno) {
                    $("#modalCancelar").html('<div class="modal fade"  id="modalCancelarAbrir" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><h4 class="modal-title"> Venda cancelada</h4></div><div class="modal-body"><div class="mensagem"></div><h5>Venda cancelada </h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="fecharModalCancelar">Fechar</button></div></div></div></div>');
                    $('#modalCancelarAbrir').modal('show');
                    $("#fecharModalCancelar").on("click", function () {
                        window.location = URL + '/vendas.php';
                    });
                }
            });
        });
    });

    function gerarParcelas() {
        var desconto = $("#desconto").val();
        var totalVenda = $("#totalvenda").val();
        var total = totalVenda - desconto;
        var selected = $('#condicaoPagamento').find('option:selected');
        var entradaSelected = selected.data('entrada');
        var qtdParcelas = selected.data('qtdparcela');
        var condicaopgto = $('#condicaoPagamento').val();
        var cartao = $('#cartao').is(":checked") == 1 ? 1 : 0;
        var erro = 0;
        var totalValidar = $("#subtotal").val();
        if (entradaSelected == 1) {
            var entrada = $("#valorEntrada").val();
            total = total - entrada;
        } else {
            entrada = 0;
        }
        if (entradaSelected == 1) {
            qtdParcelas = qtdParcelas - 1;
        }
        if (qtdParcelas < 0) {
            $("#finalizarVenda").html('Fechar venda');
            erroMsg('Quantidade de parcelas inválida!');
            erro = 1;
            return;
        }
        if (totalVenda > 0) {

        } else {
            $("#finalizarVenda").html('Fechar venda');
            erroMsg('Total venda deve ser maior que 0!');
            erro = 1;
            return;
        }
        if (desconto < 0) {
            $("#finalizarVenda").html('Fechar venda');
            erroMsg('Desconto Inválido!');
            erro = 1;
            return;
        }
        if (total < 0 || total == 0) {
            $("#finalizarVenda").html('Fechar venda');
            erroMsg('Valor total Inválido!');
            erro = 1;
            return;
        }
        if (erro != 1) {
            $.ajax({
                type: "POST",
                url: "./api/salvar-parcelas.php",
                data: {total: total, entrada: entrada, qtdParcelas: qtdParcelas, tementrada: entradaSelected, idvenda: idvenda, datavenda: datavenda, idcliente: idcliente, condpgto: condicaopgto, cartao: cartao, desconto: desconto, totalValidar: totalValidar},
                dataType: "json",
                success: function (retorno) {
                    $("#finalizarVenda").html('Fechar venda');
                    if (retorno.erro == 1) {
                        erroMsg(retorno.erroMsg);
                        return;
                        erroParcelas = 1;
                    }
                    if (retorno.sucesso == 1) {
                        baixarEstoque();
                    }
                }
            });
        }
    }

    function baixarEstoque() {
        $.ajax({
            type: "POST",
            url: "./api/baixar-estoque.php",
            data: {idvenda: idvenda},
            dataType: "json",
            success: function (retorno) {
                $("#finalizarVenda").html('Fechar venda');
                if (retorno.erro == 1) {
                    erroMsg(retorno.msgErro);
                    return;
                } else {
                    $("#modalegg").html('<div class="modal fade" id="modalEasterEgg2" role="dialog"><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Sucesso</h4></div><div class="modal-body"><div class="mensagem"><h4>Venda salva com sucesso</h4></div></div><div class="modal-footer"><a type="button" href="' + URL + '/vendas.php" class="btn btn-primary" data-dismiss="modal id="sucessoVendaBt">Fechar</a></div></div></div></div>');
                    $('#modalEasterEgg2').modal('show');
                }
                // gerarParcelas();
            }
        });
    }

    function erroMsg(msg) {
        $("#modalegg").html('<div class="modal fade" id="modalEasterEgg" role="dialog" tabindex=-1><div class=modal-dialog><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Erro</h4></div><div class="modal-body"><div class="mensagem"><h4>' + msg + '</h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button></div></div></div></div>');
        $('#modalEasterEggGif').html('<img src="js/easterEgg/img/easteregg.gif" alt="" height="300px" style="margin-left:135px;"/>');
        $('#modalEasterEgg').modal('show');
    }

});

