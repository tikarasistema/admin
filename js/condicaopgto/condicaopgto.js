$("#paginas").on("change", function() {
    window.location.href = 'condicaopgto.php?p=' + $("#paginas").val() + '&q=' + query;
});
var id;

$(".situacao").off('click').on('click', function(e) {
    e.preventDefault();
    id = ($(this).data('id'));
});

$("#btnAlterarStatus").on("click", function() {
    $.ajax({
        type: "POST",
        url: "./api/condicaopgto-status.php",
        data: {condpgto: id},
        dataType: "json",
        success: function(json) {
            if (json.erro) {
                showErrorMsg();
            } else {
                alteraStatus();
            }
        }
    });
});

function alteraStatus() {
    if ($(".situacao[data-id='" + id + "']").hasClass("btn-success")) {
        $(".situacao[data-id='" + id + "']").removeClass("btn-success");
        $(".situacao[data-id='" + id + "']").addClass("btn-danger");
        $(".situacao[data-id='" + id + "']").html('inativo');
    } else if ($(".situacao[data-id='" + id + "']").hasClass("btn-danger")) {
        $(".situacao[data-id='" + id + "']").removeClass("btn-danger");
        $(".situacao[data-id='" + id + "']").addClass("btn-success");
        $(".situacao[data-id='" + id + "']").html('ativo');
    }
}
function showErrorMsg() {
    var html = '';
    html += '<div class="alert  alert-dismissable" id="myAlert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Erro ao alterar Status</div>';
    $(".erroStatus").html(html);
    $(".alert").addClass("alert-danger");
}

