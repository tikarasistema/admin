<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$erro = '';
$where = '';
$pagina = getGet('p', 1);
$query = getGet('q', '');
$editar = true;

try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('clientes-editar.php', $telasPermissao)) {
    $editar = false;
}
$venda = true;
if (!in_array('venda-aberta.php', $telasPermissao)) {
    $venda = false;
}
if ($query) {
    $where = "where nome_razao like '$query%'";
}

$totalRegPag = "10";

$inicio = ($pagina - 1) * $totalRegPag;

$db = MyPdo::connect();
$consulta = $db->query("Select SQL_CALC_FOUND_ROWS * from tbcliente {$where} LIMIT $inicio,$totalRegPag ");
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$clientes = $consulta->fetchAll(PDO::FETCH_ASSOC);
if (!$clientes) {
    $erro = 'Sem clientes para listagem';
}

topo(array(
    "css" => array(
        "css/cliente/cliente.css"
    ),
    "icon" => "fa fa-user",
    "pageName" => " Pesquisar Clientes"
));
?>
<div id="modalVenda"></div>
<div class="modal fade" id="myModal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Alterar status</h4>
            </div>
            <div class="modal-body">
                <div class="mensagem">

                </div>
                <h3>Deseja alterar status?</h3>
            </div>
            <div class="modal-footer">
                <button id="btnAlterarStatus" type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>

    </div>
</div>
<div class="erroStatus">

</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="">
                    <div class="col-xs-11">
                        <div class="form-group">
                            <label class="sr-only" for="fquery">Pesquisa</label>
                            <input value="<?php echo $query; ?>" type="search" class="form-control" id="fquery" name="q" placeholder="Pesquisar">
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a type="reset" a="" href="clientes.php" class="btn btn- btn-default">Limpar Pesquisa

                        </a>
                    </div>
                    <div class="col-xs-1">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class=" fa fa-plus fa-fw"></i>
                            </button>
                            <ul class="dropdown-menu slidedown">
                                <li>
                                    <a href="clientes-cadastrar.php">
                                        <i class="fa fa-plus fa-fw"></i> Novo Cliente
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Situação</th>
                            <th >Nome</th>
                            <th >Telefone</th>
                            <th >Celular</th>
                            <th >Email</th>
                            <th >Tipo</th>
                            <th >Editar</th>
                            <th >Nova Venda</th>
                            <th >Receber</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($clientes as $cliente) { ?>
                            <tr >
                                <td> <?php echo $cliente['idcliente']; ?></td>
                                <td> <?php
                                    if ($cliente['situacao'] == CLIENTE_ATIVO) {
                                        if ($editar) {
                                            ?>
                                            <a href="#" data-id="<?php echo $cliente['idcliente']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs situacao">ativo</a>
                                        <?php } else { ?>
                                            <a href="#"class="btn btn-success btn-xs situacao">ativo</a>
                                            <?php
                                        }
                                    } else {
                                        if ($editar) {
                                            ?>
                                            <a href="#" data-id="<?php echo $cliente['idcliente']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-xs situacao">inativo</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="#"class="btn btn-danger btn-xs situacao">inativo</a>
                                            <?php
                                        }
                                    }
                                    ?></td>
                                <td> <?php echo $cliente['nome_razao']; ?></td>
                                <td> <?php echo $cliente['telefone']; ?></td>
                                <td> <?php echo $cliente['celular']; ?></td>
                                <td> <?php echo $cliente['email']; ?></td>
                                <td> <?php
                                    if ($cliente['tipo'] == PESSOA_FISICA) {
                                        echo 'Física';
                                    } elseif ($cliente['tipo'] == PESSOA_JURIDICA) {
                                        echo 'Jurídica';
                                    } else {
                                        echo '';
                                    }
                                    ?></td>
                                <td> <a href="clientes-editar.php?cd=<?php echo $cliente['idcliente']; ?>" title="Editar"><i class="fa fa-edit"></i></a></td>
                                <?php if ($venda != false) { ?>
                                    <td> <a href="#" class="venda" data-nome=" <?php echo $cliente['nome_razao']; ?>" data-id="<?php echo $cliente['idcliente']; ?>" title="Nova venda"><i class="fa fa-money"></i></a></td>
                                <?php } ?>
                                <td><a href="receber.php?idcliente=<?php echo $cliente['idcliente']; ?>" title="Exibir venda"><i class="fa fa-list-alt"></i></a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                </div>

            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="clientes.php?p=<?php echo $anterior; ?>&q=<?php echo $query; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php
                    for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="clientes.php?p=<?php echo $proximo; ?>&q=<?php echo $query; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>



<?php
rodape(array(
    "js" => array(
        "js/cliente/cliente.js",
    )
));
?>
<script>
    var query = '<?php echo $query; ?>';
</script>
