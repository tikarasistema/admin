<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$idusuario = '';
$_SESSION['erro'] = false;

if (isset($_GET['cd'])) {
    $idusuario = $_GET['cd'];
    $sql = "Select * from tbusuario where idusuario = :idusuario";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idusuario', $idusuario);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$consulta) {
        editarErro(array(
            'page' => 'usuarios.php',
            'origem' => 'usuário',
            'tipo' => 'Erro na atualização'
        ));
    }
} elseif (isset($_POST['idusuario'])) {
    $idusuario = $_POST['idusuario'];
} else {
    editarErro(array(
        'page' => 'usuarios.php',
        'origem' => 'usuario',
        'tipo' => 'Erro na atualização'
    ));
}

$sql = "Select * from tbusuario where idusuario = :idusuario";
$stmt = $con->prepare($sql);
$stmt->bindValue(':idusuario', $idusuario);
$stmt->execute();
$usuario = $stmt->fetch(PDO::FETCH_ASSOC);


$email = '';
$nome = '';
$senha = '';
$senha2 = '';
$tipo = '';
$cpfcnpj = '';
$telefone = '';
$celular = '';
$logradouro = '';
$bairro = '';
$numero = '';
$complemento = '';
$cidade = 3159;
$estado = 18;
$estadosTeste = '';
$cidadesTeste = '';
$situacao = USUARIO_ATIVO;
$status = PESSOA_FISICA;
$_SESSION['erro'] = false;

$sqlEstado = "SELECT id FROM estado";
$consulta = $con->query($sqlEstado);
$consulta->execute();

$sqlCidade = "SELECT id FROM cidade";
$consultaCidade = $con->query($sqlCidade);
$consultaCidade->execute();

$sqlCnpj = "Select cpf_cnpj from tbusuario where idusuario != $idusuario";
$consultaCnpjCpf = $con->query($sqlCnpj);
$consultaCnpjCpf->execute();

$estados = $consulta->fetchAll(PDO::FETCH_COLUMN);
$cidades = $consultaCidade->fetchAll(PDO::FETCH_COLUMN);
$cnpjcpfs = $consultaCnpjCpf->fetchAll(PDO::FETCH_COLUMN);



$stmt = $con->query("Select idpapel, dspapel from tbpapel where situacao = " . PAPEL_ATIVO);
$stmt->execute();
$papeisDiponiveis = $stmt->fetchAll(PDO::FETCH_ASSOC);

if ($_POST) {
    $nome = getPost('nome');
    $email = getPost('email');
    $senha = getPost('senha');
    $senha2 = getPost('senha2');
    $tipo = getPost('tipo');
    $idusuario = getPost('idusuario');
    $senhaAnterior = senha(getPost('senhaAnterior'));
    $situacao = getPost('situacao');
    $cpfcnpj = getPost('cpfcnpj');
    $telefone = getPost('telefone');
    $celular = getPost('celular');
    $logradouro = getPost('logradouro');
    $bairro = getPost('bairro');
    $numero = getPost('numero');
    $estado = getPost('estado', 18);
    $estado = getPost('estado');
    //$status = get_Post('status');
    $_SESSION['erro'] = false;

    if ($situacao) {
        $situacao = USUARIO_ATIVO;
    } else {
        $situacao = USUARIO_INATIVO;
    }
//Falta validar
    $complemento = getPost('complemento');
    $cidade = getPost('cidade', 3159);




    if (!$tipo) {
        addMessage('Selecione um papel');
        erro();
    }
    if (empty($nome)) {
        addMessage("Preencha o nome");
        erro();
    }
    if (empty($senha)) {
        addMessage("Preencha a senha");
        erro();
    } elseif (strlen($senha) < 6) {
        addMessage("Senha deve ter mínimo de 6 dígitos");
        erro();
    }

    if ($senha != $senha2) {
        addMessage("O campo senha e repita senha devem ser iguais");
        erro();
    }
    if (empty($email)) {
        addMessage("Preencha o email");
        erro();
    }

    if ($estado == 0) {
        addMessage("Selecione um estado");
        erro();
    }
    if ($cidade == 0) {
        addMessage("Selecione uma cidade");
        erro();
    }
    if (stringValidate($nome) == 0) {
        addMessage('Nome deve conter apenas letras');
        erro();
    }

    if (in_array($cpfcnpj, $cnpjcpfs)) {
        addMessage('Cpf ou cnpj de cliente ja cadastrado insira outro cliente');
        erro();
    }
//    if (!in_array($status, $tipos)) {
//        addMessage('Selecione um tipo válido');
//        erro();
//    }
    if (!in_array($estado, $estados)) {
        addMessage('Selecione um estado válido');
        erro();
    }
    if (!in_array($estado, $cidades)) {
        addMessage('Selecione uma cidade válida');
        erro();
    }

    if ($status == PESSOA_FISICA) {
        $retorno = cpf($cpfcnpj);
        if (true !== $retorno) {
            addMessage('CPF inválido!');
            erro();
        }
    } elseif ($status == PESSOA_JURIDICA) {
        $retorno = cnpj($cpfcnpj);
        if (!$retorno) {
            addMessage('CNPJ inválido!');
            erro();
        }
    }

    if (emailValidate($email) != true) {
        addMessage('Email Inválido');
        erro();
    }
    if (false === numberValidate($numero)) {
        addMessage("Preencha campo número com números ");
        erro();
    }

    if (!empty($cpfcnpj)) {
        if (in_array($cpfcnpj, $cnpjcpfs)) {
            addMessage('Cpf ou cnpj de cliente ja cadastrado insira outro cliente');
            erro();
        }
    }
//    if (!empty($tipo)) {
//        if (!in_array($tipo, $tipos)) {
//            addMessage('Selecione um tipo válido');
//            erro();
//        }
//    }
    if ($estado != 0) {
        if (!in_array($estado, $estados)) {
            addMessage('Selecione um estado válido');
            erro();
        }
    }
    if ($cidade != 0) {
        if (!in_array($cidade, $cidades)) {
            addMessage('Selecione uma cidade válida');
            erro();
        }
    }

    if (!empty($cpfcnpj)) {
        if ($status == PESSOA_FISICA) {
            $retorno = cpf($cpfcnpj);
            if (true !== $retorno) {
                addMessage('CPF inválido!');
                erro();
            }
        } elseif ($status == PESSOA_JURIDICA) {
            $retorno = cnpj($cpfcnpj);
            if (!$retorno) {
                addMessage('CNPJ inválido!');
                erro();
            }
        } else {
            addMessage('Informe o tipo');
            erro();
        }
    }
    if (!empty($email)) {
        if (emailValidate($email) != true) {
            addMessage('Email Inválido');
            erro();
        }
    }
    if (!empty($numero)) {
        if (false === numberValidate($numero)) {
            addMessage("Preencha campo número com números ");
            erro();
        }
    }
    if (empty($nome)) {
        addMessage("Preencha o campo nome");
        erro();
    } else
    if (stringValidate($nome) == 0) {
        addMessage('Nome deve conter apenas letras');
        erro();
    }



    $con = MyPdo::connect();
    $sql = "Select UPPER(stemail) from tbusuario where stemail = UPPER(:stemail) and idusuario != :idusuario";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':stemail', $email);
    $stmt->bindValue(':idusuario', $idusuario);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($consulta) {
        addMessage('Email já cadastrado, insira um email diferente');
        erro();
    }
//fazer bindvalue
    if (!getSession('erro')) {
        $senha = senha($senha);
        $db = MyPdo::connect();
        $prepare = $db->prepare('Update tbusuario set stnome = :stnome, stemail = :stemail'
                . ', stsenha = :stsenha, situacao = :situacao, intipo = :intipo, cpf_cnpj = :cpf_cnpj, telefone = :telefone, celular = :celular, logradouro = :logradouro, bairro = :bairro, numero = :numero, complemento = :complemento, cidade = :cidade, estado = :estado, status = :status where idusuario = :idusuario');
        $prepare->execute(array(
            ':stnome' => $nome,
            ':stemail' => $email,
            ':stsenha' => $senha,
            ':situacao' => $situacao,
            ':intipo' => $tipo,
            ':idusuario' => $idusuario,
            ':cpf_cnpj' => $cpfcnpj,
            ':telefone' => $telefone,
            ':celular' => $celular,
            ':logradouro' => $logradouro,
            ':bairro' => $bairro,
            ':numero' => $numero,
            ':complemento' => $complemento,
            ':cidade' => $cidade,
            ':estado' => $estado,
            ':status' => $status,
        ));
        sucessInsertUpdate(array(
            'page' => 'usuarios.php',
            'origem' => 'usuário',
            'tipo' => 'Atualização'
        ));
    }
} else {
    $nome = $usuario['stnome'];
    $email = $usuario['stemail'];
    $tipo = $usuario['intipo'];
    $situacao = $usuario['situacao'];
    $cpfcnpj = $usuario['cpf_cnpj'];
    $telefone = $usuario['telefone'];
    $celular = $usuario['celular'];
    $logradouro = $usuario['logradouro'];
    $bairro = $usuario['bairro'];
    $numero = $usuario['numero'];
    $cidade = $usuario['cidade'];
    $estado = $usuario['estado'];
    $status = $usuario['status'];
}
topo(array(
    'css' => array(
        "css/usuario/usuario.css",
    ),
    'pageName' => ' Editar usuário #' . $idusuario,
    'icon' => 'fa fa-user',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="alterarUsuarios" name="alterarUsuarios" role="form" method="post" action="usuarios-editar.php">
                    <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcliente">Nome*</label>
                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite nome " value="<?php echo $nome; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcpfcnpj">CPF / CNPJ*</label>
                                    <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Somente números " value="<?php echo $cpfcnpj; ?>">
                                </div>
                            </div>
                        </div>
<!--
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fstatus">Tipo*</label>
                                    <select class="form-control" name="status" id="fstatus">
                                        <option value="0">Selecione</option>
                                        <option value="1" <?php echo $status == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                        <option value="2" <?php echo $status == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                    </select>
                                </div>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="ftelefone">Telefone*</label>
                                    <input type="text" data-mask="(00) 0000-0000" class="form-control" id="ftelefone" name="telefone" value="<?php echo $telefone; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fcelular">Celular*</label>
                                    <input type="text" class="form-control" id="fcelular" name="celular" value="<?php echo $celular; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Endereço</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="flogradouro">Logradouro*</label>
                                            <input type="text" name="logradouro" id="flogradouro" class="form-control" value="<?php echo $logradouro; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fnumero">Bairro*</label>
                                            <input type="text" name="bairro" id="fbairro" class="form-control" value="<?php echo $bairro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-4">
                                        <div class="form-group">
                                            <label for="fnumero">Número*</label>
                                            <input type="number" name="numero" id="fnumero" class="form-control" value="<?php echo $numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="form-group">
                                            <label for="fcomplemento">Complemento</label>
                                            <input type="text" name="complemento" id="fcomplemento" class="form-control" value="<?php echo $complemento; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="festado">Estado* </label>
                                            <select class="form-control" id="festado" name="estado">
                                                <option value="0">Escolha o estado </option>
                                                <?php
                                                $sql = "SELECT id, nome FROM estado order by nome";
                                                $consulta = $con->query($sql);
                                                $consulta->execute();
                                                while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $resultado['id']; ?>"
                                                            <?php if ($resultado == $resultado['id']) { ?> selected <?php } ?>
                                                            ><?php echo $resultado['nome']; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fcidade">Cidade* </label>
                                            <select class="form-control" id="fcidade" name="cidade">
                                                <option value="0">--Selecione o Estado --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Dados de Autenticação</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email*</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite email válido " value="<?php echo $email; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label for="senha">Senha*</label>
                                                <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite senha ">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <label for="senha2">Digite Senha Novamente*</label>
                                            <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Digite senha novamente ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="ftipo">Papel*</label>
                                            <select class="form-control" name="tipo" id="ftipo">
                                                <option value="0">Selecione um papel</option>
                                                <?php foreach ($papeisDiponiveis as $value) { ?>
                                                    <option value="<?php echo $value['idpapel']; ?>" <?php echo $tipo == $value['idpapel'] ? 'selected' : ''; ?>><?php echo $value['dspapel']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6  col-md-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>   <input form="alterarUsuarios" type="checkbox" name="situacao" <?php echo $situacao ? 'checked' : ''; ?>  > Ativar usuário?</label>

                                </div>
                                <div class="help-block">Ao selecionar esta opção status do cliente sera ativo, caso contrario status sera inativo</div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="alterarUsuarios">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="alterarUsuarios">Cancelar </button>

        </div>
    </div>
</div>
<script>
    var cidade = '<?php echo $cidade; ?>';
</script>
<?php
rodape(array(
    'js' => array(
        'js/usuario/usuario.js',
    )
));
?>