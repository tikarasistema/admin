
<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
?>

<?php
$dsproduto = '';
$precocompra = '';
$precovenda = '';
$estoque = 0;
$unidade = '';
$codigobarra = '';
$marca = '';
$classificacao = '';
$lucro = '';
$idproduto = '';
$consulta = '';
$_SESSION['erro'] = false;
$con = myPdo::connect();

if (isset($_GET['cd'])) {
    $idproduto = $_GET['cd'];
    $sql = "Select * from tbproduto where idproduto = :idproduto";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idproduto', $idproduto);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$consulta) {
        editarErro(array(
            'page' => 'produtos.php',
            'origem' => 'produto',
            'tipo' => 'Erro na atualização'
        ));
    }
} elseif (isset($_POST['cd'])) {
    $idproduto = $_POST['cd'];
} else {
    editarErro(array(
        'page' => 'produtos.php',
        'origem' => 'produto',
        'tipo' => 'Erro na atualização'
    ));
}
//$sql = "Select * from tbproduto where idproduto = :idproduto";
//$stmt = $con->prepare($sql);
//$stmt->bindValue(':idproduto', $idproduto);
//$stmt->execute();
//$consulta = $stmt->fetch(PDO::FETCH_ASSOC);


if ($_POST) {

    $dsproduto = $_POST['produto'];
    $precocompra = $_POST['precocompra'];
    $precovenda = $_POST['precovenda'];
    $situacao = $_POST['situacao'];
    $estoque = $_POST['estoque'];
    $codigobarra = $_POST['codigobarra'];
    $observacao = $_POST['observacao'];
    $marca = $_POST['marca'];
    $classificacao = $_POST['classificacao'];
    $lucro = $_POST['lucro'];

    if ($situacao) {
        $situacao = PRODUTO_ATIVO;
    } else {
        $situacao = PRODUTO_INATIVO;
    }

    $sql = "SELECT idmarca FROM tbmarca where situacao != " . MARCA_INATIVA;
    $stmt = $con->query($sql);
    $marcas = $stmt->fetchall(PDO::FETCH_COLUMN);

    $sql = "SELECT idclassificacao FROM tbtipo";
    $stmt = $con->query($sql);
    $classificacoes = $stmt->fetchall(PDO::FETCH_COLUMN);

    if ($classificacao != 0) {
        if (!in_array($classificacao, $classificacoes)) {
            addMessage('Selecione tipo válido');
            erro();
        }
    }
    if ($marca != 0 and $classificacao != 0) {
        if (!in_array($marca, $marcas)) {
            addMessage('Selecione marca válida, a marca selecionada pode estar inativa!');
            erro();
        }
    }

    if (strlen($dsproduto) <= 3) {
        addMessage("Preencha o campo produto");
        $_SESSION['erro'] = true;
    }

    if (empty($codigobarra)) {
        addMessage("Preencha o código de barra");
        $_SESSION['erro'] = true;
    }

    try {
        $sql = "select codigobarra from tbproduto where idproduto != :idproduto and codigobarra = :codbarra";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idproduto', $idproduto);
        $stmt->bindValue(':codbarra', $codigobarra);
        $stmt->execute();


        $codbarra = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($codbarra['codigobarra']) {
            addMessage("Produto com código de barra ja existe digite outro produto");
            $_SESSION['erro'] = true;
        }
    } catch (PDOException $e) {
        echo '' . $e->getMessage();
    }


    if (empty($precocompra)) {
        addMessage("Preencha o preço de compra");
        $_SESSION['erro'] = true;
    }
    if ($precocompra < 0) {
        addMessage("Preço de compra negativo, digite valores positivos");
        erro();
    }

    if (empty($precovenda)) {
        addMessage("Preencha o preço de venda");
        $_SESSION['erro'] = true;
    }
    if ($precovenda < 0) {
        addMessage("Preço de venda negativo, digite valores positivos");
    }

    if ($precovenda < $precocompra) {
        addMessage("Preço de venda deve ser maior que preço de compra");
        $_SESSION['erro'] = true;
    }
    if (empty($estoque)) {
        addMessage('Preencha campo estoque');
        erro();
    }
    if ($estoque < 0) {
        addMessage('Quantidade de estoque inválido');
        erro();
    }
    if (empty($lucro)) {
        addMessage('Digite Lucro');
        erro();
    }

    if ($lucro < 0) {
        addMessage('Lucro inválido');
        erro();
    }

    if (!getSession('erro')) {
        try {
            $stmt = $con->prepare("Update  tbproduto set produto = :produto, precocompra = :precocompra, precovenda = :precovenda, situacao = :situacao, estoque = :estoque, codigobarra = :codigobarra, observacao = :observacao, idmarca = :idmarca, idclassificacao = :idclassificacao, lucro = :lucro where idproduto = :idproduto");
            $stmt->bindValue(':produto', $dsproduto);
            $stmt->bindValue(':precocompra', $precocompra);
            $stmt->bindValue(':precovenda', $precovenda);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->bindvalue(':estoque', $estoque);
            $stmt->bindValue(':codigobarra', $codigobarra);
            $stmt->bindValue(':observacao', $observacao);
            $stmt->bindValue(':idmarca', $marca);
            $stmt->bindValue(':idclassificacao', $classificacao);
            $stmt->bindValue(':lucro', $lucro);
            $stmt->bindValue(':idproduto', $idproduto);
            $r = $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'produtos.php',
                'origem' => 'produto',
                'tipo' => 'Editar'
            ));
        } catch (PDOException $e) {
            addMessage("Erro ao editar" . $e);
            $_SESSION['erro'] = true;
        }
    }
} else {

    $dsproduto = $consulta['produto'];
    $precocompra = $consulta['precocompra'];
    $precovenda = $consulta['precovenda'];
    $situacao = $consulta['situacao'];
    $estoque = $consulta['estoque'];
    $codigobarra = $consulta['codigobarra'];
    $observacao = $consulta['observacao'];
    $marca = $consulta['idmarca'];
    $classificacao = $consulta['idclassificacao'];
    $lucro = $consulta['lucro'];
}
?>


<?php
topo(array(
    'css' => array(
        "css/produto/produto.css",
    ),
    'pageName' => ' Editar Produto',
    'icon' => 'fa fa-inbox fa-lg',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="editarProduto" name="produtos-editar" role="form" method="post" action="produtos-editar.php">
                    <input type="hidden" value="<?php echo $idproduto; ?>" id="cdProduto" name="cd">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="fcodigobarra"> *Código de barra </label>
                                    <input value="<?php echo $codigobarra; ?>" type="text" class="form-control" id="fcodigobarra" name="codigobarra" placeholder="Informe o código de barra ">
                                    <span id="help-block" class="help-block">&nbspExemplo: 0123456789</span>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="fproduto">Descrição do Produto *</label>
                                    <input value ="<?php echo $dsproduto; ?>" type="text" class="form-control" id="fproduto" name="produto"placeholder="Informe o produto ">
                                    <span id="help-block" class="help-block">&nbspMin: 3 caracteres</span>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="festoque">Estoque *</label>
                                <input value="<?php echo $estoque; ?>" type="number" class="form-control" id="festoque" name="estoque"value="0" placeholder="Informe o estoque ">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="precocompra">Preço compra *</label>
                                <div class="input-group">
                                    <span class="input-group-addon">R$</span>

                                    <input value="<?php echo $precocompra; ?>"type="text" class="form-control" id="precocompra"  name="precocompra"  length="15" onkeypress="return formatar_moeda(this, '', '.', event);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="lucro">Lucro*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">%</span>
                                    <input type="text" class="form-control" id="lucro"  name="lucro" value="<?php echo $lucro; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="fprecovenda">Preço venda *</label>
                                <div class="input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input value="<?php echo $precovenda; ?>"type="text" class="form-control" id="precovenda"  name="precovenda"  length="15" onkeypress="return formatar_moedavenda(this, '', '.', event);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="classificacao">Classificação </label>
                                    <select class="form-control" id="classificacao" name="classificacao">
                                        <option value="0">Escolha o tipo de produto </option>
                                        <?php
                                        $sql = "SELECT idclassificacao, dsclassificacao FROM tbtipo  where situacao != '" . CLASSIFICACAO_INATIVO . "' order by dsclassificacao";
                                        $consulta = $con->query($sql);
                                        $consulta->execute();
                                        while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <option <?php echo $resultado['idclassificacao'] == $classificacao ? 'selected' : ''; ?> value="<?php echo $resultado['idclassificacao']; ?>" >
                                                <?php echo $resultado['dsclassificacao'];
                                                ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="marca">Marca </label>
                                    <select class="form-control" id="marca" name="marca">
                                        <option value="0">--Selecione o Tipo --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="form-group">
                                    <label for="observacao">Observação</label>
                                    <input type="text" class="form-control" id="observacao" name="observacao" placeholder=" ">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="situacao" <?php echo $situacao == PRODUTO_ATIVO ? 'checked' : ''; ?>> Produto ativo?
                                        </label>
                                    </div>
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            </form>

                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary" form="editarProduto">Salvar </button>
                            <button type="reset" class="btn btn-danger"  form="editarProduto">Cancelar </button>

                        </div>

                    </div>
            </div>
        </div>
        <script>
            var precocompra = '<?php echo $precocompra; ?>';
            var lucro = '<?php echo $lucro; ?>';
            var marca = <?php echo $marca; ?>;
        </script>
        <?php
        rodape(array(
            'js' => array(
                'js/produto/produto.js',
            )
        ));
        ?>
