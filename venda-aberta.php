<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$venda = array();
$idvenda = '';
$idcliente = '';
$dataVenda = '';
$situacao = '';
$condpgto = '';
$nome = '';
$con = MyPdo::connect();
$_SESSION['erro'] = false;
$condicaopgto = array();



if (isset($_GET['idvenda'])) {
//    if (!isset($_SERVER['HTTP_REFERER'])) {
//        $_SERVER['HTTP_REFERER'] = '';
//    }
//
//    $uri = explode('?', $_SERVER['HTTP_REFERER']);
//    $request = explode('/', ltrim($uri[0], '/'));
//    $paginaAnterior = end($request);
//
//    if ($paginaAnterior != "clientes.php") {
//
//        editarErro(array(
//            'page' => 'index.php',
//            'tipo' => 'Venda não pode ser inicializada, cliente inválido'
//        ));
//    }
    $idvenda = getGet('idvenda');
    $stmt = $con->prepare("Select tbcliente.idcliente, nome_razao, cpf_cnpj, celular, email, data_venda, tbvenda.situacao from tbcliente inner join tbvenda"
            . " on tbcliente.idcliente = tbvenda.idcliente where idvenda = :idvenda and tbvenda.situacao = " . VENDA_ABERTA);
    $stmt->bindValue(":idvenda", $idvenda);
    $stmt->execute();
    $venda = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$venda) {
        editarErro(array(
            'page' => 'clientes.php',
            'tipo' => 'Venda não pode ser inicializada, venda inválida'
        ));
    } else {
        $dataVenda = date(('d/m/Y H:i:s'), strtotime($venda['data_venda']));
        $situacao = $venda['situacao'];
        $nome = $venda['nome_razao'];
        $cpfcnpj = $venda['cpf_cnpj'];
        $celular = $venda['celular'];
        $email = $venda['email'];
    }
} else {
    editarErro(array(
        'page' => 'index.php',
        'tipo' => 'Venda não pode ser inicializada, cliente inválido'
    ));
}
?>
<?php
topo(array(
    'css' => array(
        'css/venda/venda.css',
    ),
    'pageName' => ' Nova venda #' . $idvenda,
    'icon' => 'fa fa-money',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Nova venda</h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <label>Data venda: <?php echo $dataVenda; ?></span></label>

                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <label> Situação: <?php echo $situacao == VENDA_ABERTA ? "Venda aberta" : ''; ?></label>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php
                        $sql = "Select stnome from tbusuario where idusuario = (select idusuario from tbvenda where idvenda = :idvenda)";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':idvenda', $idvenda);
                        $stmt->execute();
                        $nomeVendedor = $stmt->fetch(PDO::FETCH_COLUMN);
                        ?>
                        <label> Vendedor: <?php echo $nomeVendedor; ?></label>
                    </div>

                </div><br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Dados cliente</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <label>Cliente: <?php echo $nome; ?> </label><br>
                            <label><?php echo $cpfcnpj ? 'Cpf/Cnpj: ' . $cpfcnpj : ''; ?> </label><br>
                            <label><?php echo $celular ? 'Celular: ' . $celular : ''; ?> </label><br>
                            <label><?php echo $email ? 'Email: ' . $email : ''; ?> </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <form id="pesquisarProduto" >
                <div class="panel-heading">
                    <h1 class="panel-title">Pesquisar Produtos</h1>
                </div>
                <div class="panel-body">
                    <div id="modalErro"></div>
                    <div id="modalRemoverProd"></div>
                    <div id="modalConfirm"></div>
                    <div id="modalCancelar"></div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="pesquisa">Método de pesquisa* </label>
    <!--                                    <select class="form-control" id="pesquisa" name="pesquisa">
                                        <option value="1">Código de barra</option>
                                        <option value="2">Descrição</option>
                                    </select>-->
                                    <div class="radio">
                                        <label><input checked type="radio" class="metodo" value="1" name="optradio">Código de barra</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" class="metodo" value="2" name="optradio">Descrição</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="produto">Código de barra / Descrição*</label>
                                    <input type="text" class="form-control" id="produto" name="produto" placeholder="Digite código de barra ou descrição do produto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button id="btPesquisaProdutos" type="submit" class="btn btn-primary">Pesquisar</button>
                    <a title="Cadastrar produto" href="http://estagio.dev/admin/produtos-cadastrar.php" target="_blank" class="btn btn-success" style="float: right;">Cadastrar produto</a>
                </div>
            </form>
        </div>
        <div id="produtos" class="esconder">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos encontrados</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descricao</th>
                                <th>Valor compra</th>
                                <th>Valor venda</th>
                                <th>Quantidade</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="produtosListar">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        $sql = "Select i.*, p.produto from tbitem_venda as i inner join tbproduto as p on i.idproduto = p.idproduto where idvenda = :idvenda";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':idvenda', $idvenda);
        $stmt->execute();
        $itens = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $esconder = "class='esconder'";
        ?>
        <div id="produtosVenda" <?php echo $itens ? '' : $esconder; ?>>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Produtos venda</h1>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>ID Produto</th>
                                <th>Descrição</th>
                                <th>Quantidade</th>
                                <th>Valor</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="produtosVendaListar">
                            <?php foreach ($itens as $item) { ?>
                                <tr class="linhaProdVenda" data-idvenda="<?php echo $item['idvenda']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>">
                                    <td class="idproduto"><?php echo $item['idproduto']; ?></td>
                                    <td class="idproduto"><?php echo $item['produto']; ?></td>
                                    <td class="qtde_item"><?php echo $item['qtde_item']; ?></td>
                                    <td class="preco_item"><?php echo $item['preco_item']; ?></td>
                                    <td><a href="#" title="Remover produto" class="removerProduto" data-idvenda="<?php echo $item['idvenda']; ?>" data-idproduto="<?php echo $item['idproduto']; ?>" data-descricao="<?php echo $item['produto']; ?>"><i class="fa fa-trash-o"></i></a></td>
                                    <td class="subTotalProdValor"><input type='hidden' value="<?php echo $item['preco_item'] * $item['qtde_item']; ?>" class='subTotalProd'></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="condPgto" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Condição de pagamento</h1>
                </div>
                <div class="panel-body">
                    <form id="condicaoPgto">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalVenda">Sub-total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input disabled="" type="text" class="form-control" id="totalvenda"  value="0.00" name="totalvenda">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalVenda">Desconto</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="desconto" value="0.00" name="desconto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalVenda">Total</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control" id="subtotal" value="0.00" name="desconto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="pesquisa">Condição pagamento* </label>
                                        <select class="form-control" id="condicaoPagamento" name="pesquisa" required min="1">
                                            <option data-qtdparcela="-1" value="0">Selecione condição pagamento</option>
                                            <?php
                                            $sql = "Select * from tbcondpgto where situacao != " . CONDPGTO_INATIVO;
                                            $stmt = $con->query($sql);
                                            $condicoesPgto = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                            foreach ($condicoesPgto as $condicaopgto) {
                                                ?>
                                                <option data-qtdparcela="<?php echo $condicaopgto['qtde_parcela']; ?>" data-entrada="<?php echo $condicaopgto['entrada']; ?>" value="<?php echo $condicaopgto['idcondpgto']; ?>"><?php echo $condicaopgto['ds_condicao']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row esconder" id="entrada" >
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="totalVenda">Entrada*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input id="valorEntrada" type="text" class="form-control" value="0.00" name="entrada">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="cartao" value="1">Cartão</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="panel-footer">
                        <button id="finalizarVenda" type="submit" class="btn btn-primary">Fechar Venda</button>
                        <button id="cancelarVenda" type="submit" class="btn btn-danger"style="margin-left: 5%;">Cancelar Venda</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $diavenda = explode(' ', $venda['data_venda']);
    $diavenda = $diavenda[0];
    ?>
    <script>
        var idvenda = "<?php echo $idvenda; ?>";
        var datavenda = "<?php echo $diavenda; ?>";
        var idcliente = "<?php echo $venda['idcliente']; ?>";
    </script>

    <?php
    rodape(array(
        'js' => array(
            'js/venda/venda.js',
        )
    ));
    ?>





