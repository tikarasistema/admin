<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

define('DESCRICAO', 1);
define('CODBARRA', 0);
$erro = '';
$where = '';
$pagina = getGet('p', 1);
$query = getGet('q', '');
$editar = true;
$sql = '';
$marca = getGet('marca', '');
$classificacao = getGet('classificacao', '');
$cdbarradescricao = getGet('cdbarradescricao', '');

if ($cdbarradescricao == DESCRICAO) {
    $pesquisa = " produto like '$query%'";
} elseif ($cdbarradescricao == CODBARRA) {
    $pesquisa = " codigobarra like '%$query%'";
}



try {
    $con = MyPdo::connect();
    $sql = 'Select telas from tbpapel where idpapel = :idpapel';
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idpapel', $_SESSION['usuario']['tipo']);
    $stmt->execute();
    $telas = $stmt->fetch(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    print_r($e);
}
$telasPermissao = explode(',', $telas);

if (!in_array('produtos-editar.php', $telasPermissao)) {
    $editar = false;
}


$totalRegPag = "10";

$inicio = ($pagina - 1) * $totalRegPag;

$sql = "Select SQL_CALC_FOUND_ROWS * from tbproduto";
$array = array();


if ($classificacao != 0 and $marca != 0 and $query != '') {
    $array[] .= " $pesquisa and idclassificacao = $classificacao and  idmarca = $marca";
} else
if ($classificacao != 0 and $marca != 0) {
    $array[] .= " idclassificacao = $classificacao and  idmarca = $marca";
} else if ($classificacao != 0 and $query != '') {
    $array[] .="$pesquisa and idclassificacao = $classificacao";
} else
if ($marca != 0 and $query != '') {
    $array[] .="$pesquisa and idmarca = $marca";
} else
if ($classificacao != 0) {
    $array[] .= " idclassificacao = $classificacao";
} else
if ($marca != 0) {
    $array[] .= " idmarca = $marca";
} else
if ($query) {
    $array[] .= " $pesquisa";
}
//produto like '%$query%' or codigobarra like '%$query%'
if ($array) {

    $sql .= " Where " . join(' or ', $array);
}

$sql .= " order by produto asc LIMIT $inicio,$totalRegPag ";


$db = MyPdo::connect();
$consulta = $db->query($sql);
$qtdRows = $db->query("Select FOUND_ROWS()")->fetch(PDO::FETCH_COLUMN);
$tp = ceil($qtdRows / $totalRegPag);

$anterior = $pagina - 1;
$proximo = $pagina + 1;

$produtos = $consulta->fetchAll(PDO::FETCH_ASSOC);

if (!$produtos) {
    $erro = 'Sem produtos para listagem';
}

topo(array(
    "css" => array(
        "css/produto/produto.css"
    ),
    "icon" => "fa fa-inbox",
    "pageName" => " Pesquisar Produtos"
));
?>
<div class="modal fade" id="myModal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Alterar status</h4>
            </div>
            <div class="modal-body">
                <div class="mensagem">

                </div>
                <h3>Deseja alterar status?</h3>
            </div>
            <div class="modal-footer">
                <button id="btnAlterarStatus" type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>

    </div>
</div>
<div class="erroStatus">

</div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form class="panel-body form-inline" role="form" method="get" action="">
                    <div class="col-xs-11">
                        <div class="form-group">
                            <!--<label class="sr-only" for="fquery">Pesquisa</label>-->
                            <select class="form-control" name="cdbarradescricao" id="cdbarradescricao" style="margin-right: 20px;">
                                <option value="0" <?php echo $cdbarradescricao == CODBARRA ? 'selected' : ''; ?>>Código de barra</option>
                                <option value="1" <?php echo $cdbarradescricao == DESCRICAO ? 'selected' : ''; ?>>Descrição</option>
                            </select>
                            <input value="<?php echo $query; ?>" type="search" class="form-control" id="fquery" name="q" placeholder="Pesquisar" style="margin: 0 20px 0 20px;">
                            <select class="form-control" name="classificacao" id="class" style="margin-right: 20px;">
                                <option value="0">Tipo</option>
                                <?php
                                $sqlcat = 'Select idclassificacao,dsclassificacao from tbtipo where (situacao = 1) order by dsclassificacao asc';
                                $stmt = $con->query($sqlcat);
                                while ($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <option value="<?php echo $r['idclassificacao']; ?>" <?php if ($classificacao == $r['idclassificacao']) { ?> selected <?php } ?>><?php echo $r['dsclassificacao']; ?></option>
                                <?php } ?>
                            </select>
                            <select class="form-control" name="marca" id="marca" style="margin: 0 20px 0 20px;">
                                <option value="0">Marca</option>
                                <?php
                                $sqlmarca = 'Select idmarca,dsmarca from tbmarca where (situacao = 1) order by dsmarca';
                                $stmt = $con->query($sqlmarca);
                                while ($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <option value="<?php echo $r['idmarca']; ?>" <?php if ($marca == $r['idmarca']) { ?> selected <?php } ?>><?php echo $r['dsmarca']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-search"></span>&nbsp;Pesquisar

                        </button>
                        <a type="reset" a="" href="produtos.php" class="btn  btn-default btn-xs">Limpar Pesquisa

                        </a>
                    </div>
                    <div class="col-xs-1">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class=" fa fa-plus fa-fw"></i>
                            </button>
                            <ul class="dropdown-menu slidedown">
                                <li>
                                    <a href="produtos-cadastrar.php">
                                        <i class="fa fa-plus fa-fw"></i> Novo Produto
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-body">
                <?php echo $erro; ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Situação</th>
                                <th>Descrição</th>
                                <th>Valor Compra</th>
                                <th>Valor venda</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($produtos as $produto) { ?>
                                <tr>
                                    <td> <?php echo $produto['idproduto']; ?></td>
                                    <td> <?php
                                        if ($produto['situacao'] == CLIENTE_ATIVO) {
                                            if ($editar) {
                                                ?>
                                                <a href="#" data-id="<?php echo $produto['idproduto']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs situacao">ativo</a>
                                            <?php } else { ?>
                                                <a href="#"class="btn btn-success btn-xs situacao">ativo</a>
                                                <?php
                                            }
                                        } else {
                                            if ($editar) {
                                                ?>
                                                <a href="#" data-id="<?php echo $produto['idproduto']; ?>" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-xs situacao">inativo</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="#"class="btn btn-danger btn-xs situacao">inativo</a>
                                                <?php
                                            }
                                        }
                                        ?></td>
                                    <td> <?php echo $produto['produto']; ?></td>
                                    <td> <?php echo $produto['precocompra']; ?></td>
                                    <td> <?php echo $produto['precovenda']; ?></td>
                                    <td> <a href="produtos-editar.php?cd=<?php echo $produto['idproduto']; ?>"><i class="fa fa-edit"></i></a></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">
                <?php if ($anterior < 1) { ?>
                    <a class="btn btn-default" href="#"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <?php if ($anterior >= 1) { ?>
                    <a class="btn btn-default" href="produtos.php?p=<?php echo $anterior; ?>&q=<?php echo $query; ?>"><i class="fa fa-arrow-left"></i> Anterior</a>
                <?php } ?>
                <select id="paginas">
                    <?php
                    for ($i = 1; $i <= $tp; $i++) {
                        ?>
                        <option <?php echo getGet('p') == $i ? 'selected' : ''; ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <?php if ($tp >= $proximo) { ?>
                    <a class="btn btn-default" href="produtos.php?p=<?php echo $proximo; ?>&q=<?php echo $query; ?>">Proximo <i class="fa fa-arrow-right"></i></a>
                <?php } ?>
                <?php if ($tp < $proximo) { ?>
                    <a class="btn btn-default" href="#">Proximo <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>



<?php
rodape(array(
    "js" => array(
        "js/produto/produto.js",
    )
));
?>
<script>
    var query = '<?php echo $query; ?>';
</script>
