<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
require 'lib/telas.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

define('SEM_ENTRADA', 0);
define('COM_ENTRADA', 1);

$con = MyPdo::connect();
$condicaopgto = '';
$entrada = '';
$qtdparcela = '';
$_SESSION['erro'] = false;

if ($_POST) {
    $condicaopgto = $_POST['condicaopgto'];
    $entrada = isset($_POST['entrada']) ? $_POST['entrada'] : false;
    $qtdparcela = $_POST['qtdparcelas'];

    if ($entrada) {
        $entrada = COM_ENTRADA;
    } else {
        $estrada = SEM_ENTRADA;
    }


    try {
        $sql = 'Select ds_condicao from tbcondpgto where ds_condicao = :ds_condicao';
        $stmt = $con->prepare($sql);
        $stmt->bindValue(':ds_condicao', $condicaopgto);
        $stmt->execute();
        $condicaopgtoSelect = $stmt->fetch(PDO::FETCH_COLUMN);
    } catch (Exception $e) {
        die($e);
    }

    if ($condicaopgtoSelect) {
        addMessage('Condição pagamento ja existe, insira condição pagamento diferente');
        erro();
    }

    if (empty($condicaopgto)) {
        addMessage('Campo descrição esta vazio, insira uma descrição');
        erro();
    }
    if (($entrada != COM_ENTRADA) and ( $entrada != SEM_ENTRADA)) {
        addMessage('Campo entrada esta com valor inválido, selecione um valor válido');
        erro();
    }
    if (empty($qtdparcela) and ( $qtdparcela != 0)) {
        addMessage('Campo quantidade de parcelas esta vazio, digite quantidade');
        erro();
    }
    if ($qtdparcela < 0) {
        addMessage('Campo quantidade de parcelas esta com valor inválido, digite valor válido');
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Insert into tbcondpgto (ds_condicao, qtde_parcela, entrada, situacao) values(:ds_condicao, :qtde_parcela, :entrada,:situacao)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':ds_condicao', $condicaopgto);
            $stmt->bindValue(':qtde_parcela', $qtdparcela);
            $stmt->bindValue(':entrada', $entrada);
            $stmt->bindValue(':situacao', CONDPGTO_ATIVO);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'condicaopgto.php',
                'origem' => 'condição pagamento',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            die('Erro ao salvar condição de pagamento ' . $e);
        }
    }
}
topo(array(
    'css' => array(
        "css/condicaopgto/condicaopgto.css",
    ),
    'pageName' => ' Cadastrar Condição Pagamento',
    'icon' => 'fa fa-credit-card',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarCondpgto" name="cadastrarCondpgto" role="form" method="post" action="condicaopgto-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="papel">Descrição*</label>
                                    <input type="text" class="form-control" id="condicaopgto" name="condicaopgto" placeholder="Descrição condição pagamento " value="<?php echo $condicaopgto; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input form="alterarCliente" type="checkbox" name="generico" <?php echo $entrada ? 'checked' : ''; ?>  > Entrada?
                                        </label>
                                    </div>
                                    <div class="help-block" >Ao selecionar esta opção, condição de pagamento terá entrada.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="papel">Quantidade de parcelas*</label>
                                    <input type="number" min="0" class="form-control" id="papel" name="qtdparcelas"  value="<?php echo $qtdparcela; ?>">
                                    <div class="help-block" >Informar a quantidade total de parcelas, Ex: entrada + 2 informar 3.</div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        </form>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="cadastrarCondpgto">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="cadastrarCondpgto">Cancelar </button>

        </div>
    </div>
</div>

<?php
rodape(array(
    'js' => array(
        'js/condicaopgto/condicaopgto.js',
    )
));
?>
