
<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$idfornecedor = '';
if (isset($_GET['cd'])) {
    $idfornecedor = $_GET['cd'];
    $sql = "Select * from tbfornecedor where idfornecedor = :idfornecedor";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idfornecedor', $idfornecedor);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$consulta) {
        editarErro(array(
            'page' => 'fornecedores.php',
            'origem' => 'fornecedor',
            'tipo' => 'Erro na atualização'
        ));
    }
} elseif (isset($_POST['idfornecedor'])) {
    $idfornecedor = $_POST['idfornecedor'];
} else {
    editarErro(array(
        'page' => 'fornecedores.php',
        'origem' => 'fornecedor',
        'tipo' => 'Erro na atualização'
    ));
}

$sql = "Select * from tbfornecedor where idfornecedor = :idfornecedor";
$stmt = $con->prepare($sql);
$stmt->bindValue(':idfornecedor', $idfornecedor);
$stmt->execute();
$fornecedor = $stmt->fetch(PDO::FETCH_ASSOC);

$nome = '';
$nomeFantasia = '';
$rgie = '';
$cpfcnpj = '';
$tipo = '';
$telefone = '';
$celular = '';
$email = '';
$logradouro = '';
$bairro = '';
$numero = '';
$complemento = '';
$cidade = 0;
$generico = '';
$estado = 0;
$estadosTeste = '';
$cidadesTeste = '';
$situacao = FORNECEDOR_ATIVO;
$_SESSION['erro'] = false;


$sqlEstado = "SELECT id FROM estado";
$consulta = $con->query($sqlEstado);
$consulta->execute();

$sqlCidade = "SELECT id FROM cidade";
$consultaCidade = $con->query($sqlCidade);
$consultaCidade->execute();

$sqlCnpj = "Select cpf_cnpj from tbfornecedor where idfornecedor != :idfornecedor";
$consultaCnpjCpf = $con->prepare($sqlCnpj);
$consultaCnpjCpf->bindValue(':idfornecedor', $idfornecedor);
$consultaCnpjCpf->execute();

$estados = $consulta->fetchAll(PDO::FETCH_COLUMN);
$cidades = $consultaCidade->fetchAll(PDO::FETCH_COLUMN);
$cnpjcpfs = $consultaCnpjCpf->fetchAll(PDO::FETCH_COLUMN);


if ($_POST) {

    $hoje = strtotime(date("Y-m-d 00:00:00"));
    $nome = getPost('fornecedor');
    $cpfcnpj = getPost('cpfcnpj');
    $rgie = getPost('rgie');
    $tipo = getPost('tipo');
    $telefone = getPost('telefone');
    $celular = getPost('celular');
    $email = getPost('email');
    $logradouro = getPost('logradouro');
    $bairro = getPost('bairro');
    $numero = getPost('numero');
    $estado = getPost('estado');
    $situacao = getPost('situacao');
    $nomeFantasia = getPost('nomefantasia');
    if ($situacao) {
        $situacao = FORNECEDOR_ATIVO;
    } else {
        $situacao = FORNECEDOR_INATIVO;
    }


//Falta validar
    $complemento = getPost('complemento');
    $cidade = getPost('cidade');
    $generico = getPost('generico');

    if (!$generico) {
        if ($estado == 0) {
            addMessage("Selecione um estado");
            erro();
        }
        if ($cidade == 0) {
            addMessage("Selecione uma cidade");
            erro();
        }
        if (in_array($cpfcnpj, $cnpjcpfs)) {
            addMessage('Cpf ou cnpj de fornecedor já cadastrado! Insira outro fornecedor');
            erro();
        }
        if (!in_array($tipo, $tipos)) {
            addMessage('Selecione um tipo válido');
            erro();
        }
        if (!in_array($estado, $estados)) {
            addMessage('Selecione um estado válido');
            erro();
        }
        if (!in_array($cidade, $cidades)) {
            addMessage('Selecione uma cidade válida');
            erro();
        }

        if ($tipo == PESSOA_FISICA) {
            $retorno = cpf($cpfcnpj);
            if (true !== $retorno) {
                addMessage('CPF inválido!');
                erro();
            }
        } elseif ($tipo == PESSOA_JURIDICA) {
            $retorno = cnpj($cpfcnpj);
            if (!$retorno) {
                addMessage('CNPJ inválido!');
                erro();
            }
        }

        if (empty($nome)) {
            addMessage("Preencha razão social");
            erro();
        }
        if (empty($nomeFantasia)) {
            addMessage("Preencha nome fantasia");
            erro();
        }

        if (emailValidate($email) != true) {
            addMessage('Email Inválido');
            erro();
        }
        if (false === numberValidate($numero)) {
            addMessage("Preencha campo número com números ");
            erro();
        }
    } else {
        if ($estado != 0) {
            if (!in_array($estado, $estados)) {
                addMessage('Selecione um estado válido');
                erro();
            }
        }
        if ($cidade != 0) {
            if (!in_array($cidade, $cidades)) {
                addMessage('Selecione uma cidade válida');
                erro();
            }
        }
        if (!empty($cpfcnpj)) {
            if (in_array($cpfcnpj, $cnpjcpfs)) {
                addMessage('Cpf ou cnpj de fornecedor já cadastrado! Insira outro fornecedor');
                erro();
            }
        }
        if (!empty($tipo)) {
            if (!in_array($tipo, $tipos)) {
                addMessage('Selecione um tipo válido');
                erro();
            }
        }
        if (!empty($estado)) {
            if (!in_array($estado, $estados)) {
                addMessage('Selecione um estado válido');
                erro();
            }
        }
        if (!empty($cidade)) {
            if (!in_array($cidade, $cidades)) {
                addMessage('Selecione uma cidade válida');
                erro();
            }
        }
        if (!empty($cpfcnpj)) {
            if ($tipo == PESSOA_FISICA) {
                $retorno = cpf($cpfcnpj);
                if (true !== $retorno) {
                    addMessage('CPF inválido!');
                    erro();
                }
            } elseif ($tipo == PESSOA_JURIDICA) {
                $retorno = cnpj($cpfcnpj);
                if (!$retorno) {
                    addMessage('CNPJ inválido!');
                    erro();
                }
            } else {
                addMessage('Informe tipo');
                erro();
            }
        }
        if (!empty($email)) {
            if (emailValidate($email) != true) {
                addMessage('Email Inválido');
                erro();
            }
        }
        if (!empty($numero)) {
            if (false === numberValidate($numero)) {
                addMessage("Preencha campo número com números ");
                erro();
            }
        }

        if (empty($nomeFantasia)) {
            addMessage("Preencha o campo nome fantasia");
            erro();
        }
    }
    if (!getSession('erro')) {
        if ($cidade == 0) {
            $cidade = null;
        }
        if ($estado == 0) {
            $estado = null;
        }
        try {
            $sql = "Update  tbfornecedor set nome_razao = :nome, cpf_cnpj =:cpf_cnpj, rg_ie = :rg_ie"
                    . ", tipo = :tipo, "
                    . "telefone = :telefone, celular = :celular,"
                    . "email = :email, logradouro = :logradouro, bairro = :bairro, "
                    . "numero = :numero, complemento = :complemento, estado = :estado, cidade = :cidade,  situacao = :situacao,"
                    . " nome_fantasia = :nome_fantasia where idfornecedor = :idfornecedor";
            $stmt = $con->prepare($sql);
            $stmt->bindParam(":nome", $nome);
            $stmt->bindParam(":cpf_cnpj", $cpfcnpj);
            $stmt->bindParam(":rg_ie", $rgie);
            $stmt->bindParam(":tipo", $tipo);
            $stmt->bindParam(":telefone", $telefone);
            $stmt->bindParam(":celular", $celular);
            $stmt->bindParam(":email", $email);
            $stmt->bindParam(":logradouro", $logradouro);
            $stmt->bindParam(":bairro", $bairro);
            $stmt->bindParam(":numero", $numero);
            $stmt->bindParam(":complemento", $complemento);
            $stmt->bindParam(":cidade", $cidade);
            $stmt->bindParam(":situacao", $situacao);
            $stmt->bindParam(":idfornecedor", $idfornecedor);
            $stmt->bindParam(":nome_fantasia", $nomeFantasia);
            $stmt->bindParam(":estado", $estado);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'fornecedores.php',
                'origem' => 'fornecedor',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            addMessage("Erro ao alterar");
            $_SESSION['erro'] = true;
            throw new Exception("Erro ao alterar" . $e);
        }
    }
} else {
    $nome = $fornecedor['nome_razao'];
    $cpfcnpj = $fornecedor['cpf_cnpj'];
    $rgie = $fornecedor['rg_ie'];
    $tipo = $fornecedor['tipo'];
    $telefone = $fornecedor['telefone'];
    $celular = $fornecedor['celular'];
    $email = $fornecedor['email'];
    $logradouro = $fornecedor['logradouro'];
    $bairro = $fornecedor['bairro'];
    $numero = $fornecedor['numero'];
    $situacao = $fornecedor['situacao'];
    $nomeFantasia = $fornecedor['nome_fantasia'];
    $estado = $fornecedor['estado'];
    $cidade = $fornecedor['cidade'];
}
?>


<?php
topo(array(
    'css' => array(
        "css/fornecedor/fornecedor.css",
    ),
    'pageName' => ' Alterar Fornecedor',
    'icon' => 'fa fa-truck',
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="alterarFornecedor" name="fornecedor-alterar" role="form" method="post" action="fornecedor-editar.php">
                    <input type="hidden" name="idfornecedor" value="<?php echo $idfornecedor; ?>">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>   <input form="alterarFornecedor" type="checkbox" name="generico" <?php echo $generico ? 'checked' : ''; ?>  > Alteração Genérica?</label>

                                    </div>
                                    <div class="help-block">Ao selecionar esta opção, somente os campos preenchidos serão validados</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="ffornecedor">Nome Fantasia*</label>
                                    <input type="text" class="form-control" id="ffornecedor" name="nomefantasia" placeholder="Nome fantasia " value="<?php echo $nomeFantasia; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="ffornecedor">Razão Social*</label>
                                    <input type="text" class="form-control" id="ffornecedor" name="fornecedor" placeholder="Razão social " value="<?php echo $nome; ?>">
                                </div>
                            </div>

                        </div>

                         <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-6  col-md-6">
                                    <label for="ftipo">Tipo*</label>
                                    <select class="form-control" name="tipo" id="ftipo">
                                        <option value="0">Selecione</option>
                                        <option value="1" <?php echo $tipo == PESSOA_FISICA ? 'selected' : ''; ?>>Físico</option>
                                        <option value="2" <?php echo $tipo == PESSOA_JURIDICA ? 'selected' : ''; ?>>Jurídico</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="fcpfcnpj">CPF / CNPJ*</label>
                                        <input type="text" class="form-control" id="fcpfcnpj" name="cpfcnpj" placeholder="Somente números " value="<?php echo $cpfcnpj; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <label for="frgie">RG / IE*</label>
                                <input type="text" class="form-control" id="frgie" name="rgie" placeholder="Somente números "  value="<?php echo $rgie; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="ftelefone">Telefone*</label>
                                    <input type="text" data-mask="(00) 0000-0000" class="form-control" id="ftelefone" name="telefone" value="<?php echo $telefone; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <label for="fcelular">Celular*</label>
                                    <input type="text"  class="form-control" id="fcelular" name="celular" value="<?php echo $celular; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="fcelular">Email*</label>
                                    <input type="email" class="form-control" id="femail" name="email" placeholder="email@exemple.com" value="<?php echo $email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title">Endereço</h1>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="flogradouro">Logradouro*</label>
                                            <input type="text" name="logradouro" id="flogradouro" class="form-control" value="<?php echo $logradouro; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fnumero">Bairro*</label>
                                            <input type="text" name="bairro" id="fbairro" class="form-control" value="<?php echo $bairro; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-4">
                                        <div class="form-group">
                                            <label for="fnumero">Número*</label>
                                            <input type="number" name="numero" id="fnumero" class="form-control" value="<?php echo $numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8">
                                        <div class="form-group">
                                            <label for="fcomplemento">Complemento</label>
                                            <input type="text" name="complemento" id="fcomplemento" class="form-control" value="<?php echo $complemento; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="festado">Estado* </label>
                                            <select class="form-control" id="festado" name="estado">
                                                <option value="0">Escolha o estado </option>
                                                <?php
                                                $sql = "SELECT id, nome FROM estado order by nome";
                                                $consulta = $con->query($sql);
                                                $consulta->execute();
                                                while ($resultado = $consulta->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option <?php echo $resultado['id'] == $estado ? ' selected ' : ''; ?>value="<?php echo $resultado['id']; ?>"
                                                                                                                          <?php if ($resultado == $resultado['id']) { ?> selected <?php } ?>
                                                                                                                          ><?php echo $resultado['nome']; ?></option>
                                                                                                                      <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <div class="form-group">
                                            <label for="fcidade">Cidade* </label>
                                            <select class="form-control" id="fcidade" name="cidade">
                                                <option value="0">--Selecione o Estado --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6  col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>   <input form="alterarFornecedor" type="checkbox" name="situacao" <?php echo $situacao ? 'checked' : ''; ?>  > Fornecedor Ativo?</label>
                                    </div>
                                    <div class="help-block">Ao selecionar esta opção status do fornecedor sera ativo, caso contrario status sera inativo.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="alterarFornecedor">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="alterarFornecedor">Cancelar </button>

            </div>
        </div>
    </div>
</div>
<script>
    var cidade = '<?php echo $cidade; ?>';
    var estado = '<?php echo $estado; ?>';
</script>
<?php
rodape(array(
    'js' => array(
        'js/fornecedor/fornecedor.js',
    )
));
?>
