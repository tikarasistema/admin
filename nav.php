<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="logoSistema">
            <div class="hidden-xs">
<!--   <img src="lib/img/logohome.png" alt="" style="
 width: 200px;
 /* margin-top: 1px; */
 margin-left: 70px;
 position: absolute;
 background-color: #2196f3;
 border-radius: 14%;
 padding: 17px;
">-->
            </div>
            <!--

            -->
            <span style="font-family: Traditional Arabic"><span style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;color: #0c84e4 ;font-weight: bold;">HIVE</span><span style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;font-weight: bold;color: black;">COMMERCE</span></span>
        </div>
        <div class="collapse navbar-collapse" id="nav1">


            <ul class="nav navbar-nav">
                <li><a href="./"><i class="fa fa-home"></i> Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-key"></i> Usuários<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="usuarios.php"><i class="fa fa-user"></i> Usuários</a></li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Pessoas<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="clientes.php"><i class="fa fa-user"></i> Clientes</a></li>
                        <li class="divider"></li>
                        <li><a href="fornecedores.php"><i class="fa fa-truck"></i> Fornecedores</a></li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-inbox"></i> Produtos<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="produtos.php"><i class="fa fa-inbox"></i> Produtos</a></li>
                        <li class="divider"></li>
                        <li><a href="classificacoes.php"><i class="fa fa-sitemap"></i> Classificações</a></li>
                        <li class="divider"></li>
                        <li><a href="marcas.php"><i class="fa fa-tags"></i> Marcas</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar"></i> Vendas<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="vendas.php"><i class="fa fa-money"></i> Vendas</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cart-plus"></i> Compras<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="compras.php"><i class="fa fa-cart-plus"></i> Compras</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-dollar"></i> Financeiro<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="condicaopgto.php"><i class="fa fa-dollar"></i> Condições Pagamento</a></li>
                        <li class="divider"></li>
                        <li><a href="lancamentos.php" id="lancamentoConta"><i class="fa fa-money"></i> Lançamentos de fluxo de caixa</a></li>
                        <li class="divider"></li>
                        <li><a href="historico.php"><i class="fa fa-signal"></i> Histórico</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i> Relatórios<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="relatorio-receber-geral.php"><i class="fa fa-smile-o" aria-hidden="true"></i> Relatório à receber (todos clientes)</a></li>
                        <li class="divider"></li>
                        <li><a href="relatorio-pagar-geral.php"><i class="fa fa-frown-o" aria-hidden="true"></i> Relatório à pagar (todos fornecedores)</a></li>
                        <li class="divider"></li>
                        <li><a href="caixa.php"><i class="fa fa-money"></i> Fluxo de caixa</a></li>
                        <li class="divider"></li>
                        <li><a href="produto-mais-vendido-meses.php"><i class="fa fa-inbox" aria-hidden="true"></i> Produto mais vendido nos ultimos x meses ou dias</a></li>
                        <li class="divider"></li>
                        <li><a href="quantidade-venda-periodo.php"><i class="fa fa-line-chart" aria-hidden="true"></i> Quantidade de vendas nos ultimos x meses ou dias</a></li>
                        <li class="divider"></li>
                        <li><a href="estoque-produtos.php"><i class="fa fa-cubes" aria-hidden="true"></i> Estoque de produtos</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a id="usuario" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="perfil.php?cd=<?php echo $_SESSION['usuario']['idusuario']; ?>"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a></li>
                        <li class="divider"></li>
                        <li><a href="tema.php"><i class="fa fa-cogs" aria-hidden="true"></i> Alterar tema</a></li>
                        <li><a href="papeis.php"><i class="fa fa-key"></i> Papéis</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
