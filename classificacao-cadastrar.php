<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$classificacao = '';
$_SESSION['erro'] = false;

if ($_POST) {
    $con = MyPdo::connect();
    $classificacao = getPost('classificacao');

    $sql = "Select UPPER(dsclassificacao) from tbtipo where dsclassificacao = UPPER(:dsclassificacao)";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dsclassificacao', $classificacao);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Classificação cadastrada anteriormente");
        erro();
    }

    if (empty($classificacao)) {
        addMessage("Preecnha campo descrição");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Insert into tbtipo (dsclassificacao, situacao) values (:dsclassificacao, :situacao)";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dsclassificacao', $classificacao);
            $stmt->bindValue(':situacao', CLASSIFICACAO_ATIVO);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'classificacoes.php',
                'origem' => 'classificação',
                'tipo' => 'Cadastro'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}

topo(array(
    "pageName" => 'Cadastrar classificação',
    "icon" => 'fa fa-sitemap'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="cadastrarClassificacao" name="cadastrarClassificacao" role="form" method="post" action="classificacao-cadastrar.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fclassificacao">Descrição*</label>
                                    <input type="text" class="form-control" id="classificacao" name="classificacao" placeholder="Digite nome da classificação " value="<?php echo $classificacao; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="cadastrarClassificacao">Salvar </button>
                <button type="reset" class="btn btn-danger"  form="cadastrarClassificacao">Cancelar </button>
            </div>
        </div>
    </div>
</div>
</div>
<?php rodape(array()); ?>
