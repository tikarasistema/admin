<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$situacao = array();

$idcliente = getGet('idcliente', 0);
$idvenda = getGet('idvenda', 0);
try {
    $con = MyPdo::connect();
    $sql = "select r.*, v.*, c.* from tbvenda as v inner join tbcontasreceber as r on v.idvenda = :idvenda inner join tbcliente as c on c.idcliente = :idcliente";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idvenda', $idvenda);
    $stmt->bindValue(':idcliente', $idcliente);
    $stmt->execute();
    $parcelas = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    var_dump($e);
    exit;
}
//die("<pre>" . __FILE__ . " - " . __LINE__ . "\n" . print_r($parcelas, true) . "</pre>");

if ($idcliente == 0) {
    editarErro(array(
        'page' => 'clientes.php',
        'origem' => 'Boleto a receber',
        'tipo' => 'Cliente inválido',
    ));
}
if ($idvenda == 0) {
    editarErro(array(
        'page' => 'clientes.php',
        'origem' => 'Boleto a receber',
        'tipo' => 'Cliente inválido',
    ));
}
topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
    ),
    "pageName" => 'Boletos à receber ',
    "icon" => 'fa fa-money'
));
?>
<?php
foreach ($parcelas as $parcela) {
    ?>
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações parcela #<?php echo $parcela['parcela_cr']; ?></h1>
            </div>
            <div class="panel-body">
                 <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela"><strong><?php echo EMPRESA;?></strong> </label>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela">Cliente : <?php echo '#'.$parcela['idcliente'].'-'.$parcela['nome_razao']; ?></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela">Emissão : <?php echo formatDate($parcela['dt_emissao'], DATE_BRASIL); ?></label>
                        </div>
                    </div>
                     <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela">Vencimento : <?php echo formatDate($parcela['dt_vencimento'], DATE_BRASIL); ?></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="fparcela">Valor parcela: <?php echo 'R$ '.number_format ($parcela['vlr_parcela'],2, ',' , '.' ); ?></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela">Valor pago: <?php echo $parcela['vrl_recebido'] > 0 ? 'R$ '.number_format ($parcela['vrl_recebido'],2, ',' , '.' ) :  'R$___________'; ?></label>
                        </div>
                    </div>
                     <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fparcela">Valor à receber : <?php echo 'R$ '.number_format ($parcela['vlr_parcela']-$parcela['vrl_recebido'],2, ',' , '.' ); ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js",
    )
));
?>
