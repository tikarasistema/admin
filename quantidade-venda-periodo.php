<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
define('MESES', 1);
define('DIAS', 2);
$periodo = '';
$qtdMeses = '';
$tipo = '';
$metodo = '';
if ($_POST) {
    $qtdMeses = getPost('meses');
    $periodo = getPost('optradio');

    if ($periodo == '' or $qtdMeses == '') {
        editarErro(array(
            'page' => $_SERVER['HTTP_REFERER'],
            'origem' => 'Relatório quantidade venda periodo',
            'tipo' => 'Erro na consulta selecione dados válidos',
        ));
    }
    if ($periodo == MESES) {
        $tipo = 'meses';
    }
    if ($periodo == DIAS) {
        $tipo = 'dias';
    }
} else {
    $qtdMeses = 'x';
    $tipo = ' meses ou dias';
}
$label = array();
$data = array();

topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
        "css/relatorios/produto/produto.css"
    ),
    "pageName" => 'Quantidade de vendas nos ultimos ' . $qtdMeses . ' ' . $tipo . '<br><br> Dia atual: ' . date('d/m/Y'),
    "icon" => 'fa fa-list'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title remover-print">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="gerarRelatorio" name="gerarRelatorio" role="form" method="post" action="quantidade-venda-periodo.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="pesquisa">Método de pesquisa* </label>
<!--                                    <select class="form-control" id="pesquisa" name="pesquisa">
                                        <option value="1">Código de barra</option>
                                        <option value="2">Descrição</option>
                                    </select>-->
                                    <div class="radio">
                                        <label><input <?php echo $periodo == MESES ? 'checked' : ''; ?> type="radio" class="metodo" value="1" name="optradio">Meses</label>
                                    </div>
                                    <div class="radio">
                                        <label><input <?php echo $periodo == DIAS ? 'checked' : ''; ?>  type="radio" class="metodo" value="2" name="optradio">Dias</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group remover-print">
                                    <label for="fclassificacao">Quantidade de meses ou dias*</label>
                                    <input type="number" min="1" class="form-control" id="meses" value="<?php echo $qtdMeses; ?>" name="meses">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php if ($_POST) {
                                    ?>
                                    <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if ($_POST) {
                    try {
                        $qtdMeses = getPost('meses');
                        $optradio = getPost('optradio');

                        if ($optradio == MESES) {
                            $metodo = 'MONTH';
                            $relatorioTipo = 'Mêses';
                        }
                        if ($optradio == DIAS) {
                            $metodo = 'DAY';
                            $relatorioTipo = 'Dias';
                        }

                        $con = MyPdo::connect();
                        $sql = "select date(data_venda) as data, count(idvenda) as vendas from tbvenda where date(data_venda) BETWEEN SUBDATE(CURRENT_DATE, INTERVAL :meses $metodo ) AND CURRENT_DATE group by date(data_venda) order by date(data_venda) asc ";
                        $stmt = $con->prepare($sql);
                        $stmt->bindValue(':meses', $qtdMeses);
                        $stmt->execute();
                        $qtdVendas = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    } catch (Exception $e) {
                        // var_dump($e);
                        exit;
                    }
                    ?>
                    <div class="col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h1 class="panel-title remover-print">Quantidade de vendas nos ultimos <?php echo $qtdMeses . ' - ' . $relatorioTipo; ?></h1>
                            </div>
                            <div class="panel-body">
                                <!--<div class="col-xs-12 col-md-12">-->
                                <div class="box-chart">
                                    <canvas id="qtdVendasMes" ></canvas>
                                </div>
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($qtdVendas as $qtdVenda) {
                                $dia[] = formatDate($qtdVenda['data'], DATE_BRASIL);
                                $vendas[] = $qtdVenda['vendas'];
                                ?>
                                <tr>
                                    <td> <?php echo formatDate($qtdVenda['data'], DATE_BRASIL); ?></td>
                                    <td> <?php echo $qtdVenda['vendas']; ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                <button type="reset" class="btn btn-danger"  form="gerarRelatorio">Cancelar </button>
                <?php if ($_POST) {
                    ?>
                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                <?php }
                ?>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var vendas = <?php echo json_encode($vendas); ?>;
    var dia = <?php echo json_encode($dia); ?>;
</script>
<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js",
        'js/charts/chartsjs/Chart.min.js',
        "js/relatorios/produto/qtd_vendas.js",
    )
));
?>
