<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}
$con = MyPdo::connect();
$marca = '';
$idmarca = '';
$situacao = '';
$_SESSION['erro'] = false;

if (isset($_GET['cd'])) {
    $idmarca = $_GET['cd'];

    $sql = "Select * from tbmarca where idmarca = :idmarca";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':idmarca', $idmarca);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$consulta) {
        editarErro(array(
            'page' => 'marcas.php',
            'origem' => 'marca',
            'tipo' => 'Erro na atualização'
        ));
    }
}
if ((!isset($_GET['cd'])) and ( !isset($_POST['idmarca']))) {
    editarErro(array(
        'page' => 'marcas.php',
        'origem' => 'marca',
        'tipo' => 'Erro na atualização'
    ));
}
if ($_POST) {
    $marca = getPost('marca');
    $idmarca = getPost('idmarca');
    $situacao = getPost('situacao');
    if ($situacao) {
        $situacao = MARCA_ATIVA;
    } else {
        $situacao = MARCA_INATIVA;
    }
    $sql = "Select UPPER(dsmarca) from tbmarca where dsmarca = UPPER(:dsmarca) and idmarca != :idmarca";
    $stmt = $con->prepare($sql);
    $stmt->bindValue(':dsmarca', $marca);
    $stmt->bindValue(':idmarca', $idmarca);
    $stmt->execute();
    $consulta = $stmt->fetch(PDO::FETCH_ASSOC);


    if ($consulta) {
        addMessage("Marca cadastrada anteriormente");
        erro();
    }
    if (($situacao != MARCA_ATIVA) and ( $situacao != MARCA_INATIVA)) {
        addMessage("Selecione situação válida");
        erro();
    }


    if (empty($marca)) {
        addMessage("Preencha campo descrição");
        erro();
    }

    if (!getSession('erro')) {
        try {
            $sql = "Update tbmarca set dsmarca = :dsmarca, situacao = :situacao where idmarca = :idmarca";
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':dsmarca', $marca);
            $stmt->bindValue(':idmarca', $idmarca);
            $stmt->bindValue(':situacao', $situacao);
            $stmt->execute();
            sucessInsertUpdate(array(
                'page' => 'marcas.php',
                'origem' => 'marca',
                'tipo' => 'Atualização'
            ));
        } catch (Exception $e) {
            addMessage($e);
        }
    }
}

$sql = "select * from tbmarca where idmarca = :idmarca";
$stmt = $con->prepare($sql);
$stmt->bindParam(':idmarca', $idmarca);
$stmt->execute();
$marcaAtual = $stmt->fetch(PDO::FETCH_ASSOC);

topo(array(
    "pageName" => 'Editar marca #' . $idmarca,
    "icon" => 'fa fa-tags'
));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="editarMarca" name="editarMarca" role="form" method="post" action="marca-editar.php">
                    <input type="hidden" value="<?php echo $idmarca ?>" name="idmarca">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="fmarca">Descrição*</label>
                                    <input type="text" class="form-control" id="marca" name="marca" placeholder="Digite nome da marca " value="<?php echo $marca ? $marca : $marcaAtual['dsmarca']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="fmarca"><input name="situacao" type="checkbox" <?php echo $marcaAtual['situacao'] == MARCA_ATIVA ? 'checked' : ''; ?>> Ativar marca?
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary" form="editarMarca">Salvar </button>
            <button type="reset" class="btn btn-danger"  form="editarMarca">Cancelar </button>
        </div>
    </div>
</div>
</div>
</div>
<?php rodape(array()); ?>
