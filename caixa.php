<?php
require 'lib/conexao.php';
require 'lib/config.php';
require 'lib/funcoes.php';
require 'lib/protege.php';
require 'lib/verifica-url.php';
$acesso = permissaoAcesso();
if ($acesso == false) {
    acessoNegado();
    exit;
}

$dataInicial = date("Y-m-d");
$dataFinal = date("Y-m-d");
$totalDebito = 0;
$totalCredito = 0;
$total = 0;

if ($_POST) {
    $dataInicial = getPost('dataInicial');
    $dataFinal = getPost('dataFinal');
}
topo(array(
    "css" => array(
        "css/relatorios/relatorios.css",
        "css/relatorios/caixa/caixa.css"
    ),
    "pageName" => 'Relatório de fluxo de caixa',
    "icon" => 'fa fa-list'
));
?>
<div class="row" id="relatorioCaixa">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Informações básicas</h1>
            </div>
            <div class="panel-body">
                <form id="gerarRelatorio" name="gerarRelatorio" role="form" method="post" action="caixa.php">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="dataInicial">Data Inicial*</label>
                                    <input type="date" class="form-control" id="dataInicial" value="<?php echo $dataInicial; ?>" name="dataInicial">
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group">
                                    <label for="dataFinal">Data Final*</label>
                                    <input type="date" class="form-control" id="dataFinal" value="<?php echo $dataFinal; ?>" name="dataFinal">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
                if ($_POST) {
                    try {
                        $con = MyPdo::connect();
                        $stmt = $con->prepare("Select c.*, u.stnome, h.dshistorico from tbcaixa as c inner join tbhistorico as h on c.idhistorico = h.idhistorico inner join tbusuario as u on c.idusuario = u.idusuario where dataAbertura BETWEEN  :dataInicial  and :dataFinal order by dataLancamento");
                        $stmt->bindValue(":dataInicial", $dataInicial);
                        $stmt->bindValue(":dataFinal", $dataFinal);
                        $stmt->execute();
                        $lancamentos = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    } catch (Exception $e) {
                        var_dump($e);
                        exit;
                    }
                    ?>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">Descrição</th>
                                <th class="text-center">Data Abertura de caixa</th>
                                <th class="text-center">Data lançamento</th>
                                <th class="text-center">Usuário</th>
                                <th class="text-center">Histórico</th>
                                <th class="text-center">Crédito</th>
                                <th class="text-center">Débito</th>
                                <th class="text-center">Total Parcial</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lancamentos as $lancamento) { ?>
                                <tr class="text-center" data-id="<?php echo $lancamento['idlancamento']; ?>">
                                    <td> <?php echo $lancamento['descLancamento']; ?></td>
                                    <td> <?php echo formatDate($lancamento['dataAbertura'], DATE_BRASIL); ?></td>
                                    <?php
                                    $data = explode(" ", $lancamento['dataLancamento']);
                                    $hora = $data[1];
                                    $data = formatDate($data[0], DATE_BRASIL);
                                    ?>
                                    <td> <?php echo $data . ' ' . $hora; ?></td>
                                    <td> <?php echo $lancamento['stnome']; ?></td>
                                    <td> <?php echo $lancamento['dshistorico']; ?></td>
                                    <td> <?php echo $lancamento['tipoLancamento'] == LANCAMENTO_CREDITO ? 'R$ ' . number_format($lancamento['vlLancamento'], 2, ',', '.') : ''; ?></td>
                                    <td> <?php echo $lancamento['tipoLancamento'] == LANCAMENTO_DEBITO ? 'R$ ' . number_format($lancamento['vlLancamento'], 2, ',', '.') : ''; ?></td>
                                    <?php
                                    if ($lancamento['tipoLancamento'] == LANCAMENTO_CREDITO) {
                                        $totalCredito += $lancamento['vlLancamento'];
                                    } elseif ($lancamento['tipoLancamento'] == LANCAMENTO_DEBITO) {
                                        $totalDebito += $lancamento['vlLancamento'];
                                    }
                                    ?><td><?php echo 'R$ ' . number_format($totalCredito - $totalDebito, 2, ',', '.'); ?></td></tr>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td colspan="5" style="text-align: center;font-size: 20px;"><strong>Totais Crédito/Débito</strong></td>
                                <td><strong><?php echo 'R$ ' . number_format($totalCredito, 2, ',', '.'); ?></strong></td>
                                <td><strong><?php echo 'R$ ' . number_format($totalDebito, 2, ',', '.'); ?></strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center;font-size: 20px;"><strong>Total</strong></td>
                                <td style="text-align: center;"><strong><?php echo 'R$ ' . number_format($totalCredito - $totalDebito, 2, ',', '.'); ?></strong></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary" form="gerarRelatorio">Gerar Relatório </button>
                <button type="reset" class="btn btn-danger"  form="gerarRelatorio">Cancelar </button>
                <?php if ($_POST) {
                    ?>
                    <button type="button" class="btn btn-success imprimir" form="gerarRelatorio">Imprimir </button>
                <?php }
                ?>

            </div>
        </div>
    </div>
</div>

<?php
rodape(array(
    "js" => array(
        "js/relatorios/relatorios.js"
    )
));
?>
